<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AreaHeadController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Auth\RegisterController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/test', function () {
    return response()->json(["message" => "ok"]);
});

Route::post('login', 'AuthController@login');
Route::post('logout', 'AuthController@logout');
// Route::post('refresh', 'AuthController@refresh');
Route::post('register', 'AuthController@register');
Route::get('register_verifyOTP', 'AuthController@verifyOTP');

Route::get('/getCityByName/{name}', 'AreaHeadController@getCityByName');
Route::get('/getStateByName/{name}', 'AreaHeadController@getStateByName');
Route::get('/getCountryByName/{name}', 'AreaHeadController@getCountryByName');


Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('/me', function () {
        $id = Auth::user()->id;
        return response()->json(["id" => $id]);
    });


    Route::get('/test', function () {
        return response()->json(["message" => "ok"]);
    });

    Route::get('/', 'UsersController@search');

    Route::get('/family', 'AreaHeadController@index');
    Route::get('family/create/{tab}', 'AreaHeadController@create')->name('family.create');
    Route::post('family/store', 'AreaHeadController@store');
    // Route::get('family/{tab}/with/{latest_family_head_id}', 'AreaHeadController@createAddChild')->name('family.createAddChild');
    Route::get('family/{tab}/with/{head_id}', 'AreaHeadController@createChild')->name('family.createChild');
    Route::get('family/{tab}/withs/{head_id}', 'AreaHeadController@createSpouse')->name('family.createSpouse');
    Route::post('family/update/{user}', 'AreaHeadController@update');
    Route::post('family/updatechild/{user}', 'AreaHeadController@updatechild');

    // Route::post('family/{last_name_value}', 'AreaHeadController@getLastName');
    Route::post('/getLastName', 'AreaHeadController@getLastName');
    Route::get('/getCity/{id}', 'AreaHeadController@getCity');
    Route::get('/getState/{id}', 'AreaHeadController@getState');
    Route::get('/getCountry', 'AreaHeadController@getCountry');

    Route::get('home', 'HomeController@index')->name('home');

    // Route::get('profile', 'HomeController@index')->name('profile');

    Route::get('profile-search', 'UsersController@search')->name('users.search');
    Route::get('users/{user}', 'UsersController@show')->name('users.show');
    Route::get('users/{user}/edit', 'UsersController@edit')->name('users.edit');
    Route::post('users/{user}', 'UsersController@update')->name('users.update');
    Route::get('users/{user}/chart', 'UsersController@chart')->name('users.chart');
    Route::get('users/{user}/tree', 'UsersController@tree')->name('users.tree');
    Route::get('users/{user}/mobiletree', 'UsersController@mobiletree')->name('users.mobiletree');
    // Route::patch('users/{user}/photo-upload', 'UsersController@photoUpload')->name('users.photo-upload');
    Route::post('users/{user}/photo-upload', 'UsersController@photoUpload');

    Route::delete('users/{user}', 'UsersController@destroy');

    Route::get('requests', 'RequestsController@index');

    Route::get('/getfamilylist', 'CreateMyFamilyController@familyList');
    Route::post('/add-family-member', 'CreateMyFamilyController@addFamilyMember');
    Route::post('/add-family-member/{id}', 'CreateMyFamilyController@addFamilyMember');
    Route::post('/delete-family-member', 'CreateMyFamilyController@destroy');
    Route::get('/check-my-family-user', 'CreateMyFamilyController@checkMyFamilyUser');

    Route::post('password/change', 'Auth\ChangePasswordController@update');

    Route::post('/add-overseas', 'OverseasController@addOverseas');
    Route::get('/overseas-list', 'OverseasController@familyList');



});

Route::get('getOTP', 'Auth\ChangePasswordController@getOTP');
Route::get('verifyOTP', 'Auth\ChangePasswordController@verifyOTP');

Route::prefix("open")->group(function () {
    Route::namespace('Others')->group(function() {
        Route::get('app_versions', 'UtilityController@getAppVersions');
    });
});

Route::get('mastercall_task_form', 'MasterCallController@mastercall');

 // blog

Route::post('/add-blog', 'BlogNewController@addBlogs');
Route::get('/blog-list', 'BlogNewController@BlogsList');

// categories
Route::post('/addCategory', 'CategoryController@addCategory');
Route::get('/category-list', 'CategoryController@Category');



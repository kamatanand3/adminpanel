<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AreaHeadController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

Route::get('/', 'UsersController@search');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('password/change', 'Auth\ChangePasswordController@show')->name('password.change');
    Route::post('password/change', 'Auth\ChangePasswordController@update')->name('password.change');
});
Route::get('getOTP', 'Auth\ChangePasswordController@getOTP');
Route::get('verifyOTP', 'Auth\ChangePasswordController@verifyOTP');

Route::get('/family', 'AreaHeadController@index');
Route::get('family/create/{tab}', 'AreaHeadController@create')->name('family.create');
Route::post('family/store', 'AreaHeadController@store');
Route::get('family/{tab}/with/{head_id}', 'AreaHeadController@createChild')->name('family.createChild');
Route::get('family/{tab}/withs/{head_id}', 'AreaHeadController@createSpouse')->name('family.createSpouse');

// Route::get('family/{tab}/with/{latest_family_head_id}', 'AreaHeadController@createAddChild')->name('family.createAddChild');
// Route::post('family/{last_name_value}', 'AreaHeadController@getLastName');


Route::post('/getLastName', 'AreaHeadController@getLastName');
Route::get('/getCity/{id}', 'AreaHeadController@getCity');
Route::get('/getState/{id}', 'AreaHeadController@getState');
Route::get('/getCountry', 'AreaHeadController@getCountry');




Route::get('home', 'HomeController@index')->name('home');

Route::get('profile', 'HomeController@index')->name('profile');
Route::post('family-actions/{user}/set-father', 'FamilyActionsController@setFather')->name('family-actions.set-father');
Route::post('family-actions/{user}/set-mother', 'FamilyActionsController@setMother')->name('family-actions.set-mother');
Route::post('family-actions/{user}/add-child', 'FamilyActionsController@addChild')->name('family-actions.add-child');
Route::post('family-actions/{user}/add-wife', 'FamilyActionsController@addWife')->name('family-actions.add-wife');
Route::post('family-actions/{user}/add-husband', 'FamilyActionsController@addHusband')->name('family-actions.add-husband');
Route::post('family-actions/{user}/set-parent', 'FamilyActionsController@setParent')->name('family-actions.set-parent');

Route::get('profile-search', 'UsersController@search')->name('users.search');
Route::get('users/{user}', 'UsersController@show')->name('users.show');

Route::get('users/{user}/edit', 'UsersController@edit')->name('users.edit');
Route::patch('users/{user}', 'UsersController@update')->name('users.update');

Route::get('users/{user}/chart', 'UsersController@chart')->name('users.chart');
Route::get('users/{user}/tree', 'UsersController@tree')->name('users.tree');
// Route::patch('users/{user}/photo-upload', 'UsersController@photoUpload')->name('users.photo-upload');
Route::post('users/{user}/photo-upload', 'UsersController@photoUpload')->name('users.photo-upload');

Route::post('/delete-photo', 'UsersController@photoDelete');

Route::delete('users/{user}', 'UsersController@destroy')->name('users.destroy');

Route::get('/check_user', 'UsersController@checkUserPresent');
Route::get('/link_request', 'RequestsController@linkRequest');
Route::get('/action_request', 'RequestsController@actionRequest');
Route::get('requests', 'RequestsController@index')->name('requests.index');

Route::get('users/{user}/marriages', 'UserMarriagesController@index')->name('users.marriages');

Route::get('birthdays', 'BirthdayController@index')->name('birthdays.index');
Route::get('blogs', 'BlogsController@index')->name('blogs.index');
Route::get('addblog', 'BlogsController@addBlogs')->name('blogs.addblog');

/**
 * Couple/Marriages Routes
 */
Route::get('couples/{couple}', ['as' => 'couples.show', 'uses' => 'CouplesController@show']);
Route::get('couples/{couple}/edit', ['as' => 'couples.edit', 'uses' => 'CouplesController@edit']);
Route::patch('couples/{couple}', ['as' => 'couples.update', 'uses' => 'CouplesController@update']);

/**
 * Admin only routes
 */
Route::group(['middleware' => 'admin'], function () {
    /**
     * Backup Restore Database Routes
     */
    Route::post('backups/upload', ['as' => 'backups.upload', 'uses' => 'BackupsController@upload']);
    Route::post('backups/{fileName}/restore', ['as' => 'backups.restore', 'uses' => 'BackupsController@restore']);
    Route::get('backups/{fileName}/dl', ['as' => 'backups.download', 'uses' => 'BackupsController@download']);
    Route::resource('backups', 'BackupsController');
});


// Route::get('/check', 'UsersController@openapi');

Route::get('/check_mobile', 'UsersController@checkMobile');
Route::get('/check_email', 'UsersController@checkEmail');
Route::get('/check_update_email', 'UsersController@checkUpdateEmail');
Route::get('/check_update_mobile', 'UsersController@checkUpdateMobile');
Route::get('/check_update_alternate_mobile', 'UsersController@checkUpdateAlternateMobile');


Route::get('/getfamilylist', 'CreateMyFamilyController@familyList');
Route::get('/create-my-family', 'CreateMyFamilyController@index');
Route::get('/check-my-family-user', 'CreateMyFamilyController@checkMyFamilyUser');
Route::post('/add-family-member/{id}', 'CreateMyFamilyController@addFamilyMember');
Route::post('/delete-family-member', 'CreateMyFamilyController@destroy');


Route::get('/create-overseas', 'OverseasController@index');
Route::post('/add-overseas', 'OverseasController@addOverseas');


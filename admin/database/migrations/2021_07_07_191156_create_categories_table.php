<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogCategories', function (Blueprint $table) {
            $table->id();
            $table->string('nameEn');
            $table->string('nameGr');
            $table->string('displayOnWebsite');
            $table->string('displayAsMenu');
            $table->string('orderLevel');
            $table->string('created_id');
            $table->string('updated_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogCategories');
    }
}

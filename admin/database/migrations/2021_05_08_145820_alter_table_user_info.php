<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableUserInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->string('current_city')->nullable()->after('current_pincode');
            $table->string('permanent_city')->nullable()->after('permanent_pincode');
            $table->string('visibility')->nullable()->after('mobile');
            $table->string('charity')->nullable();
            $table->string('charity_organization')->nullable();
            $table->string('charity_designation')->nullable();
            $table->string('serve')->nullable();
            $table->string('serve_help')->nullable();
            $table->string('donate')->nullable();
            $table->string('donate_part')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->dropColumn('current_city');
            $table->dropColumn('permanent_city');
            $table->dropColumn('visibility');
            $table->dropColumn('charity');
            $table->dropColumn('charity_organization');
            $table->dropColumn('charity_designation');
            $table->dropColumn('serve');
            $table->dropColumn('serve_help');
            $table->dropColumn('donate');
            $table->dropColumn('donate_part');
        });
    }
}

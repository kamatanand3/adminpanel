<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVibhagMarriageUserinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->string('current_vibhag')->nullable()->after('current_pincode');
            $table->string('permanent_vibhag')->nullable()->after('permanent_pincode');
            $table->string('occupation_vibhag')->nullable()->after('occupation_pincode');
            $table->enum('marital_status', ['Single', 'Married', 'Widowed', 'Divorced', 'Separated'])->nullable()->default('Married');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->dropColumn('current_vibhag');
            $table->dropColumn('permanent_vibhag');
            $table->dropColumn('occupation_vibhag');
            $table->dropColumn('marital_status');
        });
    }
}

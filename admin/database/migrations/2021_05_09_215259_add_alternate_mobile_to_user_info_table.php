<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAlternateMobileToUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->string('alternate_mobile')->nullable()->after('mobile');
            $table->boolean('alternate_mobile_visibility')->default(0)->after('visibility');
            $table->string('birth_place')->nullable()->after('native_place');
            $table->string('gotra')->nullable();
            $table->string('divyang')->nullable();
            $table->string('mosad')->nullable();
            $table->boolean('maritial_status')->default(0);

            $table->string('occupation_building')->nullable();
            $table->string('occupation_landmark')->nullable();
            $table->string('occupation_road')->nullable();
            $table->string('occupation_pincode')->nullable();
            $table->string('occupation_city')->nullable();
            $table->string('occupation_state')->nullable();
            $table->string('occupation_country')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_info', function (Blueprint $table) {
            $table->dropColumn('alternate_mobile');
            $table->dropColumn('alternate_mobile_visibility');
            $table->dropColumn('birth_place');
            $table->dropColumn('gotra');
            $table->dropColumn('divyang');
            $table->dropColumn('mosad');
            $table->dropColumn('maritial_status');
            $table->dropColumn('occupation_building');
            $table->dropColumn('occupation_landmark');
            $table->dropColumn('occupation_road');
            $table->dropColumn('occupation_pincode');
            $table->dropColumn('occupation_city');
            $table->dropColumn('occupation_state');
            $table->dropColumn('occupation_country');

        });
    }
}

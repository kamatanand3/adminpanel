<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_info', function (Blueprint $table) {
            $table->id();
            $table->uuid('user_id')->nullable();
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name_original')->nullable();
            $table->string('native_place')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('email')->nullable();
            $table->string('mobile_code')->nullable();
            $table->string('mobile')->nullable();

            $table->string('current_building')->nullable();
            $table->string('current_landmark')->nullable();
            $table->string('current_road')->nullable();
            $table->string('current_pincode')->nullable();
            $table->string('current_state')->nullable();
            $table->string('current_country')->nullable();

            $table->string('permanent_building')->nullable();
            $table->string('permanent_landmark')->nullable();
            $table->string('permanent_road')->nullable();
            $table->string('permanent_pincode')->nullable();
            $table->string('permanent_state')->nullable();
            $table->string('permanent_country')->nullable();

            $table->string('education')->nullable();
            $table->string('college')->nullable();
            $table->string('passout_year')->nullable();
            $table->string('university')->nullable();

            $table->string('occupation')->nullable();
            $table->string('category')->nullable();
            $table->string('company_name')->nullable();
            $table->string('contact_person_name')->nullable();
            $table->string('contact_person_mobile')->nullable();
            $table->string('contact_person_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_info');
    }
}

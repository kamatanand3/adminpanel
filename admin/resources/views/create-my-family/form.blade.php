@extends('layouts.app')
@section('content')
<?php 
// dump($user->id);
$userInfo = \App\UserInfo::where('user_id', $user->id)->first();
// dump($userInfo->last_name); 
?>
<head>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/popup.css') }}" rel="stylesheet">
    <style>
    .disabledDiv {
        opacity: 0.5;
        pointer-events: none;
    }
</style>
</head>
<div class="content-wrapper">
    <div class="row">
        <!-- <form autocomplete="off" id="form" action="/add-family-member" method="POST" enctype="multipart/form-data"> -->
            <form id="form">
                @csrf
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="id" placeholder="Frist Name" name="id" required value="new" style="display: none;">
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label for="salutation" class="col-form-label">Salutation<span style="color:#ff0000">*</span></label>
                            <select id="salutation" class="form-control" name="salutation" required>
                                <option class="form-control" value="">Select</option>
                                <option class="form-control" value="Mr" selected="selected">Mr</option>
                                <option class="form-control" value="Late Mr">Late Mr</option>
                                <option class="form-control" value="Mrs">Mrs</option>
                                <option class="form-control" value="Late Mrs">Late Mrs</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label for="" class="col-form-label">First Name<span style="color:#ff0000">*</span></label>
                            <input type="text" class="form-control" id="create_family_first_name" placeholder="Frist Name" name="first_name" required>
                        </div>
                        <div class="col-sm-2">
                            <label for="" class="col-form-label">Middle Name</label>
                            <input type="text" class="form-control" id="middle_name" placeholder="Middle Name" name="middle_name">
                        </div>
                        <div class="col-sm-2">
                            <label for="last_name" class="col-form-label">Surname<span style="color:#ff0000">*</span></label>
                            <input class="form-control" id="create_family_last_name" placeholder="Surname" name="last_name" value="{{$userInfo->last_name}}" required>
                        </div>
                        <!-- </div> -->
                        <!-- <div class="form-group row"> -->
                            <div class="col-sm-2">
                                <label for="" class="col-form-label">Date of Birth</label>
                                <input type="date" class="form-control" id="dob" placeholder="DOB" name="dob">
                            </div>
                            <div class="col-sm-2">
                                <label for="" class="col-form-label">Native Place</label>
                                <input class="form-control" id="native_place" placeholder="Native Place" value="{{$userInfo->native_place}}" name="native_place">
                            </div>

                        </div>
                        <!-- </div> -->

                        <!-- <div class="col-sm-5"> -->
                            <div class="form-group row">
                                <div class="col-sm-2 disableOnEdit" >
                                    <label for="" class="col-form-label">Relation<span style="color:#ff0000">*</span></label>
                                    <select id="relation" class="form-control" name="relation" required>
                                        <option class="form-control" value="">Select Relation</option>
                                        <option class="form-control" value="Father" selected="selected">Father</option>
                                        <option class="form-control" value="Mother">Mother</option>
                            <!-- <option class="form-control" value="Husband">Husband</option>
                            <option class="form-control" value="Wife">Wife</option>
                            <option class="form-control" value="Brother">Brother</option>
                            <option class="form-control" value="Sister">Sister</option> -->

                        </select>
                    </div>
                    <div class="col-sm-3 disableOnEdit" >
                        <label for="" class="col-form-label">Relation With<span style="color:#ff0000">*</span></label>
                        <select id="relation_with" class="form-control" name="relation_with" required>
                            <option class="form-control" value="">Select Relation With</option>
                            @foreach ($data['userFamilyMembers'] as $member)
                            <option class="form-control" value="{{$member->id}}">{{$member->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-4 disableOnEdit" >
                        <label for="" class="col-form-label">Gender<span style="color:#ff0000">*</span></label>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-check">
                                    <input class="form-check-input position-static" type="radio" name="gender" id="gender" value="1" required checked>
                                    <label class="form-check-label" for="inlineRadio1">Male</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-check">
                                    <input class="form-check-input position-static" type="radio" name="gender" id="gender" value="2">
                                    <label class="form-check-label" for="inlineRadio1">Female</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- </div> -->
                    <!-- <div class="form-group row"> -->
                        <div class="col-sm-3">
                            <label for="relation" class="col-form-label"  style="color:#ff0000; width: 100%; visibility:hidden; ">Relation</label>
                            <button type="reset" class="btn btn-danger" onClick="resetFormReset()">Reset</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Salutation</th>
          <th scope="col">First Name</th>
          <th scope="col">Middle Name</th>
          <th scope="col">Last Name</th>
          <th scope="col">Relation</th>
          <th scope="col">Relation with</th>
          <th scope="col">DOB</th>
          <th scope="col">Native Place</th>
          <th scope="col">Action</th>
      </tr>
  </thead>
  <tbody id="table-body">
    @foreach ($data['my_family_tree'] as $family)
    <tr>
        <td>{{$family->salutation}}</td>
        <td>{{$family->first_name}}</td>
        <td>{{$family->middle_name}}</td>
        <td>{{$family->last_name}}</td>
        <td>{{$family->relation}}</td>
        <td>{{$family->relation_with}}</td>
        <?php
        if($family->dob != null){
            $newDate = date("d-m-Y", strtotime($family->dob));
        }
        else{
            $newDate = "";
        }
        ?>
        <td>{{$newDate}}</td>
        <!-- <td>{{$family->dob}}</td> -->
        <td>{{$family->native_place}}</td>
        <td class="text-left">
            <i class="fa fa-trash" aria-hidden="true" onclick="deleteFamily('{{$family->id}}', '{{$family->relation}}')" style="cursor: pointer;"></i>&nbsp;&nbsp;
            <i class="fa fa-pencil-square-o" aria-hidden="true" onclick="getAllFamily('{{$family->id}}', '{{$family->relation}}')" style="cursor: pointer;"></i>
        </td>
    </tr>
    @endforeach
</tbody>
</table>
<button id="showCheckModal" type="button" class="btn btn-primary hide" data-toggle="modal" data-target="#exampleModal"></button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title float-left" id="exampleModalLabel" style="float: left;">Users Already Exist</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="width: 100%; float:left; background-color: #f5f8fa; max-height: 700px; overflow: auto;">
                <div id="matchedProfile"> </div>
            </div>
            <div class="modal-footer" style="width: 100%; float:left; background-color: #f5f8fa;">
                <button type="submit" value="submit" id="submit_button" class="btn btn-secondary" data-dismiss="modal" onclick="submitFamily()">Add Manually</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section ('script')


<script>
    formValues = null;

    function resetFormReset() {
        $(".disableOnEdit").removeClass("disabledDiv");
    }

    function checkUserInfo() {

        let maleImage = null;
        let femaleImage = null;
        let userId = "{{ $user->id }}";
        let matchedUser = null;

        var first_name = document.getElementById('create_family_first_name').value;
        var last_name = document.getElementById('create_family_last_name').value;
        var isNew = document.getElementById('id').value == 'new'
        if (first_name != "" && last_name != "" && isNew) {
            $.ajax({
                method: "GET",
                url: "/check-my-family-user",
                dataType: "json",
                data: {
                    first_name: first_name,
                    last_name: last_name
                },
                success: function(data) {
                    console.log(data);
                    if (data && data.data && data.data.user_exist == true) {
                        data = data.data;
                        var html = "";
                        for (var i = 0; i < data.user_list.length; i++) {
                            console.log(data.user_list[i].photo_path);
                            let photo_path = data.user_list[i].photo_path;
                            if(data.user_list[i].photo_path == null){
                                if (data.gender_id == 1) {
                                    photo =`<img style="max-width: 300px; height: 241px;" src="https://test.dsvkendriya.org/images/icon_user_1.png"></img>`;
                                } else {
                                    photo =`<img style="max-width: 300px; height: 241px;" src="https://test.dsvkendriya.org/images/icon_user_2.png"></img>`;
                                }
                            }
                            else {
                                photo = `<img style="max-width: 241px; height: 241px;" src="${photo_path}"></img>`;
                            }

                            html += `<div class="panel panel-default" style="width: 48%; float:left; margin-right: 5px;">
                            <div class="panel-heading text-center">
                            ${photo}
                            </div>
                            <div class="panel-body">
                            <h3 class="panel-title">${data.user_list[i].first_name} ${data.user_list[i].middle_name ?? ''} ${data.user_list[i].last_name ?? ''}</h3>
                            <div>DOB : ${data.user_list[i].dob ? data.user_list[i].dob : ''}</div>
                            <hr style="margin: 5px 0;">
                            <div>Father : ${data.user_list[i].fatherName == null ? '' : data.user_list[i].fatherName}</div>
                            <div>Mother : ${data.user_list[i].motherName == null ? '' : data.user_list[i].motherName}</div>
                            </div>
                            <button value="${data.user_list[i].id}" onclick="submitFamily('${data.user_list[i].id}')" class="btn btn-secondary" style="margin: 5px;">Select This User</button>
                            </div>`;
                        }
                        $('#matchedProfile').html(html);
                        $('#showCheckModal').trigger("click");
                    } else {
                        submitFamily();
                    }
                }
            });
        } else {
            submitFamily();
        }
    }

    function submitFamily(id = 'new') {
        let html = ``;
        $.ajax({
            method: "POST",
            url: "/add-family-member/"+id,
            dataType: "json",
            data: formValues,
            success: function(data) {
                location.reload(true);
            }
        });
    }

    function getAllFamily(familyId, relation) {
        $.ajax({
            method: "GET",
            url: "/getfamilylist",
            dataType: "json",
            success: function(data) {
                if (data && data.data && data.data.list) {
                    data.data.list.forEach(family => {
                        if (family.id == familyId) {
                            document.getElementById('id').value = family.id;
                            document.getElementById('salutation').value = family.salutation;
                            document.getElementById('create_family_first_name').value = family.first_name;
                            document.getElementById('middle_name').value = family.middle_name;
                            document.getElementById('create_family_last_name').value = family.last_name;
                            document.getElementById('dob').value = family.dob;
                            document.getElementById('native_place').value = family.native_place;

                            document.getElementById('relation').value = family.relation;
                            document.getElementById('relation_with').value = family.relation_with_id;
                            if (family.relation == 'Father') {
                                document.getElementById('gender').value = '1';
                            } else {
                                document.getElementById('gender').value = '2';
                            }

                            $(".disableOnEdit").addClass("disabledDiv");
                        }
                    })
                }
            }
        });
    }

    $(document).ready(function() {
        $("#form").on("submit", function(event) {
            event.preventDefault();
            formValues = $(this).serialize();
            checkUserInfo();
        });
    });

    $('#relation').change(function(){
        if($('#relation').val() == "Father"){
            $("input[name=gender][value=1]").prop('checked', true);
        }
        else{
            $("input[name=gender][value=2]").prop('checked', true);
        }
    });

    $('#salutation').change(function(){
        if($('#salutation').val() == "Mr" || $('#salutation').val() == "Late Mr"){
            $('#relation').val('Father');
            $("input[name=gender][value=1]").prop('checked', true);
        }
        else{
            $('#relation').val('Mother');
            $("input[name=gender][value=2]").prop('checked', true);
        }
    });

    function deleteFamily(user_id, relation) {
        var r = confirm("Are sure want to delete ?");
        if (r == true) {
            $.ajax({
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/delete-family-member",
                dataType: "json",
                data: { user_id, relation },
                success: function(data) {
                    location.reload(true);
                }
            });
        }
    }
</script>
@endsection
@extends('layouts.app')
@section('content')
<!-- <div class="content-wrapper">
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-4">
               <h1>Family List</h1>
            </div>
            <div class="col-md-4">
               <a class="btn btn-success" href="{{route('family.create', ['tab' => 'family_head_form'])}}">Add Family</a>
            </div>
         </div>
      </div>
   </section>
</div> -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>Family Head List</h1>
                </div>
                <div class="col-md-4">
                <a class="btn btn-success" href="{{route('family.create', ['tab' => 'family_head_form'])}}">Add Family</a>
                </div>
                <!-- <div class="col-sm-4">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Student List</li>
                    </ol>
                </div> -->
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-12">

                <div class="card">
                    <!-- <div class="card-header">
                        <h3 class="card-title">Follow Ups</h3>
                    </div> -->
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="table-responsive">

                            <table class="table dataTable table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <!-- <th scope="col">Action</th> -->
                                        <!-- <th scope="col">Student ID</th> -->
                                        <th scope="col">Name</th>
                                        <th scope="col">mobile</th>
                                        <th scope="col">Address</th>
                                        <!-- <th scope="col">Count</th> -->

                                    </tr>
                                    <!-- <tr>
                                        
                                        <th rowspan="1" colspan="1">
                                            <input type="text" placeholder="search by name" class="search form-control form-control-sm" id="search1" name="search[name]" onkeyup="searchParams()">
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            <input type="text" placeholder="search by email" class="search form-control form-control-sm" id="search2" name="search[email]" onkeyup="searchParams()">
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            <input type="text" placeholder="search by address" class="search form-control form-control-sm" id="search3" name="search[address]" onkeyup="searchParams()">
                                        </th>
                                        <th rowspan="1" colspan="1">
                                            <input type="text" placeholder="search by count" class="search form-control form-control-sm" id="search4" name="search[count]" onkeyup="searchParams()">
                                        </th>



                                    </tr> -->
                                </thead>
                                <tbody id="list_table">

                                </tbody>
                            </table>
                            <div id="pager" class="float-right">
                              <ul id="pagination" class="pagination-sm"></ul>
                            </div>
                      </div>

                  </div>
                  <!-- /.card-body -->
              </div>
              <!-- /.card -->
          </div>
          <!-- /.col -->
      </div>
      <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function() {


    $.ajaxSetup({
      headers: {
        'Accept': "application/json"
    }
});
    searchParams();

});
  var $pagination = $('#pagination'),
  totalRecords = 0,
  records = [],
  displayRecords = [],
  recPerPage = 10,
  page = 1,
  visiblePages = 1,
  totalPages = 0,
  maxVisiblePages = 10,
  allDataCalled = false;

  function searchParams() {
    //  var params = data.next_page_url;
    //  console.log(data);
    var params = '?page=' + page;

    element = document.getElementsByClassName('search');
    for (var i = 0; i < element.length; i++) {
      params = params + '&' + element[i].name + '=' + element[i].value;
  }
    //params = encodeURIComponent(params);
    //console.log(params);
    $.get('/family/' + params, function(data, status) {
      records = [];
      displayRecords = [];
      records = data.data.data;
      displayRecords = records;
      console.log(displayRecords);
      totalRecords = data.data.total;
      if (!allDataCalled) {
        allRecordsTotal = totalRecords;
        allDataCalled = true;
    }
    totalPages = data.data.last_page;
    visiblePages = totalRecords / recPerPage + (totalRecords % recPerPage == 0 ? 0 : 1);
    visiblePages = visiblePages > maxVisiblePages ? maxVisiblePages : visiblePages;

    apply_pagination();
    generate_table();


});
}



function generate_table() {

    var tr;
    $('#list_table').html('');
    for (var i = 0; i < displayRecords.length; i++) {
      tr = $('<tr/>');

      tr.append(" <td><a href='users/" + displayRecords[i].id + "'> " + displayRecords[i].name + "</td>");
        if (displayRecords[i].mobile){
            tr.append("<td>" + displayRecords[i].mobile + "</td>");
        }
        else {
            tr.append("<td> - </td>");
        }
        if (displayRecords[i].city && displayRecords[i].current_state) {
            tr.append("<td>" + displayRecords[i].city +" , "+ displayRecords[i].current_state + "</td>");
        }
        else {
            tr.append("<td> - </td>");
        }

     // tr.append("<td>" + displayRecords[i].name + "</td>");

      $('#list_table').append(tr);}
  }

  function apply_pagination() {

    $pagination.twbsPagination({
      totalPages: totalPages,
      visiblePages: 3,
      next: 'Next',
      prev: 'Prev',

     onPageClick: function(event, p) {
        displayRecordsIndex = Math.max(p - 1, 0) * recPerPage;
        endRec = (displayRecordsIndex) + recPerPage;
        page = p;
        displayRecords = records.slice(displayRecordsIndex, endRec);
        searchParams();
        generate_table();
    }

});
}
</script>
@endsection

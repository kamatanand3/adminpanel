@extends('layouts.app')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Family Form</h2>
                    <br>
                </div>
            </div>
        </div>
    </section> -->
    <section>
        <?php
        //dd($head_id);
        ?>
        <div class="row">
            <!-- <div class="col-md-2">@include('AreaHead.partials.nav_tabs')</div> -->
            <div class="col-md-12">
                <div class="row">
                    <form autocomplete="off" id="form" action="/family/store" method="POST" enctype="multipart/form-data">
                        @csrf
                        @includeWhen(request('tab') == 'family_head_form', 'AreaHead.partials.family-head-form')
                        @includeWhen(request('tab') == 'spouse_form', 'AreaHead.partials.spouse-form',[$head_id, $categories])
                        @includeWhen(request('tab') == 'children_form', 'AreaHead.partials.children-form', [$head_id, $categories])
                        @includeWhen(request('tab') == 'other_form', 'AreaHead.partials.other-form')
                        <div class="text-left">
                            @if (request('tab') == 'family_head_form')
                            <button type="submit" class="btn btn-primary">Save</button>
                            <!-- <button type="submit" class="btn btn-primary">Save & Go To Spouse Form</button> -->
                            @elseif (request('tab') == 'spouse_form')
                            <button id="spouse_submit" type="submit" class="btn btn-primary">Save</button>
                            <!-- <button type="submit" class="btn btn-primary">Save & Go To Children Form</button> -->
                            <!-- <a class="btn btn-primary" href="{{route('family.create',['tab' => 'children_form'])}}">skip</a> -->
                            @elseif (request('tab') == 'children_form')
                            <!-- <button type="submit" class="btn btn-primary" name="add_child" value="yes">Save & Add Another Child</button> -->
                            <!-- <button type="submit" class="btn btn-primary" name="add_child" value="no">Save & Go To Other Form</button> -->
                            <!-- <a class="btn btn-primary" href="{{route('family.create',['tab' => 'other_form'])}}">Skip</a> -->
                            <button id="children_submit" type="submit" class="btn btn-primary">Save</button>
                            <!-- @elseif (request('tab') == 'other_form')
                            <button type="submit" class="btn btn-primary">Save & Go To List</button>
                            @else -->
                            <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                            @endif

                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection

@section('script')

<script type="text/javascript">
    function checkLastName(that) {
        if (that.value == "0") {
            document.getElementById("LastName_other").style.display = "block";
        } else {
            document.getElementById("LastName_other").style.display = "none";
        }
    }

    function openPermanentAddress() {
        if (document.getElementById("permanent_address_check").checked == false) {
            document.getElementById("permanent_address").style.display = "block";
        } else {
            document.getElementById("permanent_address").style.display = "none";
        }
    }

    function openEducationDetails(that) {
        if (that.value > 1) {
            document.getElementById("education_details").style.display = "block";
        } else {
            document.getElementById("education_details").style.display = "none";
        }
    }


    function openOccupationDetails(that) {
        if (that.value > 1) {
            document.getElementById("occupation_details").style.display = "block";
            document.getElementById("occupation_details_2").style.display = "block";

        } else {
            document.getElementById("occupation_details").style.display = "none";
            document.getElementById("occupation_details_2").style.display = "none";

        }
    }

    function openSpouseCurrentAddress() {
        if (document.getElementById("spouse_current_address_check").checked == false) {
            document.getElementById("spouse_current_address").style.display = "block";
        } else {
            document.getElementById("spouse_current_address").style.display = "none";
            document.getElementById("spouse_current_building").value = null;
            document.getElementById("spouse_current_landmark").value = null;
            document.getElementById("spouse_current_road").value = null;
            document.getElementById("spouse_current_city").value = null;
            document.getElementById("spouse_current_pincode").value = null;
            document.getElementById("spouse_current_state").value = null;
            document.getElementById("spouse_current_country").value = null;

        }
    }

    function openSpouseEducationDetails(that) {
        console.log("1");
        if (that.value > 1) {
            document.getElementById("spouse_education_details").style.display = "block";
        } else {
            document.getElementById("spouse_education_details").style.display = "none";
            document.getElementById("spouse_college").value = null;
            document.getElementById("spouse_year").value = null;
            document.getElementById("spouse_university").value = null;

        }
    }


    function openSpouseOccupationDetails(that) {
        if (that.value != '' && +(that.value) > 1 && +(that.value) < 7) {
            document.getElementById("spouse_occupation_details").style.display = "block";
            document.getElementById("spouse_occupation_details_2").style.display = "block";

        } else {
            document.getElementById("spouse_occupation_details").style.display = "none";
            document.getElementById("spouse_occupation_details_2").style.display = "none";
            document.getElementById("spouse_category").value = "";
            document.getElementById("spouse_company_name").value = null;
            document.getElementById("spouse_contact_person_name").value = null;
            document.getElementById("spouse_contact_person_mobile").value = null;
            document.getElementById("spouse_contact_person_address").value = null;


        }
    }

    function openChildrenCurrentAddress() {
        if (document.getElementById("children_current_address_check").checked == false) {
            document.getElementById("children_current_address").style.display = "block";
        } else {
            document.getElementById("children_current_address").style.display = "none";
            // document.getElementById("children_current_building").value = null;
            // document.getElementById("children_current_landmark").value = null;
            // document.getElementById("children_current_road").value = null;
            // document.getElementById("children_current_city").value = null;
            // document.getElementById("children_current_pincode").value = null;
            // document.getElementById("children_current_state").value = null;
            // document.getElementById("children_current_country").value = null;
        }
    }

    function openChildrenEducationDetails(that) {
        if (that.value > 1) {
            document.getElementById("children_education_details").style.display = "block";
        } else {
            document.getElementById("children_education_details").style.display = "none";
            document.getElementById("children_college").value = null;
            document.getElementById("children_year").value = null;
            document.getElementById("children_university").value = null;
        }
    }


    function openChildrenOccupationDetails(that) {
        if (that.value != '' && +(that.value) > 1 && +(that.value) < 7) {
            document.getElementById("children_occupation_details").style.display = "block";
            document.getElementById("children_occupation_details_2").style.display = "block";

        } else {
            document.getElementById("children_occupation_details").style.display = "none";
            document.getElementById("children_occupation_details_2").style.display = "none";
            document.getElementById("children_category").value = "";
            document.getElementById("children_company_name").value = null;
            // document.getElementById("children_contact_person_name").value = null;
            document.getElementById("children_contact_person_mobile").value = null;
            // document.getElementById("children_contact_person_address").value = null;
            document.getElementById("children_occupation_building").value = null;
            document.getElementById("children_occupation_landmark").value = null;
            document.getElementById("children_occupation_road").value = null;
            document.getElementById("children_occupation_city").value = null;
            document.getElementById("children_occupation_pincode").value = null;
            document.getElementById("children_occupation_state").value = null;
            // document.getElementById("children_occupation_country").value = null;
        }
    }

    function charityDetails(that) {
        if (that.value == 1) {

            document.getElementById("charity_yes").style.display = "block";
        } else {
            document.getElementById("charity_yes").style.display = "none";

        }
    }

    function serveDetails(that) {
        if (that.value == 1) {
            document.getElementById("serve_yes").style.display = "block";
        } else {
            document.getElementById("serve_yes").style.display = "none";
        }
    }

    function donateDetails(that) {
        if (that.value == 1) {
            document.getElementById("donate_yes").style.display = "block";
        } else {
            document.getElementById("donate_yes").style.display = "none";
        }
    }
</script>

<script>
    $(document).ready(function() {

        $("#last_name").autocomplete({
            minLength: 3,
            source: function(request, response) {
                $.ajax({
                    method: "POST",
                    url: "/getLastName",
                    dataType: "json",
                    data: {
                        lastName: request.term,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(data) {
                        console.log(data);
                        var arr = data.last_names.map(function(obj) {
                            return obj.name;
                        });
                        response(arr);

                    }
                });
            }
        });
    });

    $(document).ready(function() {
        $("#children_mobile").on('blur', function() {
            if ($('#children_mobile').val() != "") {
                $.ajax({
                    method: "GET",
                    url: "/check_mobile",
                    dataType: "json",
                    data: {
                        mobile: $('#children_mobile').val(),
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.mobile_exist == true) {
                            // $('#children_mobile').val('');
                            $("#children_submit").attr("disabled", 'disabled');
                            $('#children_mobile_exist').css('display', 'block');
                        } else {
                            $("#children_submit").attr("disabled", false);
                            $('#children_mobile_exist').css('display', 'none');
                        }
                    }

                });
            }
        });
    });
    $(document).ready(function() {
        $("#children_alternate_mobile").on('blur', function() {
            if ($('#children_alternate_mobile').val() != "") {
                $.ajax({
                    method: "GET",
                    url: "/check_mobile",
                    dataType: "json",
                    data: {
                        mobile: $('#children_alternate_mobile').val(),
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.mobile_exist == true) {
                            // $('#children_alternate_mobile').val('');
                            $("#children_submit").attr("disabled", 'disabled');
                            $('#children_alternate_mobile_exist').css('display', 'block');
                        } else {
                            $("#children_submit").attr("disabled", false);
                            $('#children_alternate_mobile_exist').css('display', 'none');
                        }
                    }

                });
            }
        });
    });

    $(document).ready(function() {
        $("#children_email").on('blur', function() {
            if ($('#children_email').val() != "") {
                $.ajax({
                    method: "GET",
                    url: "/check_email",
                    dataType: "json",
                    data: {
                        email: $('#children_email').val(),
                    },
                    success: function(data) {
                        console.log(data);
                        if (data.email_exist == true) {
                            // $('#children_email').val('');
                            $("#children_submit").attr("disabled", 'disabled');
                            $('#children_email_exist').css('display', 'block');
                        } else {
                            $("#children_submit").attr("disabled", false);
                            $('#children_email_exist').css('display', 'none');
                        }
                    }

                });
            }
        });
    });
    
    $(document).ready(function() {
        $('#children_current_country').on('change', function() {
            $('#children_current_state').html('');
            $('#children_current_city').html('');
            $.ajax({
                method: "GET",
                url: "/getState/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#children_current_state').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    $('#children_current_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    if (data && data.state) {
                        data.state.forEach(state => {
                            $('#children_current_state').append($("<option></option>")
                                        .attr("value", state.id)
                                        .text(state.name)); 
                        })
                    }

                }
            });
        });

        $('#children_current_state').on('change', function() {
            $('#children_current_city').html('');
            $.ajax({
                method: "GET",
                url: "/getCity/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#children_current_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select City')); 
                    if (data && data.city) {
                        data.city.forEach(city => {
                            $('#children_current_city').append($("<option></option>")
                                        .attr("value", city.id)
                                        .text(city.name)); 
                        })
                    }

                }
            });
        });

        $('#children_occupation_country').on('change', function() {
            $('#children_occupation_state').html('');
            $('#children_occupation_city').html('');
            $.ajax({
                method: "GET",
                url: "/getState/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#children_occupation_state').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    $('#children_occupation_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    if (data && data.state) {
                        data.state.forEach(state => {
                            $('#children_occupation_state').append($("<option></option>")
                                        .attr("value", state.id)
                                        .text(state.name)); 
                        })
                    }

                }
            });
        });

        $('#children_occupation_state').on('change', function() {
            $('#children_occupation_city').html('');
            $.ajax({
                method: "GET",
                url: "/getCity/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#children_occupation_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select City')); 
                    if (data && data.city) {
                        data.city.forEach(city => {
                            $('#children_occupation_city').append($("<option></option>")
                                        .attr("value", city.id)
                                        .text(city.name)); 
                        })
                    }

                }
            });
        });
    });
</script>
@endsection
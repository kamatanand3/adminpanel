
 <?php
                                    $user = \App\User::where('id', $head_id)->first();
                                    $userInfo = \App\UserInfo::where('user_id', $head_id)->first();
                                    //dd($userInfo);
                                    // dd($categories);

                                    ?>
<div class="form-group row">
    <label style="font-size: large;" for="" class="col-sm-12 col-form-label">Children Info</label>
    <!-- <input type="text" id="children_family_head_tab" name="tab" value="other_form" hidden> -->
    <input type="text" id="family_head_id" name="head_id" value="{{$head_id}}" hidden>

</div>

<div class="form-group row">
    
    <div class="col-sm-4">
        <label for="" class="col-form-label">First Name<span style="color:#ff0000">*</span></label>
        <input type="text" class="form-control" id="children_inputFristName" placeholder="Frist Name" name="children_first_name" value="{{old('children_first_name')}}" required>
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label">Middle Name<span style="color:#ff0000">*</span></label>
        <input type="text" class="form-control" id="children_inputMiddleName" placeholder="Middle Name" value="{{old('children_middle_name', $user->name)}}" name="children_middle_name" required>
    </div>
    <div class="col-sm-4">
        <label for="last_name" class="col-form-label">Surname<span style="color:#ff0000">*</span></label>
        <input class="form-control" id="children_last_name" placeholder="Last Name" name="children_last_name" value="{{old('children_last_name', $userInfo->last_name)}}" required>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        <label for="gotra" class="col-form-label" maxlength="10">Gotra</label>
        <input class="form-control" list="gotra" id="children_gotra" placeholder="Gotra" name="children_gotra" value="{{old('children_gotra',$userInfo->gotra)}}">
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label">Divyang</label>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_divyang" id="blankRadio1" value="1" {{ (old('children_divyang') == 1) ? 'checked' : '' }}>
                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_divyang" id="blankRadio1" value="0" {{ (old('children_divyang') == 0) || (old('children_divyang') == null) ? 'checked' : '' }}>
                    <label class="form-check-label" for="inlineRadio1">No</label>
                </div>
            </div>

        </div>
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label">Mosal</label>
        <input type="text" class="form-control" id="inputMiddleName" placeholder="Mosal" name="children_mosad" value="{{old('children_mosad',$userInfo->mosad)}}">
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        <!-- <label for="" class="col-form-label">Date of Birth</label>
        <input type="date" class="form-control" id="children_DOB" placeholder="DOB" name="children_dob" value="{{old('children_dob')}}" required> -->
        <div class="row">
            <div class="col-sm-6">
                <label for="" class="col-form-label">Date of Birth<span style="color:#ff0000">*</span></label>
                <input type="date" class="form-control" id="children_DOB" placeholder="DOB" name="children_dob" value="{{old('children_dob')}}" required>
            </div>
            <div class="col-sm-6">

                <label for="" class="col-form-label" maxlength="10">Birth Place</label>
                <input class="form-control" id="children_birth_place_1" placeholder="Birth Place" name="children_birth_place" value="{{old('children_birth_place')}}">


            </div>
        </div>
    </div>
    <div class="col-sm-4">

        <label for="" class="col-form-label">Gender<span style="color:#ff0000">*</span></label>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_gender" id="children_gender" value="1" {{ old('children_gender') == "1" ? 'checked' : '' }} required>
                    <label class="form-check-label" for="inlineRadio1">Male</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_gender" id="children_gender" value="2" {{ old('children_gender') == "2" ? 'checked' : '' }}>
                    <label class="form-check-label" for="inlineRadio1">Female</label>
                </div>
            </div>
            <!-- <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_gender" id="children_gender" value="3" {{ old('children_gender') == "3" ? 'checked' : '' }}>
                    <label class="form-check-label" for="inlineRadio1">Other</label>
                </div>
            </div> -->
        </div>
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label">Marital Status<span style="color:#ff0000">*</span></label>
        <select class="form-control" name="children_marital_status">
            <option class="form-control" value="Single" selected>Single</option>
            <option class="form-control" value="Married">Married</option>
            <option class="form-control" value="Widowed">Widowed</option>
            <option class="form-control" value="Divorced">Divorced</option>
            <option class="form-control" value="Separated">Separated</option>
        </select>
    </div>
    <!-- <label for="password" class="col-sm-2 col-md-1 col-form-label">Password: </label>
                                <div class="col-sm-4">
                                    <input id="children_password" type="password" class="form-control @error('password') is-invalid @enderror" name="children_password"  autocomplete="new-password">
                                     <small class="text-danger"></small> 
                                </div> -->
</div>

<div class="form-group row">
    <div class="col-sm-4">
        <label for="" class="col-form-label">Email</label>
        <input type="email" class="form-control" id="children_email" placeholder="Email" name="email" value="{{old('email')}}">
        <span style="color: red; display:none;" id="children_email_exist">The email already exists</span>
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-sm-4">
        <div class="row">
            <label style="margin-left: 15px;" for="" class="col-form-label">WhatsApp mobile | Display on profile?</label>
            <input type="checkbox" name="children_visibility" id="children_visibility" checked>
        </div>
        <div class="col-sm-3">
            <input type="text" class="form-control" maxlength="4" id="inputMobile_code" placeholder="Code" value="{{old('children_mobile_code', "+91")}}" name="children_mobile_code">
        </div>
        <!-- </div> -->
        <!-- <div class="col-sm-3"> -->
        <!-- <label for="" class="col-form-label">Mobile</label> -->
        <div class="col-sm-9">
            <input type="tel" class="form-control" id="children_mobile" placeholder="Mobile Number" pattern="[0-9]{10}" name="mobile" value="{{old('mobile')}}">
            <span style="color: red; display:none;" id="children_mobile_exist">The mobile already exists</span>
            @error('mobile')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

    </div>

    <div class="col-sm-4">
        <div class="row">
            <label style="margin-left: 15px;" for="" class="col-form-label">Alternate Mobile | Display on profile?</label>
            <input type="checkbox" name="children_alternate_visibility" id="children_alternate_visibility" checked>
        </div>
        <div class="col-sm-3">
            <input type="text" class="form-control" maxlength="4" id="inputMobile_code" placeholder="Code" value="{{old('children_mobile_code', "+91")}}" name="children_alternate_mobile_code">
        </div>
        <!-- </div> -->
        <!-- <div class="col-sm-3"> -->
        <!-- <label for="" class="col-form-label">Mobile</label> -->
        <div class="col-sm-9">
            <input type="tel" class="form-control" id="children_alternate_mobile" placeholder="Mobile Number" pattern="[0-9]{10}" name="alternate_mobile" value="{{old('alternate_mobile')}}">
            <span style="color: red; display:none;" id="children_alternate_mobile_exist">The mobile already exists</span>
            @error('alternate_mobile')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

    </div>


    <!-- <div class="col-sm-1">
        <label for="" class="col-form-label">Code</label>
        <input type="text" class="form-control" id="children_inputMobile_code" placeholder="Code" maxlength="4" value="{{old('children_mobile_code', '+91')}}" name="children_mobile_code">
    </div>
    <div class="col-sm-3">
        <label for="" class="col-form-label">Mobile</label>
        <input type="tel" class="form-control" id="children_inputMobile" placeholder="Mobile Number" pattern="[0-9]{10}" name="mobile" value="{{old('mobile')}}" required>
        @error('mobile')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div> -->

    <!-- <div class="col-sm-4">
        <label for="" class="col-form-label">Mobile Number Privacy</label>
        <div class="row">

            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_visibility" id="children_blankRadio2" value="1" {{ old('children_visibility') == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="inlineRadio2">Display</label>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_visibility" id="children_blankRadio2" value="0" {{ old('children_visibility') == 0 ? 'checked' : '' }}>
                    <label class="form-check-label" for="inlineRadio2">Don't Display</label>
                </div>
            </div>
        </div>
    </div> -->
</div>
<div class="form-group row">
    <div class="col-sm-4">
        <label for="" class="col-form-label" maxlength="10">Blood Group</label>
        <select class="form-control" name="children_bloodgroup">
            <option class="form-control" value="">Select Blood Group</option>
            <option class="form-control" value="A+" {{old('children_bloodgroup') == "A+" ? 'selected' : '' }}>A+</option>
            <option class="form-control" value="A-" {{old('children_bloodgroup') == "A-" ? 'selected' : '' }}>A-</option>
            <option class="form-control" value="B+" {{old('children_bloodgroup') == "B+" ? 'selected' : '' }}>B+</option>
            <option class="form-control" value="B-" {{old('children_bloodgroup') == "B-" ? 'selected' : '' }}>B-</option>
            <option class="form-control" value="O+" {{old('children_bloodgroup') == "O+" ? 'selected' : '' }}>O+</option>
            <option class="form-control" value="O-" {{old('children_bloodgroup') == "O-" ? 'selected' : '' }}>O-</option>
            <option class="form-control" value="AB+" {{old('children_bloodgroup') == "AB+" ? 'selected' : '' }}>AB+</option>
            <option class="form-control" value="AB-" {{old('children_bloodgroup') == "AB-" ? 'selected' : '' }}>AB-</option>
            <option class="form-control" value="unknown" {{old('children_bloodgroup') == "unknown" ? 'selected' : '' }}>unknown</option>
        </select>
    </div>
</div>
<hr>
<!-- <label style="font-size: medium;" for="" class="col-form-label">Current Address</label>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Block No./Building Name</label>
                                    <input type="text" class="form-control" id="children_address" placeholder="Block No./Building Name" name="children_building">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Landmark</label>
                                    <input type="text" class="form-control" id="children_address" placeholder="Landmark" name="children_landmark">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Road Name</label>
                                    <input type="text" class="form-control" id="children_address" placeholder="Road Name" name="children_road">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">City</label>
                                    <select class="form-control" name="children_city_code">
                                        <option class="form-control" value="">Select City</option>
                                        <option class="form-control" value="1">Mumbai</option>
                                        <option class="form-control" value="2">Pune</option>
                                        <option class="form-control" value="3">Nashik</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Pincode</label>
                                    <input type="number" class="form-control" id="children_address" placeholder="Pincode" name="children_pincode">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">State</label>
                                    <select class="form-control" name="children_state_code">
                                        <option class="form-control" value="">Select State</option>
                                        <option class="form-control" value="1">Maharashtra</option>
                                        <option class="form-control" value="2">Gujarat</option>
                                        <option class="form-control" value="3">Goa</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Country</label>
                                    <select class="form-control" name="children_country_code">
                                        <option class="form-control" value="">Select Country</option>
                                        <option class="form-control" value="1">India</option>
                                        <option class="form-control" value="2">US</option>
                                        <option class="form-control" value="3">UK</option>
                                    </select>
                                </div>
                            </div> -->

<label style="font-size: medium;" for="" class="col-form-label">Current Address</label>

<div class="form-group row">
    <div class="col-sm-3">
        <label style="font-size: medium;" for="" class="col-form-label">Same as Family Head</label>
        <input style="width: 17px ; height: 17px" class="" type="checkbox" id="children_current_address_check" name="children_current_address_check" onclick="openChildrenCurrentAddress()" {{ old('children_current_country') == null ? "checked" : " " }}>
    </div>
</div>


<div id="children_current_address" style="{{ old('children_current_country') == null ? 'display:none' : 'display:block'}}">
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="" class="col-form-label">Block No./Building Name</label>
            <input type="text" class="form-control" id="children_current_building" placeholder="Block No./Building Name" name="children_current_building" value="{{old('children_current_building')}}">
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Landmark</label>
            <input type="text" class="form-control" id="children_current_landmark" placeholder="Landmark" name="children_current_landmark" value="{{old('children_current_landmark')}}">
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Road Name</label>
            <input type="text" class="form-control" id="children_current_road" placeholder="Road Name" name="children_current_road" value="{{old('children_current_road')}}">
        </div>
        <!-- <div class="col-sm-3">
            <label for="" class="col-form-label">Vibhag</label>
            <select id="children_current_vibhag" class="form-control" name="children_current_vibhag">
                <option class="form-control" value="">Select Vibhag</option>
                @foreach($vibhags as $vibhag)
                <option class="form-control" value="{{$vibhag->id}}" {{ old('current_vibhag',$userInfo->current_vibhag) == $vibhag->id ? 'selected' : '' }}>{{$vibhag->name}}</option>
                @endforeach
            </select>
        </div> -->
    </div>

    <div class="form-group row">
        <div class="col-sm-3">
            <label for="" class="col-form-label">Pincode</label>
            <input type="text" class="form-control" id="children_current_pincode" pattern="^[1-9][0-9]{5}$" placeholder="Pincode" name="children_current_pincode" value="{{old('children_current_pincode')}}">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">Country</label>
            <select id="children_current_country" class="form-control" name="children_current_country">
                <option class="form-control" value="">Select Country</option>
                @foreach($countries as $country)
                <option class="form-control" value="{{$country->id}}" {{ old('children_current_country',$userInfo->current_country) == $country->id || (old('children_current_country',$userInfo->current_country) == null && $country->id == 101)? 'selected' : '' }}>{{$country->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label" maxlength="10">State</label>
            <select id="children_current_state" class="form-control" name="children_current_state">
                <option class="form-control" value="">Select State</option>
                @foreach($states as $state)
                <option class="form-control" value="{{$state->id}}" {{ old('children_current_state',$userInfo->current_state) == $state->id? 'selected' : '' }}>{{$state->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label" maxlength="10">City</label>
            <select id="children_current_city" class="form-control" name="children_current_city">
                <option class="form-control" value="">Select City</option>
                @foreach($cities as $city)
                <option class="form-control" value="{{$city->id}}" {{ old('children_current_city',$userInfo->current_city) == $city->id? 'selected' : '' }}>{{$city->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<hr>
<div class="form-group row">
    <div class="col-sm-3">
        <label for="" class="col-form-label">Education Details</label>
        <select class="form-control" name="children_education" onchange="openChildrenEducationDetails(this)">
            <option class="form-control" value="">Select Education</option>
            <option class="form-control" value="1" {{old('children_education') == "1" ? 'selected' : ''}}>Incomplete</option>
            <option class="form-control" value="2" {{old('children_education') == "2" ? 'selected' : ''}}>High-School</option>
            <option class="form-control" value="3" {{old('children_education') == "3" ? 'selected' : ''}}>Graduate</option>
            <option class="form-control" value="4" {{old('children_education') == "4" ? 'selected' : ''}}>Post-Graduate</option>
            <option class="form-control" value="5" {{old('children_education') == "5" ? 'selected' : ''}}>PhD</option>
            <option class="form-control" value="6" {{old('children_education') == "6" ? 'selected' : ''}}>Doctorate</option>
        </select>
    </div>

    <div id="children_education_details" style="{{ ((old('children_education') == "1") || (old('children_education') == null)) ? 'display:none' : 'display:block'}}">
        <div class="col-sm-3">
            <label for="" class="col-form-label">College</label>
            <input type="text" class="form-control" id="children_college" placeholder="College" name="children_college" value="{{old('children_college')}}">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">Year Of Passing</label>
            <input type="text" class="form-control" id="children_year" placeholder="Year Of Passing" name="children_year" value="{{old('children_year')}}">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">University</label>
            <input type="text" class="form-control" id="children_university" placeholder="University" name="children_university" value="{{old('children_university')}}">
        </div>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-3">
        <label for="" class="col-form-label">Occupation Details</label>
        <select class="form-control" name="children_occupation" onchange="openChildrenOccupationDetails(this)">
            <option class="form-control" value="">Select Occupation</option>
            <option class="form-control" value="1" {{old('children_occupation') == "1" ? 'selected' : '' }}>Student</option>
            <option class="form-control" value="2" {{old('children_occupation') == "2" ? 'selected' : '' }}>Intern</option>
            <option class="form-control" value="3" {{old('children_occupation') == "3" ? 'selected' : '' }}>Job</option>
            <option class="form-control" value="4" {{old('children_occupation') == "4" ? 'selected' : '' }}>Freelancer</option>
            <option class="form-control" value="5" {{old('children_occupation') == "5" ? 'selected' : '' }}>Professional</option>
            <option class="form-control" value="6" {{old('children_occupation') == "6" ? 'selected' : '' }}>Business</option>
            <option class="form-control" value="7" {{old('children_occupation') == "7" ? 'selected' : '' }}>House Person</option>
        </select>
    </div>
    <div id="children_occupation_details" style="{{ ((old('children_occupation') == "1") || (old('children_occupation',$userInfo->occupation) == "7") || (old('children_occupation') == null)) ? 'display:none' : 'display:block'}}">
        <div class="col-sm-3">
            <label for="" class="col-form-label">Industries/Profession</label>
            <select id="children_category" class="form-control" name="children_category">
                <option class="form-control" value="">Select Category</option>
                @foreach($categories as $category)
                <option class="form-control" value="{{$category->id}} {{old('children_category') == $category->id ? 'selected' : ''}}">{{$category->name}}</option>
                @endforeach
                <!-- <option class="form-control" value="1" {{old('children_category') == "1" ? 'selected' : ''}}>Manager</option>
                <option class="form-control" value="2" {{old('children_category') == "2" ? 'selected' : ''}}>Senior Employee</option>
                <option class="form-control" value="3" {{old('children_category') == "3" ? 'selected' : ''}}>Junior Employee</option> -->
            </select>
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">Company Name</label>
            <input type="text" class="form-control" id="children_company_name" placeholder="Company Name" name="children_company_name" value="{{old('children_company_name')}}">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">Company Contact</label>
            <input type="text" class="form-control" id="children_contact_person_mobile" placeholder="Mobile" pattern="[0-9]{10}" name="children_contact_person_mobile" value="{{old('children_contact_person_mobile')}}">
        </div>
    </div>
</div>

<div id="children_occupation_details_2" style="{{ ((old('children_occupation') == "1") || (old('children_occupation') == null)) ? 'display:none' : 'display:block'}}">
    <!-- <div class="form-group row"> -->
    <!-- <div class="col-sm-3">
            <label for="" class="col-form-label">Contact Person Name</label>
            <input type="text" class="form-control" id="children_contact_person_name" placeholder="Contact Person Name" name="children_contact_person_name" value="{{old('children_contact_person_name')}}">
        </div>
        
        <div class="col-sm-3">
            <label for="" class="col-form-label">Address</label>
            <input type="text" class="form-control" id="children_contact_person_address" placeholder="Address" name="children_contact_person_address" value="{{old('children_contact_person_address')}}">
        </div> -->

    <!-- </div>  -->
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="" class="col-form-label">Block No./Building Name</label>
            <input type="text" class="form-control" id="children_occupation_building" placeholder="Block No./Building Name" name="children_occupation_building" value="{{old('children_occupation_building')}}">
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Landmark</label>
            <input type="text" class="form-control" id="children_occupation_landmark" placeholder="Landmark" name="children_occupation_landmark" value="{{old('children_occupation_landmark')}}">
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Road Name</label>
            <input type="text" class="form-control" id="children_occupation_road" placeholder="Road Name" name="children_occupation_road" value="{{old('children_occupation_road')}}">
        </div>
        <!-- <div class="col-sm-3">
            <label for="" class="col-form-label">Vibhag</label>
            <select id="children_occupation_vibhag" class="form-control" name="children_occupation_vibhag">
                <option class="form-control" value="">Select Vibhag</option>
                @foreach($vibhags as $vibhag)
                <option class="form-control" value="{{$vibhag->id}}" {{ old('occupation_vibhag',$userInfo->occupation_vibhag) == $vibhag->id ? 'selected' : '' }}>{{$vibhag->name}}</option>
                @endforeach
            </select>
        </div> -->
    </div>

    <div class="form-group row">
        <div class="col-sm-3">
            <label for="" class="col-form-label">Pincode</label>
            <input type="text" class="form-control" id="children_occupation_pincode" pattern="^[1-9][0-9]{5}$" placeholder="Pincode" name="children_occupation_pincode" value="{{old('children_occupation_pincode')}}">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">Country</label>
            <select id="children_occupation_country" class="form-control" name="children_occupation_country">
                <option class="form-control" value="">Select Country</option>
                @foreach($countries as $country)
                <option class="form-control" value="{{$country->id}}" {{ $country->id == 101? 'selected' : '' }}>{{$country->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label" maxlength="10">State</label>
            <select id="children_occupation_state" class="form-control" name="children_occupation_state">
                <option class="form-control" value="">Select State</option>
                @foreach($occ_states as $state)
                <option class="form-control" value="{{$state->id}}">{{$state->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label" maxlength="10">City</label>
            <select id="children_occupation_city" class="form-control" name="children_occupation_city">
            </select>
        </div>
    </div>
    <!-- </div> -->

</div>



<!-- <div class="form-group row">
    <div class="col-sm-4">
        <label for="last_name" class="col-form-label" maxlength="10">Last Name</label>
        <input class="form-control" id="children_last_name" placeholder="Last Name" name="children_last_name">
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label">First Name</label>
        <input type="text" class="form-control" id="children_inputFristName" placeholder="Frist Name" name="children_first_name">
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label">Middle Name</label>
        <input type="text" class="form-control" id="children_inputMiddleName" placeholder="Middle Name" name="children_middle_name">
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        <label for="" class="col-form-label">Date of Birth</label>
        <input type="date" class="form-control" id="children_DOB" placeholder="DOB" name="children_dob">
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label">Gender</label>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_gender" id="children_blankRadio1" value="1">
                    <label class="form-check-label" for="inlineRadio1">Male</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_gender" id="children_blankRadio1" value="2" >
                    <label class="form-check-label" for="inlineRadio1">Female</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_gender" id="children_blankRadio1" value="3" >
                    <label class="form-check-label" for="inlineRadio1">Other</label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label" maxlength="10">Blood Group</label>
        <select class="form-control" name="children_bloodgroup">
            <option class="form-control" value="">Select Blood Group</option>
            <option class="form-control" value="A+">A+</option>
            <option class="form-control" value="A-">A-</option>
            <option class="form-control" value="B+">B+</option>
            <option class="form-control" value="B-">B-</option>
            <option class="form-control" value="O+">O+</option>
            <option class="form-control" value="O-">O-</option>
            <option class="form-control" value="AB+">AB+</option>
            <option class="form-control" value="AB-">AB-</option>
            <option class="form-control" value="unknown">unknown</option>
        </select>
    </div> -->
<!-- <label for="password" class="col-sm-2 col-md-1 col-form-label">Password: </label>
                                <div class="col-sm-4">
                                    <input id="children_password" type="password" class="form-control @error('password') is-invalid @enderror" name="children_password"  autocomplete="new-password">
                                     <small class="text-danger"></small> 
                                </div> -->
<!-- </div> -->

<!-- <div class="form-group row">
    <div class="col-sm-4">
        <label for="" class="col-form-label">Email</label>
        <input type="email" class="form-control" id="children_inputEmail" placeholder="Email" name="children_email">
    </div>
    <div class="col-sm-1">
        <label for="" class="col-form-label">Code</label>
        <input type="text" class="form-control" id="children_inputMobile_code" placeholder="Code" value="91" name="children_mobile_code">
    </div>
    <div class="col-sm-3">
        <label for="" class="col-form-label">Mobile</label>
        <input type="tel" class="form-control" id="children_inputMobile" placeholder="Mobile Number" name="children_mobile">
    </div>
    <div class="col-sm-4">
        <label for="" class="col-form-label">Mobile Number Privacy</label>
        <div class="row">

            <div class="col-sm-4">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_visibility" id="children_blankRadio2" value="1">
                    <label class="form-check-label" for="inlineRadio2">Display</label>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="form-check">
                    <input class="form-check-input position-static" type="radio" name="children_visibility" id="children_blankRadio2" value="0">
                    <label class="form-check-label" for="inlineRadio2">Don't Display</label>
                </div>
            </div>
        </div>
    </div>
</div>
<hr> -->
<!-- <label style="font-size: medium;" for="" class="col-form-label">Current Address</label>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Block No./Building Name</label>
                                    <input type="text" class="form-control" id="children_address" placeholder="Block No./Building Name" name="children_building">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Landmark</label>
                                    <input type="text" class="form-control" id="children_address" placeholder="Landmark" name="children_landmark">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Road Name</label>
                                    <input type="text" class="form-control" id="children_address" placeholder="Road Name" name="children_road">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">City</label>
                                    <select class="form-control" name="children_city_code">
                                        <option class="form-control" value="">Select City</option>
                                        <option class="form-control" value="1">Mumbai</option>
                                        <option class="form-control" value="2">Pune</option>
                                        <option class="form-control" value="3">Nashik</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Pincode</label>
                                    <input type="number" class="form-control" id="children_address" placeholder="Pincode" name="children_pincode">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">State</label>
                                    <select class="form-control" name="children_state_code">
                                        <option class="form-control" value="">Select State</option>
                                        <option class="form-control" value="1">Maharashtra</option>
                                        <option class="form-control" value="2">Gujarat</option>
                                        <option class="form-control" value="3">Goa</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Country</label>
                                    <select class="form-control" name="children_country_code">
                                        <option class="form-control" value="">Select Country</option>
                                        <option class="form-control" value="1">India</option>
                                        <option class="form-control" value="2">US</option>
                                        <option class="form-control" value="3">UK</option>
                                    </select>
                                </div>
                            </div> -->

<!-- <label style="font-size: medium;" for="" class="col-form-label">Current Address</label>

<div class="form-group row">
    <div class="col-sm-3">
        <label style="font-size: medium;" for="" class="col-form-label">Same as Family Head</label>
        <input style="width: 17px ; height: 17px" class="" type="checkbox" id="children_current_address_check" onclick="openChildrenCurrentAddress()" checked>
    </div>
</div>


<div id="children_current_address" style="display: none;">
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="" class="col-form-label">Block No./Building Name</label>
            <input type="text" class="form-control" id="children_current_address" placeholder="Block No./Building Name" name="children_current_building">
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Landmark</label>
            <input type="text" class="form-control" id="children_current_address" placeholder="Landmark" name="children_current_landmark">
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Road Name</label>
            <input type="text" class="form-control" id="children_current_address" placeholder="Road Name" name="children_current_road">
        </div>
    </div>

    <div class="form-group row">
        <div class="col-sm-3">
            <label for="" class="col-form-label">City</label>
            <input type="text" class="form-control" id="children_current_city" placeholder="City" name="children_current_city">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">Pincode</label>
            <input type="number" class="form-control" id="children_current_address" placeholder="Pincode" name="children_current_pincode">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">State</label>
            <input type="text" class="form-control" id="children_current_state" placeholder="State" name="children_current_state">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">Country</label>
            <input type="text" class="form-control" id="children_current_country" placeholder="State" name="children_current_country">
        </div>
    </div>
</div>
<hr>
<div class="form-group row">
    <div class="col-sm-3">
        <label for="" class="col-form-label">Education Details</label>
        <select class="form-control" name="children_education" onchange="openChildrenEducationDetails(this)">
            <option class="form-control" value="">Select Education</option>
            <option class="form-control" value="1">Incomplete</option>
            <option class="form-control" value="2">High-School</option>
            <option class="form-control" value="3">Graduate</option>
            <option class="form-control" value="4">Post-Graduate</option>
            <option class="form-control" value="5">PhD</option>
            <option class="form-control" value="6">Doctorate</option>
        </select>
    </div>

    <div id="children_education_details" style="display: none;">
        <div class="col-sm-3">
            <label for="" class="col-form-label">College</label>
            <input type="text" class="form-control" id="children_college" placeholder="College" name="children_college">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">Year Of Passing</label>
            <input type="text" class="form-control" id="children_year" placeholder="Year Of Passing" name="children_year">
        </div>
        <div class="col-sm-3">
            <label for="" class="col-form-label">University</label>
            <input type="text" class="form-control" id="children_university" placeholder="University" name="children_university">
        </div>
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-4">
        <label for="" class="col-form-label">Occupation Details</label>
        <select class="form-control" name="children_occupation" onchange="openChildrenOccupationDetails(this)">
            <option class="form-control" value="">Select Occupation</option>
            <option class="form-control" value="1">Student</option>
            <option class="form-control" value="2">Intern</option>
            <option class="form-control" value="3">Job</option>
            <option class="form-control" value="4">Freelancer</option>
            <option class="form-control" value="5">Professional</option>
            <option class="form-control" value="6">Business</option>
        </select>
    </div>
    <div id="children_occupation_details" style="display: none;">
        <div class="col-sm-4">
            <label for="" class="col-form-label">Category</label>
            <select class="form-control" name="children_category">
                <option class="form-control" value="">Select Category</option>
                <option class="form-control" value="1">Manager</option>
                <option class="form-control" value="2">Senior Employee</option>
                <option class="form-control" value="3">Junior Employee</option>
            </select>
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Company Name</label>
            <input type="text" class="form-control" id="children_company_name" placeholder="Company Name" name="children_company_name">
        </div>
    </div>
</div>

<div id="children_occupation_details_2" style="display: none;">
    <div class="form-group row">
        <div class="col-sm-4">
            <label for="" class="col-form-label">Contact Person Name</label>
            <input type="text" class="form-control" id="children_contact_person_name" placeholder="Contact Person Name" name="children_contact_person_name">
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Mobile</label>
            <input type="text" class="form-control" id="children_contact_person_mobile" placeholder="Mobile" name="children_contact_person_mobile">
        </div>
        <div class="col-sm-4">
            <label for="" class="col-form-label">Address</label>
            <input type="text" class="form-control" id="children_contact_person_address" placeholder="Address" name="children_contact_person_address">
        </div>
    </div>
</div> -->
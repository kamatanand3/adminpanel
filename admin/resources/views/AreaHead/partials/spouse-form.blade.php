                            
                            <?php
                                    $user = \App\User::where('id', $head_id)->first();
                                    $userInfo = \App\UserInfo::where('user_id', $head_id)->first();
                                    //dd($userInfo);
                                // dd($categories);
                                    ?>
                            <div class="form-group row">

                                <label style="font-size: large;" for="" class="col-sm-12 col-form-label">Spouse Info</label>
                                <input type="text" id="spouse_family_head_id" name="head_id" value="{{$head_id}}" hidden>

                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">First Name<span style="color:#ff0000">*</span></label>
                                    <input type="text" class="form-control" id="spouse_inputFristName" placeholder="Frist Name" name="spouse_first_name" value="{{old('spouse_first_name')}}" required>
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Middle Name<span style="color:#ff0000">*</span></label>
                                    <input type="text" class="form-control" id="spouse_inputMiddleName" placeholder="Middle Name" value="{{old('spouse_middle_name', $user->name)}}" name="spouse_middle_name" required>
                                </div>
                                <div class="col-sm-4">
                                    <label for="last_name" class="col-form-label">Surname<span style="color:#ff0000">*</span></label>
                                    <input class="form-control" id="spouse_last_name" placeholder="Surname" name="spouse_last_name" value="{{old('spouse_last_name', $userInfo->last_name)}}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="gotra" class="col-form-label" maxlength="10">Gotra</label>
                                    <input class="form-control" list="gotra" id="spouse_gotra" placeholder="Gotra" name="spouse_gotra" value="{{old('spouse_gotra', $userInfo->gotra)}}">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Divyang</label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="spouse_divyang" id="blankRadio1" value="1" {{ (old('spouse_divyang') == 1) ? 'checked' : '' }}>
                                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="spouse_divyang" id="blankRadio1" value="0" {{ (old('spouse_divyang') == 0) || (old('spouse_divyang') == null) ? 'checked' : '' }}>
                                                <label class="form-check-label" for="inlineRadio1">No</label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Mosal</label>
                                    <input type="text" class="form-control" id="inputMiddleName" placeholder="Mosal" name="spouse_mosad" value="{{old('spouse_mosad', $userInfo->mosad)}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="" class="col-form-label">Date of Birth<span style="color:#ff0000">*</span></label>
                                            <input type="date" class="form-control" id="spouse_DOB" placeholder="DOB" name="spouse_dob" value="{{old('spouse_dob')}}" required>
                                        </div>
                                        <div class="col-sm-6">

                                            <label for="" class="col-form-label">Birth Place</label>
                                            <input class="form-control" id="spouse_birth_place_1" placeholder="Birth Place" name="spouse_birth_place" value="{{old('spouse_birth_place')}}">


                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    
                                    <label for="" class="col-form-label">Gender<span style="color:#ff0000">*</span></label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="spouse_gender" required id="spouse_gender" value="1" {{ old('spouse_gender',$user->gender_id) == "2" ? 'checked' : '' }}>
                                                <label class="form-check-label" for="inlineRadio1">Male</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="spouse_gender" id="spouse_gender" value="2" {{ old('spouse_gender',$user->gender_id) == "1" ? 'checked' : '' }}>
                                                <label class="form-check-label" for="inlineRadio1">Female</label>
                                            </div>
                                        </div>
                                        <!-- <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="spouse_gender" id="spouse_gender" value="3" {{ old('spouse_gender') == "3" ? 'checked' : '' }}>
                                                <label class="form-check-label" for="inlineRadio1">Other</label>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label" maxlength="10">Blood Group</label>
                                    <select class="form-control" name="spouse_bloodgroup">
                                        <option class="form-control" value="">Select Blood Group</option>
                                        <option class="form-control" value="A+" {{old('spouse_bloodgroup') == "A+" ? 'selected' : '' }}>A+</option>
                                        <option class="form-control" value="A-" {{old('spouse_bloodgroup') == "A-" ? 'selected' : '' }}>A-</option>
                                        <option class="form-control" value="B+" {{old('spouse_bloodgroup') == "B+" ? 'selected' : '' }}>B+</option>
                                        <option class="form-control" value="B-" {{old('spouse_bloodgroup') == "B-" ? 'selected' : '' }}>B-</option>
                                        <option class="form-control" value="O+" {{old('spouse_bloodgroup') == "O+" ? 'selected' : '' }}>O+</option>
                                        <option class="form-control" value="O-" {{old('spouse_bloodgroup') == "O-" ? 'selected' : '' }}>O-</option>
                                        <option class="form-control" value="AB+" {{old('spouse_bloodgroup') == "AB+" ? 'selected' : '' }}>AB+</option>
                                        <option class="form-control" value="AB-" {{old('spouse_bloodgroup') == "AB-" ? 'selected' : '' }}>AB-</option>
                                        <option class="form-control" value="unknown" {{old('spouse_bloodgroup') == "unknown" ? 'selected' : '' }}>unknown</option>
                                    </select>
                                </div>
                                <!-- <label for="password" class="col-sm-2 col-md-1 col-form-label">Password: </label>
                                <div class="col-sm-4">
                                    <input id="spouse_password" type="password" class="form-control @error('password') is-invalid @enderror" name="spouse_password"  autocomplete="new-password">
                                     <small class="text-danger"></small> 
                                </div> -->
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Email</label>
                                    <input type="email" class="form-control" id="spouse_email" placeholder="Email" name="email" value="{{old('email')}}">
                                    <span style="color: red; display:none;" id="spouse_email_exist">The email already exists</span>
                                    @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="col-sm-4">
                                    <div class="row">
                                        <label style="margin-left: 15px;" for="" class="col-form-label">WhatsApp Mobile | Display on profile?</label>
                                        <input type="checkbox" name="spouse_visibility" id="spouse_visibility" checked>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" maxlength="4" id="inputMobile_code" placeholder="Code" value="{{old('spouse_mobile_code', "+91")}}" name="spouse_mobile_code">
                                    </div>
                                    <!-- </div> -->
                                    <!-- <div class="col-sm-3"> -->
                                    <!-- <label for="" class="col-form-label">Mobile</label> -->
                                    <div class="col-sm-9">
                                        <input type="tel" class="form-control" id="spouse_inputMobile" placeholder="Mobile Number" pattern="[0-9]{10}" name="mobile" value="{{old('mobile')}}">
                                        <span style="color: red; display:none;" id="spouse_mobile_exist">The mobile already exists</span>

                                        @error('mobile')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-sm-4">
                                    <div class="row">
                                        <label style="margin-left: 15px;" for="" class="col-form-label">Alternate Mobile | Display on profile?</label>
                                        <input type="checkbox" name="spouse_alternate_visibility" id="spouse_alternate_visibility" checked>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" maxlength="4" id="inputMobile_code" placeholder="Code" value="{{old('spouse_mobile_code',"+91")}}" name="spouse_alternate_mobile_code">
                                    </div>
                                    <!-- </div> -->
                                    <!-- <div class="col-sm-3"> -->
                                    <!-- <label for="" class="col-form-label">Mobile</label> -->
                                    <div class="col-sm-9">
                                        <input type="tel" class="form-control" id="spouse_alternate_mobile" placeholder="Mobile Number" pattern="[0-9]{10}" name="alternate_mobile" value="{{old('alternate_mobile')}}">
                                        <span style="color: red; display:none;" id="spouse_alternate_mobile_exist">The mobile already exists</span>
                                        @error('alternate_mobile')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                </div>

                                <!-- <div class="col-sm-1">
                                    <label for="" class="col-form-label">Code</label>
                                    <input type="text" class="form-control" id="spouse_inputMobile_code" placeholder="Code" value="{{old('spouse_mobile_code', '+91')}}" name="spouse_mobile_code">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Mobile</label>
                                    <input type="tel" class="form-control" id="spouse_inputMobile" placeholder="Mobile Number" pattern="[0-9]{10}" name="mobile" value="{{old('mobile')}}" required>
                                    @error('mobile')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div> -->

                                <!-- <div class="col-sm-4">
                                    <label for="" class="col-form-label">Mobile Number Privacy</label>
                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="spouse_visibility" id="spouse_blankRadio2" value="1" {{ old('spouse_visibility') == 1 ? 'checked' : '' }}>
                                                <label class="form-check-label" for="inlineRadio2">Display</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="spouse_visibility" id="spouse_blankRadio2" value="0" {{ old('spouse_visibility') == 0 ? 'checked' : '' }}>
                                                <label class="form-check-label" for="inlineRadio2">Don't Display</label>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <hr>
                            <!-- <label style="font-size: medium;" for="" class="col-form-label">Current Address</label>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Block No./Building Name</label>
                                    <input type="text" class="form-control" id="spouse_address" placeholder="Block No./Building Name" name="spouse_building">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Landmark</label>
                                    <input type="text" class="form-control" id="spouse_address" placeholder="Landmark" name="spouse_landmark">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Road Name</label>
                                    <input type="text" class="form-control" id="spouse_address" placeholder="Road Name" name="spouse_road">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">City</label>
                                    <select class="form-control" name="spouse_city_code">
                                        <option class="form-control" value="">Select City</option>
                                        <option class="form-control" value="1">Mumbai</option>
                                        <option class="form-control" value="2">Pune</option>
                                        <option class="form-control" value="3">Nashik</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Pincode</label>
                                    <input type="number" class="form-control" id="spouse_address" placeholder="Pincode" name="spouse_pincode">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">State</label>
                                    <select class="form-control" name="spouse_state_code">
                                        <option class="form-control" value="">Select State</option>
                                        <option class="form-control" value="1">Maharashtra</option>
                                        <option class="form-control" value="2">Gujarat</option>
                                        <option class="form-control" value="3">Goa</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Country</label>
                                    <select class="form-control" name="spouse_country_code">
                                        <option class="form-control" value="">Select Country</option>
                                        <option class="form-control" value="1">India</option>
                                        <option class="form-control" value="2">US</option>
                                        <option class="form-control" value="3">UK</option>
                                    </select>
                                </div>
                            </div> -->

                            <label style="font-size: medium;" for="" class="col-form-label">Current Address</label>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label style="font-size: medium;" for="" class="col-form-label">Same as Family Head</label>
                                    <input style="width: 17px ; height: 17px" class="" type="checkbox" id="spouse_current_address_check" name="spouse_current_address_check" onclick="openSpouseCurrentAddress()" {{ old('spouse_current_country') == null ? "checked" : " " }}>
                                </div>
                            </div>


                            <div id="spouse_current_address" style="{{ old('spouse_current_country') == null ? 'display:none' : 'display:block'}}">
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Block No./Building Name</label>
                                        <input type="text" class="form-control" id="spouse_current_building" placeholder="Block No./Building Name" name="spouse_current_building" value="{{old('spouse_current_building')}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Landmark</label>
                                        <input type="text" class="form-control" id="spouse_current_landmark" placeholder="Landmark" name="spouse_current_landmark" value="{{old('spouse_current_landmark')}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Road Name</label>
                                        <input type="text" class="form-control" id="spouse_current_road" placeholder="Road Name" name="spouse_current_road" value="{{old('spouse_current_road')}}">
                                    </div>
                                    <!-- <div class="col-sm-3">
                                        <label for="" class="col-form-label">Vibhag</label>
                                        <select id="spouse_current_vibhag" class="form-control" name="spouse_current_vibhag">
                                            <option class="form-control" value="">Select Vibhag</option>
                                            @foreach($vibhags as $vibhag)
                                            <option class="form-control" value="{{$vibhag->id}}" {{ old('current_vibhag',$userInfo->current_vibhag) == $vibhag->id ? 'selected' : '' }}>{{$vibhag->name}}</option>
                                            @endforeach
                                        </select>
                                    </div> -->
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Pincode</label>
                                        <input type="text" class="form-control" id="spouse_current_pincode" pattern="^[1-9][0-9]{5}$" placeholder="Pincode" name="spouse_current_pincode" value="{{old('spouse_current_pincode')}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Country</label>
                                        <select id="spouse_current_country" class="form-control" name="spouse_current_country">
                                            <option class="form-control" value="">Select Country</option>
                                            @foreach($countries as $country)
                                            <option class="form-control" value="{{$country->id}}" {{ old('spouse_current_country',$userInfo->current_country) == $country->id || (old('spouse_current_country',$userInfo->current_country) == null && $country->id == 101)? 'selected' : '' }}>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label" maxlength="10">State</label>
                                        <select id="spouse_current_state" class="form-control" name="spouse_current_state">
                                            <option class="form-control" value="">Select State</option>
                                            @foreach($states as $state)
                                            <option class="form-control" value="{{$state->id}}" {{ old('spouse_current_state',$userInfo->current_state) == $state->id? 'selected' : '' }}>{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label" maxlength="10">City</label>
                                        <select id="spouse_current_city" class="form-control" name="spouse_current_city">
                                            <option class="form-control" value="">Select City</option>
                                            @foreach($cities as $city)
                                            <option class="form-control" value="{{$city->id}}" {{ old('spouse_current_city',$userInfo->current_city) == $city->id? 'selected' : '' }}>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Education Details</label>
                                    <select class="form-control" name="spouse_education" onchange="openSpouseEducationDetails(this)">
                                        <option class="form-control" value="">Select Education</option>
                                        <option class="form-control" value="1" {{old('spouse_education') == "1" ? 'selected' : ''}}>Incomplete</option>
                                        <option class="form-control" value="2" {{old('spouse_education') == "2" ? 'selected' : ''}}>High-School</option>
                                        <option class="form-control" value="3" {{old('spouse_education') == "3" ? 'selected' : ''}}>Graduate</option>
                                        <option class="form-control" value="4" {{old('spouse_education') == "4" ? 'selected' : ''}}>Post-Graduate</option>
                                        <option class="form-control" value="5" {{old('spouse_education') == "5" ? 'selected' : ''}}>PhD</option>
                                        <option class="form-control" value="6" {{old('spouse_education') == "6" ? 'selected' : ''}}>Doctorate</option>
                                    </select>
                                </div>

                                <div id="spouse_education_details" style="{{ ((old('spouse_education') == "1") || (old('spouse_education') == null)) ? 'display:none' : 'display:block'}}">
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">College</label>
                                        <input type="text" class="form-control" id="spouse_college" placeholder="College" name="spouse_college" value="{{old('spouse_college')}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Year Of Passing</label>
                                        <input type="text" class="form-control" id="spouse_year" placeholder="Year Of Passing" name="spouse_year" value="{{old('spouse_year')}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">University</label>
                                        <input type="text" class="form-control" id="spouse_university" placeholder="University" name="spouse_university" value="{{old('spouse_university')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Occupation Details</label>
                                    <select class="form-control" name="spouse_occupation" onchange="openSpouseOccupationDetails(this)">
                                        <option class="form-control" value="">Select Occupation</option>
                                        <option class="form-control" value="1" {{old('spouse_occupation') == "1" ? 'selected' : '' }}>Student</option>
                                        <option class="form-control" value="2" {{old('spouse_occupation') == "2" ? 'selected' : '' }}>Intern</option>
                                        <option class="form-control" value="3" {{old('spouse_occupation') == "3" ? 'selected' : '' }}>Job</option>
                                        <option class="form-control" value="4" {{old('spouse_occupation') == "4" ? 'selected' : '' }}>Freelancer</option>
                                        <option class="form-control" value="5" {{old('spouse_occupation') == "5" ? 'selected' : '' }}>Professional</option>
                                        <option class="form-control" value="6" {{old('spouse_occupation') == "6" ? 'selected' : '' }}>Business</option>
                                        <option class="form-control" value="7" {{old('spouse_occupation') == "7" ? 'selected' : '' }}>House Person</option>
                                    </select>
                                </div>
                                <div id="spouse_occupation_details" style="{{ ((old('spouse_occupation') == "1") || (old('spouse_occupation') == "7") || (old('spouse_occupation') == null)) ? 'display:none' : 'display:block'}}">
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Industries/Profession</label>
                                        <select id="spouse_category" class="form-control" name="spouse_category">
                                            <option class="form-control" value="">Select Category</option>
                                            @foreach($categories as $category)
                                            <option class="form-control" value="{{$category->id}} {{old('spouse_category') == $category->id ? 'selected' : ''}}">{{$category->name}}</option>
                                            @endforeach
                                            
                                            <!-- <option class="form-control" value="1" {{old('spouse_category') == "1" ? 'selected' : ''}}>Manager</option>
                                            <option class="form-control" value="2" {{old('spouse_category') == "2" ? 'selected' : ''}}>Senior Employee</option>
                                            <option class="form-control" value="3" {{old('spouse_category') == "3" ? 'selected' : ''}}>Junior Employee</option> -->
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Company Name</label>
                                        <input type="text" class="form-control" id="spouse_company_name" placeholder="Company Name" name="spouse_company_name" value="{{old('spouse_company_name')}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Company Contact</label>
                                        <input type="text" class="form-control" id="spouse_contact_person_mobile" placeholder="Mobile" pattern="[0-9]{10}" name="spouse_contact_person_mobile" value="{{old('spouse_contact_person_mobile')}}">
                                    </div>
                                </div>
                            </div>

                            <div id="spouse_occupation_details_2" style="{{ ((old('spouse_occupation') == "1") || (old('spouse_occupation') == null)) ? 'display:none' : 'display:block'}}">
                                <!-- <div class="form-group row">
                                
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Contact Person Name</label>
                                        <input type="text" class="form-control" id="spouse_contact_person_name" placeholder="Contact Person Name" name="spouse_contact_person_name" value="{{old('spouse_contact_person_name')}}">
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Address</label>
                                        <input type="text" class="form-control" id="spouse_contact_person_address" placeholder="Address" name="spouse_contact_person_address" value="{{old('spouse_contact_person_address')}}">
                                    </div>
                                </div> -->
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Block No./Building Name</label>
                                        <input type="text" class="form-control" id="spouse_occupation_building" placeholder="Block No./Building Name" name="spouse_occupation_building" value="{{old('spouse_occupation_building')}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Landmark</label>
                                        <input type="text" class="form-control" id="spouse_occupation_landmark" placeholder="Landmark" name="spouse_occupation_landmark" value="{{old('spouse_occupation_landmark')}}">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Road Name</label>
                                        <input type="text" class="form-control" id="spouse_occupation_road" placeholder="Road Name" name="spouse_occupation_road" value="{{old('spouse_occupation_road')}}">
                                    </div>
                                    <!-- <div class="col-sm-3">
                                        <label for="" class="col-form-label">Vibhag</label>
                                        <select id="spouse_occupation_vibhag" class="form-control" name="spouse_occupation_vibhag">
                                            <option class="form-control" value="">Select Vibhag</option>
                                            @foreach($vibhags as $vibhag)
                                            <option class="form-control" value="{{$vibhag->id}}" {{ old('occupation_vibhag',$userInfo->occupation_vibhag) == $vibhag->id ? 'selected' : '' }}>{{$vibhag->name}}</option>
                                            @endforeach
                                        </select>
                                    </div> -->
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Pincode</label>
                                        <input type="text" class="form-control" id="spouse_occupation_pincode" pattern="^[1-9][0-9]{5}$" placeholder="Pincode" name="spouse_occupation_pincode" value="{{old('spouse_occupation_pincode')}}">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Country</label>
                                        <select id="spouse_occupation_country" class="form-control" name="spouse_occupation_country">
                                            <option class="form-control" value="">Select Country</option>
                                            @foreach($countries as $country)
                                            <option class="form-control" value="{{$country->id}}" {{ $country->id == 101? 'selected' : '' }}>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label" maxlength="10">State</label>
                                        <select id="spouse_occupation_state" class="form-control" name="spouse_occupation_state">
                                            <option class="form-control" value="">Select State</option>
                                            @foreach($occ_states as $state)
                                            <option class="form-control" value="{{$state->id}}">{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label" maxlength="10">City</label>
                                        <select id="spouse_occupation_city" class="form-control" name="spouse_occupation_city">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <button id="showCheckModal" type="button" class="btn btn-primary hide" data-toggle="modal" data-target="#exampleModal"></button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title float-left" id="exampleModalLabel">User Already Exist</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div id="matchedProfile"> </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Add Manually</button>
                                            <button id="sendRequestToLink" type="button" class="btn btn-primary">Send Request To Link</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @section ('script')

                            <script>
                            console.log("in script");
                                // let maleImage = '{{ extraUserPhoto("123456QWERTY%$.jpg", 1, ['style' => 'width: 100 % ; max - width: 300 px ']) }}';
                                // let femaleImage = '{{ extraUserPhoto("123456QWERTY%$.jpg", 2, ['style ' => 'width: 100 % ; max - width: 300 px ']) }}';
                                let maleImage = null;
                                let femaleImage = null;
                                let userId = "{{ $user->id }}";
                                let matchedUser = null;
                                (function() {
                                    console.log("in function");
                                    $('#spouse_inputFristName, #spouse_DOB, #spouse_inputMobile').on('blur', function() {
                                        console.log('inblur');
                                        matchedUser = null;
                                        if ($('#spouse_inputFristName').val() != '' && $('#spouse_DOB').val() != '' && $('#spouse_inputMobile').val() != '') {
                                            $.ajax({
                                                method: "GET",
                                                url: "/check_user",
                                                dataType: "json",
                                                data: {
                                                    first_name: $('#spouse_inputFristName').val(),
                                                    dob: $('#spouse_DOB').val(),
                                                    mobile: $('#spouse_inputMobile').val(),
                                                    gender_code: $("input[name='spouse_gender']:checked").val()
                                                },
                                                success: function(data) {
                                                    // console.log("data: ",data);
                                                    
                                                    if (data && data.user_already_linked === false) {
                                                        console.log(data);
                                                        let photo_path = data.photo_path;
                                                        if (photo_path == null || Object.keys(photo_path).length === 0) {
                                                            if (data.gender_id == 1) {
                                                                // photo_path = maleImage
                                                                photo_path ="";
                                                            } else {
                                                                // photo_path = femaleImage;
                                                                photo_path = "";
                                                            }
                                                        } else {
                                                            photo_path = `<img style="width: 25%;" src="${photo_path}"></img>`; // temp change
                                                        }
                                                        matchedUser = data;
                                                        let html = `<div class="panel panel-default">
                                <div class="panel-heading text-center">
                                ${photo_path}
                                </div>
                                <div class="panel-body">
                                    <h3 class="panel-title">${data.first_name} ${data.middle_name ?? ''} ${data.last_name ?? ''}</h3>
                                    <div>DOB : ${data.dob}</div>
                                    <hr style="margin: 5px 0;">
                                    <div>Father : ${(data.father_name == null) ? '' : data.father_name}</div>
                                    <div>Mother : ${(data.mother_name == null) ? '' : data.mother_name}</div>
                                </div>
                            </div>`;
                                                        $('#matchedProfile').html(html);
                                                        $('#showCheckModal').trigger("click");
                                                    }
                                                }
                                            });
                                        }
                                    });

                                    $('#sendRequestToLink').on('click', function() {
                                        $.ajax({
                                            method: "GET",
                                            url: "/link_request",
                                            dataType: "json",
                                            data: {
                                                requester: userId,
                                                request_to: matchedUser.user_id,
                                                request_for: $("input[name='spouse_gender']:checked").val() == '2' ? 'Wife' : 'Husband',
                                                module: 'Vastipatrak',
                                            },
                                            success: function(data) {
                                                window.location = "/requests";
                                            }
                                        });
                                    });
                                })();
                            </script>

                            <script type="text/javascript">
                                function openSpouseEducationDetails(that) {
                                    if (that.value > 1) {
                                        document.getElementById("spouse_education_details").style.display = "block";
                                    } else {
                                        document.getElementById("spouse_education_details").style.display = "none";
                                        document.getElementById("spouse_college").value = null;
                                        document.getElementById("spouse_year").value = null;
                                        document.getElementById("spouse_university").value = null;

                                    }
                                }

                                function openSpouseCurrentAddress() {
                                    if (document.getElementById("spouse_current_address_check").checked == false) {
                                        document.getElementById("spouse_current_address").style.display = "block";
                                    } else {
                                        document.getElementById("spouse_current_address").style.display = "none";
                                        // document.getElementById("spouse_current_building").value = null;
                                        // document.getElementById("spouse_current_landmark").value = null;
                                        // document.getElementById("spouse_current_road").value = null;
                                        // document.getElementById("spouse_current_city").value = null;
                                        // document.getElementById("spouse_current_pincode").value = null;
                                        // document.getElementById("spouse_current_state").value = null;
                                        // document.getElementById("spouse_current_country").value = null;

                                    }
                                }

                                function openSpouseOccupationDetails(that) {
                                    if (that.value != '' && +(that.value) > 1 && +(that.value) < 7) {
                                        document.getElementById("spouse_occupation_details").style.display = "block";
                                        document.getElementById("spouse_occupation_details_2").style.display = "block";

                                    } else {
                                        document.getElementById("spouse_occupation_details").style.display = "none";
                                        document.getElementById("spouse_occupation_details_2").style.display = "none";
                                        document.getElementById("spouse_category").value = "";
                                        document.getElementById("spouse_company_name").value = null;
                                        // document.getElementById("spouse_contact_person_name").value = null;
                                        document.getElementById("spouse_contact_person_mobile").value = null;
                                        // document.getElementById("spouse_contact_person_address").value = null;
                                        document.getElementById("spouse_occupation_building").value = null;
                                        document.getElementById("spouse_occupation_landmark").value = null;
                                        document.getElementById("spouse_occupation_road").value = null;
                                        document.getElementById("spouse_occupation_city").value = null;
                                        document.getElementById("spouse_occupation_pincode").value = null;
                                        document.getElementById("spouse_occupation_state").value = null;
                                        // document.getElementById("spouse_occupation_country").value = null;

                                    }
                                }
                            </script>
                            <script>
                                $(document).ready(function() {

                                    $("#last_name").autocomplete({
                                        minLength: 3,
                                        source: function(request, response) {
                                            $.ajax({
                                                method: "POST",
                                                url: "/getLastName",
                                                dataType: "json",
                                                data: {
                                                    lastName: request.term,
                                                    "_token": "{{ csrf_token() }}",
                                                },
                                                success: function(data) {
                                                    console.log(data);
                                                    var arr = data.last_names.map(function(obj) {
                                                        return obj.name;
                                                    });
                                                    response(arr);

                                                }
                                            });
                                        }
                                    });
                                });

                                $(document).ready(function() {
                                    $('#spouse_current_country').on('change', function() {
                                        $('#spouse_current_state').html('');
                                        $('#spouse_current_city').html('');
                                        $.ajax({
                                            method: "GET",
                                            url: "/getState/"+this.value,
                                            dataType: "json",
                                            success: function(data) {
                                                $('#spouse_current_state').append($("<option></option>")
                                                            .attr("value", '')
                                                            .text('Select State')); 
                                                $('#spouse_current_city').append($("<option></option>")
                                                            .attr("value", '')
                                                            .text('Select State')); 
                                                if (data && data.state) {
                                                    data.state.forEach(state => {
                                                        $('#spouse_current_state').append($("<option></option>")
                                                                    .attr("value", state.id)
                                                                    .text(state.name)); 
                                                    })
                                                }

                                            }
                                        });
                                    });

                                    $('#spouse_current_state').on('change', function() {
                                        $('#spouse_current_city').html('');
                                        $.ajax({
                                            method: "GET",
                                            url: "/getCity/"+this.value,
                                            dataType: "json",
                                            success: function(data) {
                                                $('#spouse_current_city').append($("<option></option>")
                                                            .attr("value", '')
                                                            .text('Select City')); 
                                                if (data && data.city) {
                                                    data.city.forEach(city => {
                                                        $('#spouse_current_city').append($("<option></option>")
                                                                    .attr("value", city.id)
                                                                    .text(city.name)); 
                                                    })
                                                }

                                            }
                                        });
                                    });

                                    $('#spouse_occupation_country').on('change', function() {
                                        $('#spouse_occupation_state').html('');
                                        $('#spouse_occupation_city').html('');
                                        $.ajax({
                                            method: "GET",
                                            url: "/getState/"+this.value,
                                            dataType: "json",
                                            success: function(data) {
                                                $('#spouse_occupation_state').append($("<option></option>")
                                                            .attr("value", '')
                                                            .text('Select State')); 
                                                $('#spouse_occupation_city').append($("<option></option>")
                                                            .attr("value", '')
                                                            .text('Select State')); 
                                                if (data && data.state) {
                                                    data.state.forEach(state => {
                                                        $('#spouse_occupation_state').append($("<option></option>")
                                                                    .attr("value", state.id)
                                                                    .text(state.name)); 
                                                    })
                                                }

                                            }
                                        });
                                    });

                                    $('#spouse_occupation_state').on('change', function() {
                                        $('#spouse_occupation_city').html('');
                                        $.ajax({
                                            method: "GET",
                                            url: "/getCity/"+this.value,
                                            dataType: "json",
                                            success: function(data) {
                                                $('#spouse_occupation_city').append($("<option></option>")
                                                            .attr("value", '')
                                                            .text('Select City')); 
                                                if (data && data.city) {
                                                    data.city.forEach(city => {
                                                        $('#spouse_occupation_city').append($("<option></option>")
                                                                    .attr("value", city.id)
                                                                    .text(city.name)); 
                                                    })
                                                }

                                            }
                                        });
                                    });
                                });

                                $(document).ready(function() {
                                    $("#spouse_inputMobile").on('blur', function() {
                                        if ($('#spouse_inputMobile').val() != "") {
                                            $.ajax({
                                                method: "GET",
                                                url: "/check_mobile",
                                                dataType: "json",
                                                data: {
                                                    mobile: $('#spouse_inputMobile').val(),
                                                },
                                                success: function(data) {
                                                    // console.log(data);
                                                    if (data.mobile_exist == true) {
                                                         $("#spouse_submit").attr("disabled", 'disabled');
                                                        $('#spouse_mobile_exist').css('display', 'block');
                                                    } else {
                                                        $("#spouse_submit").attr("disabled", false);
                                                        $('#spouse_mobile_exist').css('display', 'none');
                                                    }
                                                }

                                            });
                                        }
                                    });
                                });
                                $(document).ready(function() {
                                    $("#spouse_alternate_mobile").on('blur', function() {
                                        if ($('#spouse_alternate_mobile').val() != "") {
                                            $.ajax({
                                                method: "GET",
                                                url: "/check_mobile",
                                                dataType: "json",
                                                data: {
                                                    mobile: $('#spouse_alternate_mobile').val(),
                                                },
                                                success: function(data) {
                                                    // console.log(data);
                                                    if (data.mobile_exist == true) {
                                                        // $('#spouse_alternate_mobile').val('');
                                                        $("#spouse_submit").attr("disabled", 'disabled');
                                                        $('#spouse_alternate_mobile_exist').css('display', 'block');
                                                    } else {
                                                        $("#spouse_submit").attr("disabled", false);
                                                        $('#spouse_alternate_mobile_exist').css('display', 'none');
                                                    }
                                                }

                                            });
                                        }
                                    });
                                });

                                $(document).ready(function() {
                                    $("#spouse_email").on('blur', function() {
                                        if ($('#spouse_email').val() != "") {
                                            $.ajax({
                                                method: "GET",
                                                url: "/check_email",
                                                dataType: "json",
                                                data: {
                                                    email: $('#spouse_email').val(),
                                                },
                                                success: function(data) {
                                                    // console.log(data);
                                                    if (data.email_exist == true) {
                                                        // $('#spouse_email').val('');
                                                        $("#spouse_submit").attr("disabled", 'disabled');
                                                        $('#spouse_email_exist').css('display', 'block');
                                                    } else {
                                                        $("#spouse_submit").attr("disabled", false);
                                                        $('#spouse_email_exist').css('display', 'none');
                                                    }
                                                }

                                            });
                                        }
                                    });
                                });
                            </script>

                            @endsection
                            <div class="form-group row">
                                <label style="font-size: large;" for="" class="col-sm-12 col-form-label">Family Head Info</label>
                                <input type="text" id="family_head_tab" name="tab" value="spouse_form" hidden>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="last_name" class="col-form-label" maxlength="10">Last Name</label>
                                    <input class="form-control" list="lastnames" id="last_name" placeholder="Last Name" name="last_name">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">First Name</label>
                                    <input type="text" class="form-control" id="inputFristName" placeholder="Frist Name" name="first_name">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Middle Name</label>
                                    <input type="text" class="form-control" id="inputMiddleName" placeholder="Middle Name" name="middle_name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="last_name" class="col-form-label" maxlength="10">original Last Name(if any other)</label>
                                    <input class="form-control" id="last_name" placeholder="Last Name" name="last_name_original">
                                </div>
                                <div class="col-sm-4">
                                    <label for="last_name" class="col-form-label" maxlength="10">Native Place</label>
                                    <input class="form-control" id="native_place" placeholder="Native Place" name="native_place">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Date of Birth</label>
                                    <input type="date" class="form-control" id="DOB" placeholder="DOB" name="dob">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Gender</label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="gender" id="blankRadio1" value="1">
                                                <label class="form-check-label" for="inlineRadio1">Male</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="gender" id="blankRadio1" value="2" >
                                                <label class="form-check-label" for="inlineRadio1">Female</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="gender" id="blankRadio1" value="3">
                                                <label class="form-check-label" for="inlineRadio1">Other</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label" maxlength="10">Blood Group</label>
                                    <select class="form-control" name="bloodgroup">
                                        <option class="form-control" value="">Select Blood Group</option>
                                        <option class="form-control" value="A+">A+</option>
                                        <option class="form-control" value="A-">A-</option>
                                        <option class="form-control" value="B+">B+</option>
                                        <option class="form-control" value="B-">B-</option>
                                        <option class="form-control" value="O+">O+</option>
                                        <option class="form-control" value="O-">O-</option>
                                        <option class="form-control" value="AB+">AB+</option>
                                        <option class="form-control" value="AB-">AB-</option>
                                        <option class="form-control" value="unknown">unknown</option>
                                    </select>
                                </div>
                                <!-- <label for="password" class="col-sm-2 col-md-1 col-form-label">Password: </label>
                                <div class="col-sm-4">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">
                                     <small class="text-danger"></small> 
                                </div> -->
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Email</label>
                                    <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email">
                                </div>
                                <div class="col-sm-1">
                                    <label for="" class="col-form-label">Code</label>
                                    <input type="text" class="form-control" id="inputMobile_code" placeholder="Code" value="91" name="mobile_code">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Mobile</label>
                                    <input type="tel" class="form-control" id="inputMobile" placeholder="Mobile Number" name="mobile">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Mobile Number Privacy</label>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="visibility" id="blankRadio2" value="1">
                                                <label class="form-check-label" for="inlineRadio2">Display</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-check">
                                                <input class="form-check-input position-static" type="radio" name="visibility" id="blankRadio2" value="0">
                                                <label class="form-check-label" for="inlineRadio2">Don't Display</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <label style="font-size: medium;" for="" class="col-form-label">Current Address</label>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Block No./Building Name</label>
                                    <input type="text" class="form-control" id="current_building" placeholder="Block No./Building Name" name="current_building">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Landmark</label>
                                    <input type="text" class="form-control" id="current_landmark" placeholder="Landmark" name="current_landmark">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Road Name</label>
                                    <input type="text" class="form-control" id="current_road" placeholder="Road Name" name="current_road">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">City</label>
                                    <input type="text" class="form-control" id="current_city" placeholder="City" name="current_city">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Pincode</label>
                                    <input type="number" class="form-control" id="current_pincode" placeholder="Pincode" name="current_pincode">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">State</label>
                                    <input type="text" class="form-control" id="current_state" placeholder="State" name="current_state">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Country</label>
                                    <input type="text" class="form-control" id="current_country" placeholder="Country" name="current_country">
                                </div>
                            </div>

                            <label style="font-size: medium;" for="" class="col-form-label">Permanent Address</label>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label style="font-size: medium;" for="" class="col-form-label">Same as current</label>
                                    <input style="width: 17px ; height: 17px" class="" type="checkbox" id="permanent_address_check" onclick="openPermanentAddress()" checked>
                                </div>
                            </div>


                            <div id="permanent_address" style="display: none;">
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Block No./Building Name</label>
                                        <input type="text" class="form-control" id="permanent_building" placeholder="Block No./Building Name" name="permanent_building">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Landmark</label>
                                        <input type="text" class="form-control" id="permanent_landmark" placeholder="Landmark" name="permanent_landmark">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Road Name</label>
                                        <input type="text" class="form-control" id="permanent_road" placeholder="Road Name" name="permanent_road">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">City</label>
                                        <input type="text" class="form-control" id="permanent_city" placeholder="City" name="permanent_city">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Pincode</label>
                                        <input type="number" class="form-control" id="permanent_pincode" placeholder="Pincode" name="permanent_pincode">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">State</label>
                                        <input type="text" class="form-control" id="permanent_state" placeholder="State" name="permanent_state">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Country</label>
                                        <input type="text" class="form-control" id="permanent_country" placeholder="Country" name="permanent_country">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Education Details</label>
                                    <select class="form-control" name="education" onchange="openEducationDetails(this)">
                                        <option class="form-control" value="">Select Education</option>
                                        <option class="form-control" value="1">Incomplete</option>
                                        <option class="form-control" value="2">High-School</option>
                                        <option class="form-control" value="3">Graduate</option>
                                        <option class="form-control" value="4">Post-Graduate</option>
                                        <option class="form-control" value="5">PhD</option>
                                        <option class="form-control" value="6">Doctorate</option>
                                    </select>
                                </div>

                                <div id="education_details" style="display: none;">
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">College</label>
                                        <input type="text" class="form-control" id="college" placeholder="College" name="college">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">Year Of Passing</label>
                                        <input type="text" class="form-control" id="year" placeholder="Year Of Passing" name="year">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="" class="col-form-label">University</label>
                                        <input type="text" class="form-control" id="university" placeholder="University" name="university">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Occupation Details</label>
                                    <select class="form-control" name="occupation" onchange="openOccupationDetails(this)">
                                        <option class="form-control" value="">Select Occupation</option>
                                        <option class="form-control" value="1">Student</option>
                                        <option class="form-control" value="2">Intern</option>
                                        <option class="form-control" value="3">Job</option>
                                        <option class="form-control" value="4">Freelancer</option>
                                        <option class="form-control" value="5">Professional</option>
                                        <option class="form-control" value="6">Business</option>
                                        <option class="form-control" value="7">House Person</option>
                                    </select>
                                </div>
                                <div id="occupation_details" style="display: none;">
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Category</label>
                                        <select class="form-control" name="category">
                                            <option class="form-control" value="">Select Category</option>
                                            <option class="form-control" value="1">Manager</option>
                                            <option class="form-control" value="2">Senior Employee</option>
                                            <option class="form-control" value="3">Junior Employee</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Company Name</label>
                                        <input type="text" class="form-control" id="company_name" placeholder="Company Name" name="company_name">
                                    </div>
                                </div>
                            </div>

                            <div id="occupation_details_2" style="display: none;">
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Contact Person Name</label>
                                        <input type="text" class="form-control" id="contact_person_name" placeholder="Contact Person Name" name="contact_person_name">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Mobile</label>
                                        <input type="text" class="form-control" id="contact_person_mobile" placeholder="Mobile" name="contact_person_mobile">
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="" class="col-form-label">Address</label>
                                        <input type="text" class="form-control" id="contact_person_address" placeholder="Address" name="contact_person_address">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label style="font-size: large;" for="" class="col-sm-12 col-form-label">Other Info Info</label>
                                <!-- <input type="text" id="family_head_tab" name="tab" value="done" hidden> -->
                            </div>
                            
                            <div class="form-group row">
                                <label for="" class="col-sm-6 col-form-label">Any family members is associated with Charitable / Social / Organization ?</label>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="charityDetails(this)" id="charity_option1" name="charity" value="1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="charityDetails(this)" id="charity_option2" name="charity" value="0">
                                        <label class="form-check-label" for="inlineRadio1">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="charity_yes" style="display: none;">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="charity_organization" placeholder="Charity Organization" name="charity_organization">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="charity_designation" placeholder="Charity Designation" name="charity_designation">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-6 col-form-label">Ready to Serve Society or Sanstha ?</label>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="serveDetails(this)" id="serve_option1" name="serve" value="1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="serveDetails(this)" id="serve_option2" name="serve" value="0">
                                        <label class="form-check-label" for="inlineRadio1">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="serve_yes" style="display: none;">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="serve_help" placeholder=" Are you ready to provide Help" name="serve_help">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-6 col-form-label">Ready to Donate Eye / Organ / Body ?</label>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="donateDetails(this)" id="donate_option1" name="donate" value="1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="donateDetails(this)" id="donate_option2" name="donate" value="0">
                                        <label class="form-check-label" for="inlineRadio1">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="donate_yes" style="display: none;">
                                <div class="col-sm-2">
                                    <div class="form-check">
                                        <input type="radio" id="eye" name="donate_part" value="eye">
                                        <label class="form-check-label" for="inlineRadio1">Eye</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-check">
                                        <input type="radio" id="organ" name="donate_part" value="organ">
                                        <label class="form-check-label" for="inlineRadio1">Organ</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-check">
                                        <input type="radio" id="body" name="donate_part" value="body">
                                        <label class="form-check-label" for="inlineRadio1">Body</label>
                                    </div>
                                </div>
                            </div>
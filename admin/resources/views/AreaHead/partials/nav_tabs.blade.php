<ul class="nav nav-pills nav-stacked">
    <li class="{{ request('tab') == null ? 'active' : '' }}">
        <a href="{{route('family.create', ['tab' => 'family_head_form'])}}">Head Form</a>
    </li>
    <li class="{{ request('tab') == 'spouse-form' ? 'active' : '' }}">
        <a href="{{route('family.create', ['tab' => 'spouse_form'])}}">Spouse Form</a>
    </li>
    <li class="{{ request('tab') == 'children-form' ? 'active' : '' }}">
        <a href="{{route('family.create',['tab' => 'children_form'])}}">Children Form</a>
    </li>
    <li class="{{ request('tab') == 'other-form' ? 'active' : '' }}">
        <a href="{{route('family.create',['tab' => 'other_form'])}}">Other</a>
    </li>
</ul>
<br>

                            <div class="form-group row">
                                <label style="font-size: large;" for="" class="col-sm-12 col-form-label">Other Info Info</label>
                                <input type="text" id="family_head_tab" name="tab" value="done" hidden>
                            </div>
                            
                            <div class="form-group row">
                                <label for="" class="col-sm-6 col-form-label">Any family members is associated with Charitable / Social / Organization ?</label>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="charityDetails()" id="charity_option1" name="charity" value="1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" id="charity_option2" name="charity" value="0">
                                        <label class="form-check-label" for="inlineRadio1">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="charity_yes" style="display: none;">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="charity_organization" placeholder="Charity Organization" name="charity_organization">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="charity_designation" placeholder="Charity Designation" name="charity_designation">
                                </div>
                            </div>
<hr>
                            <div class="form-group row">
                                <label for="" class="col-sm-6 col-form-label">Ready to Serve Society or Sanstha ?</label>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="serveDetails()" id="serve_option1" name="serve" value="1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" id="serve_option2" name="serve" value="0">
                                        <label class="form-check-label" for="inlineRadio1">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="serve_yes" style="display: none;">
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="serve_help" placeholder=" Are you ready to provide Help" name="serve_help">
                                </div>
                            </div>
<hr>
                            <div class="form-group row">
                                <label for="" class="col-sm-6 col-form-label">Ready to Donate Eye / Organ / Body ?</label>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" onclick="donateDetails()" id="donate_option1" name="donate" value="1">
                                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-check">
                                        <input type="radio" id="donate_option2" name="donate" value="0">
                                        <label class="form-check-label" for="inlineRadio1">No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row" id="donate_yes" style="display: none;">
                                <div class="col-sm-2">
                                    <div class="form-check">
                                        <input type="radio" id="eye" name="donate_part" value="eye">
                                        <label class="form-check-label" for="inlineRadio1">Eye</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-check">
                                        <input type="radio" id="organ" name="donate_part" value="organ">
                                        <label class="form-check-label" for="inlineRadio1">Organ</label>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-check">
                                        <input type="radio" id="body" name="donate_part" value="body">
                                        <label class="form-check-label" for="inlineRadio1">Body</label>
                                    </div>
                                </div>
                            </div>
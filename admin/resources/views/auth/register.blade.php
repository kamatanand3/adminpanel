@extends('layouts.app')
<?php
$request = request();
$isOTP = request('otp', 'false');
$userID = request('userID', 'null');
?>
@section('content')


<!-- Content-->
<div>
    <div class="lg:p-12 max-w-xl lg:my-20 my-20 mx-auto p-6 space-y-">
        <form class="lg:p-10 p-6 space-y-3 relative bg-white shadow-xl rounded-md" role="form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <h1 class="lg:text-2xl text-xl font-semibold mb-6"> Register </h1>

            @if ($isOTP == 'false')

            <div class="grid lg:grid-cols-2 gap-3">
                <div>
                    <label class="mb-0 {{ $errors->has('name') ? ' has-error' : '' }}"> First Name <span style="color:#ff0000">*</span></label>
                    <input id="name"  type="text" placeholder="Your Name" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" name="name" value="{{ old('name') }}" required>
                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
                <div>
                    <label class="mb-0"> Last  Name <span style="color:#ff0000">*</span></label>
                    <input id="last_name" type="text" placeholder="Last  Name" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" name="last_name" value="{{ old('last_name') }}" required>
                    @if ($errors->has('last_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div>
                <label class="mb-0"> Middle Name <span style="color:#ff0000">*</span></label>
                <input id="middle_name" type="text" placeholder="Middle Name" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" name="middle_name" value="{{ old('middle_name') }}" required>
                @if ($errors->has('middle_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('middle_name') }}</strong>
                </span>
                @endif
            </div>
            <div>
                <label class="mb-0">{{ trans('user.email') }}</label>
                <input id="email" type="email" placeholder="Info@example.com" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" name="email" value="{{ old('email') }}">
                <span style="color: red; display:none;" id="email_exist">The email already exists</span>
            </div>
            <div>
                <label class="mb-0">Mobile(WhatsApp Number)<span style="color:#ff0000">*</span></label>
                <input id="mobile_code" type="hidden" maxlength="4" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" placeholder="Code" name="mobile_code" value="{{ old('mobile_code', "+91") }}" required>

                <input id="mobile" type="tel" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" placeholder="Mobile (WhatsApp Number)" name="mobile" value="{{ old('mobile') }}" required>

                <span style="color: red; display:none;" id="mobile_exist">The mobile already exists</span>
            </div>
            <div>
                <label class="mb-0 {{ $errors->has('password') ? ' has-error' : '' }}">{{ trans('auth.password') }}<span style="color:#ff0000">*</span></label>
                <input type="password" id="password" placeholder="******" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" name="password" required>
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
            <div>
                <label class="mb-0">{{ trans('auth.password_confirmation') }}<span style="color:#ff0000">*</span></label>
                <input id="password-confirm" type="password" placeholder="******" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" name="password_confirmation" required>
            </div>
            <div>
                <label class="mb-0 {{ $errors->has('gender_id') ? ' has-error' : '' }}">{{ trans('user.gender') }}<span style="color:#ff0000">*</span></label>
                <select class="selectpicker mt-2" name="gender_id" id="gender_id">
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
            </div>
            <div>
                <label class="mb-0">Country<span style="color:#ff0000">*</span></label>
                <select id="current_country" class="mt-2" name="current_country" required>
                </select>
            </div>
            <div>
                <label class="mb-0">State<span style="color:#ff0000">*</span></label>
                <select id="current_state" class="mt-2" name="current_state" required>
                </select>
            </div>
            <div>
                <label class="mb-0">City<span style="color:#ff0000">*</span></label>
                <select id="current_city" class="mt-2" name="current_city" required>
                </select>
            </div>

            <div>
                <button id="submit_button" type="submit" class="bg-blue-600 font-semibold p-2 mt-5 rounded-md text-center text-white w-full">
                Get Started</button>
            </div>
            @endif
            @if ($isOTP == 'true')
            <div>
                <label class="mb-0 {{ $errors->has('otp') ? ' has-error' : '' }}">OTP<span style="color:#ff0000">*</span></label>
                <input id="otp" type="number" class="bg-gray-100 h-12 mt-2 px-3 rounded-md w-full" name="otp" required>
                @if ($errors->has('otp'))
                <span class="help-block">
                    <strong>{{ $errors->first('otp') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group" style="display: none">
                <label for="userID" class="col-md-3 control-label">User</label>

                <div class="col-md-6">
                    <input id="userID" type="text" class="form-control" name="userID" value="{{$userID}}">
                </div>
            </div>
            <div>
                <button id="submit_button" type="submit" class="bg-blue-600 font-semibold p-2 mt-5 rounded-md text-center text-white w-full">Verify OTP</button>
            </div>

            @endif
        </form>


    </div>
</div>




@section('script')
<script>
    $(document).ready(function() {

        $.ajax({
            method: "GET",
            url: "/getCountry",
            dataType: "json",
            success: function(data) {
                $('#current_country').append($("<option></option>")
                    .attr("value", '')
                    .text('Select Country')); 
                if (data && data.country) {
                    data.country.forEach(country => {
                        $('#current_country').append($("<option></option>")
                            .attr("value", country.id)
                            .text(country.name)); 
                    })
                    $("#current_country").val('101');
                }

            }
        });

        $('#current_country').on('change', function() {
            stateHandling(this.value);
        });
        stateHandling(101);

        $('#current_state').on('change', function() {
            $('#current_city').html('');
            $.ajax({
                method: "GET",
                url: "/getCity/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#current_city').append($("<option></option>")
                        .attr("value", '')
                        .text('Select City')); 
                    if (data && data.city) {
                        data.city.forEach(city => {
                            $('#current_city').append($("<option></option>")
                                .attr("value", city.id)
                                .text(city.name)); 
                        })
                    }

                }
            });
        });

        $("#mobile").on('blur', function() {
            if ($('#mobile').val() != "") {
                $.ajax({
                    method: "GET",
                    url: "/check_mobile",
                    dataType: "json",
                    data: {
                        mobile: $('#mobile').val(),
                    },
                    success: function(data) {
                        if (data.mobile_exist == true) {
                            // $('#mobile').val('');
                            $("#submit_button").attr("disabled", 'disabled');
                            $('#mobile_exist').css('display', 'block');
                        } else {
                            $("#submit_button").attr("disabled", false);
                            $('#mobile_exist').css('display', 'none');
                        }
                    }

                });
            }
        });

        function stateHandling(country) {
            $('#current_state').html('');
            $('#current_city').html('');
            $.ajax({
                method: "GET",
                url: "/getState/"+country,
                dataType: "json",
                success: function(data) {
                    $('#current_state').append($("<option></option>")
                        .attr("value", '')
                        .text('Select State')); 
                    $('#current_city').append($("<option></option>")
                        .attr("value", '')
                        .text('Select City')); 
                    if (data && data.state) {
                        data.state.forEach(state => {
                            $('#current_state').append($("<option></option>")
                                .attr("value", state.id)
                                .text(state.name)); 
                        })
                    }

                }
            });
        }
    });
    $(document).ready(function() {
        $("#email").on('blur', function() {
            if ($('#email').val() != "") {
                $.ajax({
                    method: "GET",
                    url: "/check_email",
                    dataType: "json",
                    data: {
                        email: $('#email').val(),
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.email_exist == true) {
                            // $('#email').val('');
                            $("#submit_button").attr("disabled", 'disabled');
                            $('#email_exist').css('display', 'block');
                        } else {
                            $("#submit_button").attr("disabled", false);
                            $('#email_exist').css('display', 'none');
                        }
                    }

                });
            }
        });
    });
</script>

@endsection
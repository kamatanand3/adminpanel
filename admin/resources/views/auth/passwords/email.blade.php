@extends('layouts.app')

@section('content')
<div id="resetPassword" class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    <form id="resetForm" class="form-horizontal" role="form" method="POST">
                        {{ csrf_field() }}

                        <div id="resetForm-mobile" class="form-group">
                            <label for="mobile" class="col-md-4 control-label">Mobile</label>

                            <div class="col-md-6">
                                <input id="mobile" type="number" class="form-control" name="mobile" required>

                                <span id="mobExists" class="help-block" style="display: none;">
                                    <strong>Mobile Number Daoes Not Exists !!</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Get OTP
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="verifyOTP" class="container" style="display: none;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Verify OTP</div>
                <div class="panel-body">
                    <form id="verifyOTPForm" class="form-horizontal" role="form" method="POST">
                        {{ csrf_field() }}

                        <div id="OTPSentMsg" class="alert alert-success">
                            OTP sent to your mobile number
                        </div>

                        <div id="pwdChange" class="alert alert-success" style="display: none;">
                            Password changed successfully !!
                        </div>

                        <div id="resetForm-otp" class="form-group">
                            <label for="otp" class="col-md-4 control-label">OTP</label>

                            <div class="col-md-6">
                                <input id="otp" type="number" class="form-control" name="otp" required>

                                <span id="otpWrong" class="help-block" style="display: none;">
                                    <strong id="otpWrongMSG">Enter Valid OTP !!</strong>
                                </span>
                            </div>
                        </div>

                        <div id="resetForm-pwd" class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required onblur="validatePwd()">

                                <span id="pwdLen" class="help-block" style="display: none;">
                                    <strong id="pwdLenMSG">The password must be at least 6 characters.</strong>
                                </span>
                            </div>
                        </div>

                        <div id="resetForm-repwd" class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required onblur="validatePwd()">

                                <span id="pwdMatch" class="help-block" style="display: none;">
                                    <strong id="pwdMatchMSG">Passwords not matching</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Verify OTP
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    $("#mobExists").hide();
    $("#resetForm-mobile").removeClass("has-error");
    $(document).ready(function() {
        $("#resetForm").on("submit", function(event) {
            $("#mobExists").hide();
            $("#resetForm-mobile").removeClass("has-error");
            event.preventDefault();
            $.ajax({
                method: "GET",
                url: "/getOTP",
                dataType: "json",
                data: {
                    mobile: $('#mobile').val(),
                },
                success: function(data) {
                    $("#resetPassword").hide();
                    $("#verifyOTP").show();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $("#mobExists").show();
                    $("#resetForm-mobile").addClass("has-error");
                }
            });
        });
        
        $("#verifyOTPForm").on("submit", function(event) {
            $("#otpWrong").hide();
            $("#OTPSentMsg").hide();
            $("#resetForm-otp").removeClass("has-error");
            event.preventDefault();

            if (validatePwd()) {
                $.ajax({
                    method: "GET",
                    url: "/verifyOTP",
                    dataType: "json",
                    data: {
                        mobile: $('#mobile').val(),
                        otp: $('#otp').val(),
                        password: $('#password').val(),
                        repassword: $('#password-confirm').val()
                    },
                    success: function(data) {
                        $("#pwdChange").show();
                        setTimeout(() => {
                            window.location = "/login";
                        }, 2000);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#resetForm-otp").addClass("has-error");
                        $("#otpWrong").show();
                        try {
                            res = JSON.parse(jqXHR.responseText)
                            $("#otpWrongMSG").text(res.message);
                        } catch (e) {
                            $("#otpWrongMSG").text('Something went wrong !!');
                        }
                    }
                });
            }
        });
    });

    function validatePwd() {
        $("#pwdMatch").hide();
        $("#resetForm-pwd").removeClass("has-error");
        $("#resetForm-repwd").removeClass("has-error");
        $("#pwdLen").hide();
        password = $('#password').val();
        repassword = $('#password-confirm').val();
        if (password && password.length < 6) {
            $("#pwdLen").show();
            $("#resetForm-pwd").addClass("has-error");
            return false;
        } else {
            if (password != repassword) {
                $("#pwdMatch").show();
                $("#resetForm-repwd").addClass("has-error");
                return false;
            }
        }
        return true;
    }

    
</script>

@endsection

@extends('layouts.app')

@section('content')

<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="../../index2.html" class="h1">DSVS</a>
  </div>
  <div class="card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form  method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="input-group mb-3 {{ $errors->has('email') ? ' has-error' : '' }}">
          <input type="email" class="form-control" placeholder="Email"  name="email" value="{{ old('email') }}" required autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
          </div>
      </div>
      @if ($errors->has('email'))
      <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
    @endif
</div>
<div class="input-group mb-3 {{ $errors->has('password') ? ' has-error' : '' }}">
  <input type="password" class="form-control" placeholder="Password" name="password" required>
  <div class="input-group-append">
    <div class="input-group-text">
      <span class="fas fa-lock"></span>
  </div>
</div>
@if ($errors->has('password'))
<span class="help-block">
    <strong>{{ $errors->first('password') }}</strong>
</span>
@endif
</div>
<div class="row">
  <div class="col-8">
    <div class="icheck-primary">
        <input id="remember" type="checkbox" name="remember" class="{{ old('remember') ? 'checked' : '' }}" >
      <label for="remember">
        Remember Me
    </label>
</div>
</div>
<!-- /.col -->
<div class="col-4">
    <button type="submit" class="btn btn-primary btn-block">
                {{ trans('auth.login') }}</button>
</div>
<!-- /.col -->
</div>
</form>

<div class="social-auth-links text-center mt-2 mb-3">
    <a href="#" class="btn btn-block btn-primary">
      <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
  </a>
  <a href="#" class="btn btn-block btn-danger">
      <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
  </a>
</div>
<!-- /.social-auth-links -->

<p class="mb-1">
    <a href="{{ route('password.request') }}">I forgot my password</a>
</p>
<p class="mb-0">
    <a href="register.html" class="text-center">Register a new membership</a>
</p>
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->
</div>
<!-- /.login-box -->

@endsection

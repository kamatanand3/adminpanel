@extends('layouts.app')
@section('content')
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/popup.css') }}" rel="stylesheet">

<div class="content-wrapper">
    <div class="row">
        <form id="form">
            @csrf
            <div class="col-sm-12">
                <div class="form-group row">
                    <div class="col-sm-6">
                        <label for="name" class="col-form-label">Name<span style="color:#ff0000">*</span></label>
                        <input type="text" class="form-control" id="name" placeholder="Name" name="name" required>
                    </div>
                    <div class="col-sm-2">
                        <label for="mobile_code" class="col-form-label">Mobile Code</label>
                        <input type="text" class="form-control" id="mobile_code" placeholder="Code" name="mobile_code">
                    </div>
                    <div class="col-sm-4">
                        <label for="mobile" class="col-form-label">Mobile</label>
                        <input type="number" class="form-control" id="mobile" placeholder="Mobile" name="mobile">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <label for="email" class="col-form-label">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" >
                    </div>
                    <div class="col-sm-8">
                        <label for="address" class="col-form-label">Address<span style="color:#ff0000">*</span></label>
                        <input type="text" class="form-control" id="address" placeholder="Email" name="address" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <label for="relation" class="col-form-label">Relation<span style="color:#ff0000">*</span></label>
                        <input type="text" class="form-control" id="relation" placeholder="Relation" name="relation" required>
                    </div>
                    <div class="col-sm-2">
                        <label for="relation" class="col-form-label"  style="color:#ff0000; width: 100%; visibility:hidden; ">Relation</label>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<table class="table">
  <thead>
    <tr>
      <th scope="col" style="width: 25%">Name</th>
      <th scope="col" style="width: 20%">Mobile</th>
      <th scope="col" style="width: 20%">Email</th>
      <th scope="col" style="width: 15%">Relation</th>
      <th scope="col" style="width: 20%">Address</th>
    </tr>
  </thead>
  <tbody id="table-body">
    @foreach ($data['overseas_family'] as $family)
        <tr>
            <td>{{$family->name}}</td>
            <td>{{$family->mobile}}</td>
            <td>{{$family->email ?? '-'}}</td>
            <td>{{$family->relation}}</td>
            <td>{{$family->address}}</td>
        </tr>
    @endforeach
  </tbody>
</table>

@endsection

@section ('script')


<script>
    $(document).ready(function() {
        $("#form").on("submit", function(event) {
            event.preventDefault();
            let html = ``;
            var formValues = $(this).serialize();
            $.ajax({
                method: "POST",
                url: "/add-overseas",
                dataType: "json",
                data: formValues,
                success: function(data) {
                    location.reload(true);
                }
            });
        });
    });
</script>
@endsection
@extends('layouts.app')

@section('title', __('user.upcoming_birthday'))

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blogs</h1>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Projects</li>
          </ol>
      </div>
  </div>
</div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Blog list</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
      </button>
  </div>
</div>
<div class="card-body p-0">
  <table class="table table-striped projects">
      <thead>
          <tr>
              <th style="width: 1%">
                  #
              </th>
              <th style="width: 20%">Name</th>
              <th style="width: 30%">category</th>
              <th style="width: 8%" class="text-center">status</th>
              <th style="width: 20%">
              </th>
          </tr>
      </thead>
      <tbody>
        @php
        $no = 1;
        @endphp
        @forelse($blogs->data as $key => $blog)
        <tr>
          <td>
           {{ $no++ }}
       </td>
       <td>
        {{ $blog->nameEn }}
      </td>
      <td>
        {{ $blog->category }}
    </td>
    
  <td class="project-state">
      <span class="badge badge-success">Active</span>
  </td>
  <td class="project-actions text-right">
      <a class="btn btn-primary btn-sm" href="#">
          <i class="fas fa-folder">
          </i>
          View
      </a>
      <a class="btn btn-info btn-sm" href="#">
          <i class="fas fa-pencil-alt">
          </i>
          Edit
      </a>
      <a class="btn btn-danger btn-sm" href="#">
          <i class="fas fa-trash">
          </i>
          Delete
      </a>
  </td>
</tr>
@empty
<tr>
    <td colspan="4">{{ __('birthday.no_upcoming', ['days' => 60]) }}</td>
</tr>
@endforelse
</tbody>
</table>
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->

</section>
<!-- /.content -->
@endsection

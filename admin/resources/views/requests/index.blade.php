<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/popup.css') }}" rel="stylesheet">
@extends('layouts.app')

@section('title', 'Request')

@section('content')

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="panel panel-default text-center">
            <div class="panel-heading text-left">
                <h3 class="panel-title">Requests To Me</h3>
            </div>
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <td>#</td>
                        <td class="text-left">Request For</td>
                        <td class="text-left">Request From</td>
                        <td class="text-left">Request Date</td>
                        <td class="text-left">Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $no = 1;
                    @endphp
                    @forelse($data['requestsToMe'] as $key => $request)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td class="text-left">{{ $request->request_for }}</td>
                        <td class="text-left">{{ $request->name }}</td>
                        <td class="text-left">{{ $request->created_at }}</td>
                        <td class="text-left">
                            @if ($request->status == 'Pending')
                            <i class="fa fa-check" aria-hidden="true" onclick="takeAction('{{$request->id}}', 'Approved')"></i> &nbsp;&nbsp;
                            <i class="fa fa-times" aria-hidden="true" onclick="takeAction('{{$request->id}}', 'Rejected')"></i>
                            @else
                            {{$request->status}}
                            @endif
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">No Requests To You</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-md-offset-1">
        <div class="panel panel-default text-center">
            <div class="panel-heading text-left">
                <h3 class="panel-title">My Requests</h3>
            </div>
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <td>#</td>
                        <td class="text-left">Request For</td>
                        <td class="text-left">Request To</td>
                        <td class="text-left">Request Date</td>
                        <td class="text-left">Request Status</td>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $no = 1;
                    @endphp
                    @forelse($data['myRequests'] as $key => $request)
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td class="text-left">{{ $request->request_for }}</td>
                        <td class="text-left">{{ $request->name }}</td>
                        <td class="text-left">{{ $request->created_at }}</td>
                        <td class="text-left">{{ $request->status }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">No Requests From You</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section ('script')
<script>

    function takeAction(request_id, action) {
        if(confirm("are you sure")) {
            $.ajax({
                method: "GET",
                url: "/action_request",
                dataType: "json",
                data: {
                    request_id: request_id,
                    action: action
                },
                success: function(data) {
                    window.location = "/requests";
                }
            });
        }
    }

</script>
@endsection

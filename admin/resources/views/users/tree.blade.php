@extends('layouts.user-profile-wide')

@section('subtitle', trans('app.family_tree'))

@section('user-content')

<?php
$childsTotal = 0;
$grandChildsTotal = 0;
$ggTotal = 0;
$ggcTotal = 0;
$ggccTotal = 0;
// foreach ($user->childs as $child) {
//     $child_id = $child->id;
//     dd($child_id);
// }
?>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/popup.css') }}" rel="stylesheet">
<div>
    @if($user->father_id !=NULL and $user->mother_id !=NULL) 
        {{ link_to_route('users.tree', '<<<<<-more', [$user->father_id], ['title' => 'more']) }}
    @elseif($user->father_id !=NULL and $user->mother_id == NULL) 
        {{ link_to_route('users.tree', '<<<<<-more', [$user->father_id], ['title' => 'more']) }}
    @elseif($user->father_id == NULL and $user->mother_id !=NULL)
        {{ link_to_route('users.tree', '<<<<<-more', [$user->mother_id], ['title' => 'more']) }}
    @endif
</div>
    <br>
        <div id="wrapper">

            @if($couple != NULL)
            <span class="label">{{ link_to_route('users.tree', $user->name, [$user->id], ['title' => $user->name.' ('.$user->gender.')']) }} &#10084; {{ link_to_route('users.tree', $couple->name, [$couple->id], ['title' => $couple->name.' ('.$couple->gender.')']) }}</span>
            @else
            <span class="label">{{ link_to_route('users.tree', $user->name, [$user->id], ['title' => $user->name.' ('.$user->gender.')']) }} </span>
            @endif

            @if ($childsCount = $user->childs->count())
            <?php $childsTotal += $childsCount ?>
            <div class="branch lv1">
                @foreach($user->childs as $child)
                <div class="entry {{ $childsCount == 1 ? 'sole' : '' }}">
                    <?php
                    if ($child->couples->isEmpty()) {
                        $child_couple = Null;
                    } else {
                        foreach ($child->couples as $child_couple) {
                            $child_couple_name = $child_couple->name;
                            $child_couple_id = $child_couple->id;
                            $child_couple_gender = $child_couple->gender;
                        }
                    }
                    ?>
                    @if($child_couple != NULL)
                    <span class="label">{{ link_to_route('users.tree', $child->name, [$child->id], ['title' => $child->name.' ('.$child->gender.')']) }} &#10084; {{ link_to_route('users.tree', $child_couple_name, [$child_couple_id], ['title' => $child_couple_name.' ('.$child_couple_gender.')']) }}</span>
                    @else
                    <span class="label">{{ link_to_route('users.tree', $child->name, [$child->id], ['title' => $child->name.' ('.$child->gender.')']) }} </span>
                    @endif

                    @if ($grandsCount = $child->childs->count())
                    <?php $grandChildsTotal += $grandsCount ?>
                    <div class="branch lv2">
                        @foreach($child->childs as $grand)
                        <div class="entry {{ $grandsCount == 1 ? 'sole' : '' }}">
                            <?php
                            if ($grand->couples->isEmpty()) {
                                $grand_couple = Null;
                            } else {
                                foreach ($grand->couples as $grand_couple) {
                                    $grand_couple_name = $grand_couple->name;
                                    $grand_couple_id = $grand_couple->id;
                                    $grand_couple_gender = $grand_couple->gender;
                                }
                            }
                            ?>
                            @if($grand_couple != NULL)
                            <span class="label">{{ link_to_route('users.tree', $grand->name, [$grand->id], ['title' => $grand->name.' ('.$grand->gender.')']) }} &#10084; {{ link_to_route('users.tree', $grand_couple_name, [$grand_couple_id], ['title' => $grand_couple_name.' ('.$grand_couple_gender.')']) }}</span>
                            @else
                            <span class="label">{{ link_to_route('users.tree', $grand->name, [$grand->id], ['title' => $grand->name.' ('.$grand->gender.')']) }}</span>
                            @endif

                            @if ($ggCount = $grand->childs->count())
                            <?php $ggTotal += $ggCount ?>
                            <div class="branch lv3">
                                @foreach($grand->childs as $gg)
                                <div class="entry {{ $ggCount == 1 ? 'sole' : '' }}">
                                    <?php
                                    if ($gg->couples->isEmpty()) {
                                        $gg_couple = Null;
                                    } else {
                                        foreach ($gg->couples as $gg_couple) {
                                            $gg_couple_name = $gg_couple->name;
                                            $gg_couple_id = $gg_couple->id;
                                            $gg_couple_gender = $gg_couple->gender;
                                        }
                                    }
                                    ?>
                                    @if($gg_couple != NULL)

                                    <span class="label">{{ link_to_route('users.tree', $gg->name, [$gg->id], ['title' => $gg->name.' ('.$gg->gender.')']) }} &#10084; {{ link_to_route('users.tree', $gg_couple_name, [$gg_couple_id], ['title' => $gg_couple_name.' ('.$gg_couple_gender.')']) }} </span>
                                    @else
                                    <span class="label">{{ link_to_route('users.tree', $gg->name, [$gg->id], ['title' => $gg->name.' ('.$gg->gender.')']) }} </span>
                                    @endif

                                    @if ($ggcCount = $gg->childs->count())
                                    <?php $ggcTotal += $ggcCount ?>
                                    <div class="branch lv4">
                                        @foreach($gg->childs as $ggc)
                                        <div class="entry {{ $ggcCount == 1 ? 'sole' : '' }}">
                                            <?php
                                            if ($ggc->couples->isEmpty()) {
                                                $ggc_couple = Null;
                                            } else {
                                                foreach ($ggc->couples as $ggc_couple) {
                                                    $ggc_couple_name = $ggc_couple->name;
                                                    $ggc_couple_id = $ggc_couple->id;
                                                    $ggc_couple_gender = $ggc_couple->gender;
                                                }
                                            }
                                            ?>
                                            @if($ggc_couple != NULL)
                                            <span class="label">{{ link_to_route('users.tree', $ggc->name, [$ggc->id], ['title' => $ggc->name.' ('.$ggc->gender.')']) }} &#10084; {{ link_to_route('users.tree', $ggc_couple_name, [$ggc_couple_id], ['title' => $ggc_couple_name.' ('.$ggc_couple_gender.')']) }} </span>
                                            @else
                                            <span class="label">{{ link_to_route('users.tree', $ggc->name, [$ggc->id], ['title' => $ggc->name.' ('.$ggc->gender.')']) }} </span>
                                            @endif

                                            @if ($ggccCount = $ggc->childs->count())
                                            <?php $ggccTotal += $ggccCount ?>
                                            <div class="branch lv5">
                                                @foreach($ggc->childs as $ggcc)
                                                <div class="entry {{ $ggccCount == 1 ? 'sole' : '' }}">
                                                    <?php
                                                            //dd($ggcc->couples);
                                                     if ($ggcc->couples->isEmpty()) {
                                                         $ggcc_couple = Null;
                                                     } else {
                                                         foreach ($ggcc->couples as $ggcc_couple) {
                                                            $ggcc_couple_name = $ggcc_couple->name;
                                                             $ggcc_couple_id = $ggcc_couple->id;
                                                             $ggcc_couple_gender = $ggcc_couple->gender;
                                                         }
                                                     }
                                                     //dd($ggcc_couple);
                                                    ?>
                                                    @if($ggcc_couple != NULL)
                                                    <span class="label">{{ link_to_route('users.tree', $ggcc->name, [$ggcc->id], ['title' => $ggcc->name.' ('.$ggcc->gender.')']) }} &#10084; {{ link_to_route('users.tree', $ggcc_couple_name, [$ggcc_couple_id], ['title' => $ggcc_couple_name.' ('.$ggcc_couple_gender.')']) }} </span>
                                                    @else
                                                    <span class="label">{{ link_to_route('users.tree', $ggcc->name, [$ggcc->id], ['title' => $ggcc->name.' ('.$ggcc->gender.')']) }} </span>
                                                    @endif
                                                </div>
                                                @endforeach
                                            </div>
                                            @endif
                                        </div>
                                        @endforeach
                                    </div>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
    <hr>
    <!-- <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-2">&nbsp;</div>
            @if ($childsTotal)
            <div class="col-md-1 text-right">{{ trans('app.child_count') }}</div>
            <div class="col-md-1 text-left"><strong style="font-size:20px">{{ $childsTotal }}</strong></div>
            @endif
            @if ($grandChildsTotal)
            <div class="col-md-1 text-right">{{ trans('app.grand_child_count') }}</div>
            <div class="col-md-1 text-left"><strong style="font-size:20px">{{ $grandChildsTotal }}</strong></div>
            @endif
            @if ($ggTotal)
            <div class="col-md-1 text-right">Great-Grandchild-Count</div>
            <div class="col-md-1 text-left"><strong style="font-size:20px">{{ $ggTotal }}</strong></div>
            @endif
            @if ($ggcTotal)
            <div class="col-md-1 text-right">G-G-Grandchild</div>
            <div class="col-md-1 text-left"><strong style="font-size:20px">{{ $ggcTotal }}</strong></div>
            @endif
            @if ($ggccTotal)
            <div class="col-md-1 text-right">G-G-G-Grandchild</div>
            <div class="col-md-1 text-left"><strong style="font-size:20px">{{ $ggccTotal }}</strong></div>
            @endif
        </div>
    </div>         -->

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2"></div>
            @if ($childsTotal)
            <div class="col-md-2" id="child" >Child : <strong style="font-size:15px">{{ $childsTotal }}</strong></div>
            @endif
            @if ($grandChildsTotal)
            <div class="col-md-2" id="grandchild" >Grandchild : <strong style="font-size:15px">{{ $grandChildsTotal }}</strong></div>
            @endif
            @if ($ggTotal)
            <div class="col-md-2" id="ggc" >Great-Grandchild : <strong style="font-size:15px">{{ $ggTotal }}</strong></div>
            @endif
            @if ($ggcTotal)
            <div class="col-md-2" id="gggc" >Great-G-Grandchild : <strong style="font-size:15px">{{ $ggcTotal }}</strong></div>
            @endif
            @if ($ggccTotal)
            <div class="col-md-2" id="ggggc" >Great-G-G-Grandchild : <strong style="font-size:15px">{{ $ggccTotal }}</strong></div>
            @endif
        </div>
    </div> 

        @endsection

        @section ('ext_css')
        <link rel="stylesheet" href="{{ asset('css/tree.css') }}">
        @endsection
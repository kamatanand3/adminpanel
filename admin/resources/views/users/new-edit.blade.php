@extends('layouts.app')
@section('content')

<div class="content-wrapper">
    <section>
        <?php
        // dd($user, $userInfo);
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <form autocomplete="off" id="form" action="{{route('users.update',['user' => $user->id])}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="form-group row">
                            <label style="font-size: large;" for="" class="col-sm-12 col-form-label">{{$user->name}} Info</label>
                        </div>

                        <div class="form-group row">

                            <div class="col-sm-4">
                                <label for="" class="col-form-label">First Name<span style="color:#ff0000">*</span></label>
                                <input type="text" class="form-control" id="inputFristName" placeholder="Frist Name" name="first_name" value="{{old('first_name',$user->name)}}" required>
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Middle Name<span style="color:#ff0000">*</span></label>
                                <input type="text" class="form-control" id="inputMiddleName" placeholder="Middle Name" name="middle_name" value="{{old('middle_name',$userInfo->middle_name)}}" required>
                            </div>
                            <div class="col-sm-4">
                                <label for="last_name" class="col-form-label">Surname<span style="color:#ff0000">*</span></label>
                                <input class="form-control" list="lastnames" id="last_name" placeholder="Surname" name="last_name" value="{{old('last_name',$userInfo->last_name)}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="last_name" class="col-form-label" maxlength="10">original Last Name(if any other)</label>
                                <input class="form-control" id="last_name" placeholder="Last Name" name="last_name_original" value="{{old('last_name_original',$userInfo->last_name_original)}}">
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label" maxlength="10">Native Place</label>
                                <input class="form-control" id="native_place_1" placeholder="Native Place" name="native_place" value="{{old('native_place',$userInfo->native_place)}}">
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label" maxlength="10">Birth Place</label>
                                <input class="form-control" id="birth_place_1" placeholder="Birth Place" name="birth_place" value="{{old('birth_place',$userInfo->birth_place)}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="gotra" class="col-form-label" maxlength="10">Gotra</label>
                                <input class="form-control" list="gotra" id="gotra" placeholder="Gotra" name="gotra" value="{{old('gotra',$userInfo->gotra)}}">
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Divyang</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input position-static" type="radio" name="divyang" id="blankRadio1" value="1" {{ (old('divyang',$userInfo->divyang) == 1) ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio1">Yes</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input position-static" type="radio" name="divyang" id="blankRadio1" value="0" {{ (old('divyang',$userInfo->divyang) == 0) || (old('divyang',$userInfo->divyang) == null) ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio1">No</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input position-static" type="radio" name="gender" id="blankRadio1" value="3" {{ old('gender',$user->gender_id) == 3 ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio1">Other</label>
                                        </div>
                                    </div> -->
                                </div>
                                <!-- <input type="text" class="form-control" id="inputFristName" placeholder="Divyang" name="divyang" value="{{old('divyang',$userInfo->divyang)}}" required> -->
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Mosal</label>
                                <input type="text" class="form-control" id="inputMiddleName" placeholder="Mosal" name="mosad" value="{{old('mosad',$userInfo->mosad)}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Date of Birth<span style="color:#ff0000">*</span></label>
                                <input type="date" class="form-control" id="DOB" placeholder="DOB" name="dob" value="{{old('dob',$user->dob)}}" required>
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Gender<span style="color:#ff0000">*</span></label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input position-static" type="radio" name="gender" id="blankRadio1" value="1" {{ old('gender',$user->gender_id) == 1 ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio1">Male</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input position-static" type="radio" name="gender" id="blankRadio1" value="2" {{ old('gender',$user->gender_id) == 2 ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio1">Female</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input position-static" type="radio" name="gender" id="blankRadio1" value="3" {{ old('gender',$user->gender_id) == 3 ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio1">Other</label>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Marital Status<span style="color:#ff0000">*</span></label>
                                <select class="form-control" name="marital_status">
                                    <option class="form-control" value="Single" {{ old('marital_status',$userInfo->marital_status) == "Single" ? 'selected' : '' }}>Single</option>
                                    <option class="form-control" value="Married" {{ old('marital_status',$userInfo->marital_status) == "Married" ? 'selected' : '' }}>Married</option>
                                    <option class="form-control" value="Widowed" {{ old('marital_status',$userInfo->marital_status) == "Widowed" ? 'selected' : '' }}>Widowed</option>
                                    <option class="form-control" value="Divorced" {{ old('marital_status',$userInfo->marital_status) == "Divorced" ? 'selected' : '' }}>Divorced</option>
                                    <option class="form-control" value="Separated" {{ old('marital_status',$userInfo->marital_status) == "Separated" ? 'selected' : '' }}>Separated</option>
                                </select>
                            </div>
                            <!-- <label for="password" class="col-sm-2 col-md-1 col-form-label">Password: </label>
                                <div class="col-sm-4">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">
                                     <small class="text-danger"></small> 
                                </div> -->
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Email</label>
                                <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="{{old('email', $user->email)}}">
                                <span style="color: red; display:none;" id="email_exist">The email already exists</span>
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>


                            <?php //dd(old('visibility',$userInfo->visibility), $userInfo->visibility);
                            ?>
                            <div class="col-sm-4">
                                <div class="row">
                                    <label style="margin-left: 15px;" for="" class="col-form-label">WhatsApp Mobile | Display on profile?</label>
                                    <input type="checkbox" name="visibility" id="visibility" {{ (old('visibility',$userInfo->visibility) == null) || (old('visibility',$userInfo->visibility) == 0) ? '' : 'checked' }}>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" maxlength="4" id="inputMobile_code" placeholder="Code" value="{{($userInfo->mobile_code == null) ? '+91' : $userInfo->mobile_code }}" name="mobile_code">
                                </div>
                                <!-- </div> -->
                                <!-- <div class="col-sm-3"> -->
                                <!-- <label for="" class="col-form-label">Mobile</label> -->
                                <div class="col-sm-9">
                                    <input type="tel" class="form-control" id="mobile" placeholder="Mobile Number" pattern="[0-9]{10}" name="mobile" value="{{old('mobile',$userInfo->mobile)}}">
                                    <span style="color: red; display:none;" id="mobile_exist">The mobile already exists</span>
                                    @error('mobile')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>

                            <div class="col-sm-4">
                                <div class="row">
                                    <label style="margin-left: 15px;" for="" class="col-form-label">Alternate Mobile | Display on profile?</label>
                                    <input type="checkbox" name="alternate_visibility" id="alternate_visibility" {{ (old('alternate_visibility',$userInfo->alternate_mobile_visibility) == null) || (old('alternate_visibility',$userInfo->alternate_mobile_visibility) == 0) ? '' : 'checked' }}>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" maxlength="4" id="inputMobile_code" placeholder="Code" value="{{($userInfo->alternate_mobile_code == null) ? '+91' : $userInfo->alternate_mobile_code }}" name="alternate_mobile_code">
                                </div>
                                <!-- </div> -->
                                <!-- <div class="col-sm-3"> -->
                                <!-- <label for="" class="col-form-label">Mobile</label> -->
                                <div class="col-sm-9">
                                    <input type="tel" class="form-control" id="alternate_mobile" placeholder="Mobile Number" pattern="[0-9]{10}" name="alternate_mobile" value="{{old('alternate_mobile',$userInfo->alternate_mobile)}}">
                                    <span style="color: red; display:none;" id="alternate_mobile_exist">The mobile already exists</span>
                                    @error('alternate_mobile')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>



                            <!-- <div class="col-sm-4">
                                <label for="" class="col-form-label">Mobile Number Privacy</label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <input class="form-check-input position-static" type="radio" name="visibility" id="blankRadio2" value="1" {{ old('visibility',$userInfo->visibility) == 1 ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio2">Display</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-check">
                                            <input class="form-check-input position-static" type="radio" name="visibility" id="blankRadio2" value="0" {{ old('visibility',$userInfo->visibility) == 0 ? 'checked' : '' }}>
                                            <label class="form-check-label" for="inlineRadio2">Don't Display</label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->



                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="" class="col-form-label" maxlength="10">Blood Group</label>
                                <select class="form-control" name="bloodgroup">
                                    <option class="form-control" value="">Select Blood Group</option>
                                    <option class="form-control" value="A+" {{ old('bloodgroup',$userInfo->blood_group) == "A+" ? 'selected' : '' }}>A+</option>
                                    <option class="form-control" value="A-" {{ old('bloodgroup',$userInfo->blood_group) == "A-" ? 'selected' : '' }}>A-</option>
                                    <option class="form-control" value="B+" {{ old('bloodgroup',$userInfo->blood_group) == "B+" ? 'selected' : '' }}>B+</option>
                                    <option class="form-control" value="B-" {{ old('bloodgroup',$userInfo->blood_group) == "B-" ? 'selected' : '' }}>B-</option>
                                    <option class="form-control" value="O+" {{ old('bloodgroup',$userInfo->blood_group) == "O+" ? 'selected' : '' }}>O+</option>
                                    <option class="form-control" value="O-" {{ old('bloodgroup',$userInfo->blood_group) == "O-" ? 'selected' : '' }}>O-</option>
                                    <option class="form-control" value="AB+" {{ old('bloodgroup',$userInfo->blood_group) == "AB+" ? 'selected' : '' }}>AB+</option>
                                    <option class="form-control" value="AB-" {{ old('bloodgroup',$userInfo->blood_group) == "AB-" ? 'selected' : '' }}>AB-</option>
                                    <option class="form-control" value="unknown" {{ old('bloodgroup',$userInfo->blood_group) == "unknown" ? 'selected' : '' }}>unknown</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <label style="font-size: medium;" for="" class="col-form-label">Current Address</label>

                        <div class="form-group row">
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Block No./Building Name</label>
                                <input type="text" class="form-control" id="current_building" placeholder="Block No./Building Name" name="current_building" value="{{old('current_building',$userInfo->current_building) }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Landmark</label>
                                <input type="text" class="form-control" id="current_landmark" placeholder="Landmark" name="current_landmark" value="{{old('current_landmark',$userInfo->current_landmark) }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="" class="col-form-label">Road Name</label>
                                <input type="text" class="form-control" id="current_road" placeholder="Road Name" name="current_road" value="{{old('current_road',$userInfo->current_road) }}">
                            </div>
                            
                            <!-- <div class="col-sm-3">
                                <label for="" class="col-form-label">Vibhag</label>
                                <select id="current_vibhag" class="form-control" name="current_vibhag">
                                    <option class="form-control" value="">Select Vibhag</option>
                                    @foreach($vibhags as $vibhag)
                                    <option class="form-control" value="{{$vibhag->id}}" {{ old('current_vibhag',$userInfo->current_vibhag) == $vibhag->id ? 'selected' : '' }}>{{$vibhag->name}}</option>
                                    @endforeach
                                </select>
                            </div> -->
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="" class="col-form-label">Pincode</label>
                                <input type="text" class="form-control" pattern="^[1-9][0-9]{5}$" id="current_pincode" placeholder="Pincode" name="current_pincode" value="{{old('current_pincode',$userInfo->current_pincode)}}">
                            </div>
                            <div class="col-sm-3">
                                <label for="" class="col-form-label">Country</label>
                                <select id="current_country" class="form-control" name="current_country">
                                    <option class="form-control" value="">Select Country</option>
                                    @foreach($countries as $country)
                                    <option class="form-control" value="{{$country->id}}" {{ old('current_country',$userInfo->current_country) == $country->id || (old('current_country',$userInfo->current_country) == null && $country->id == 101)? 'selected' : '' }}>{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="" class="col-form-label" maxlength="10">State</label>
                                <select id="current_state" class="form-control" name="current_state">
                                    <option class="form-control" value="">Select State</option>
                                    @foreach($states as $state)
                                    <option class="form-control" value="{{$state->id}}" {{ old('current_state',$userInfo->current_state) == $state->id? 'selected' : '' }}>{{$state->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="" class="col-form-label" maxlength="10">City</label>
                                <select id="current_city" class="form-control" name="current_city">
                                    <option class="form-control" value="">Select City</option>
                                    @foreach($cities as $city)
                                    <option class="form-control" value="{{$city->id}}" {{ old('current_city',$userInfo->current_city) == $city->id? 'selected' : '' }}>{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <label style="font-size: medium;" for="" class="col-form-label">Permanent Address</label>

                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label style="font-size: medium;" for="" class="col-form-label">Same as current</label>
                                <input style="width: 17px ; height: 17px" class="" type="checkbox" id="permanent_address_check" name="permanent_address_check" onclick="openPermanentAddress()" {{ (old('permanent_building',$userInfo->permanent_building) == $userInfo->current_building) || (old('permanent_building',$userInfo->permanent_building) == null) ? "checked" : " " }}>
                            </div>
                        </div>

                        <div id="permanent_address" style="{{ (old('permanent_building',$userInfo->permanent_building) == $userInfo->current_building) || (old('permanent_building',$userInfo->permanent_building) == null) ? 'display:none' : 'display:block'}}">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Block No./Building Name</label>
                                    <input type="text" class="form-control" id="permanent_building" placeholder="Block No./Building Name" name="permanent_building" value="{{old('permanent_building',$userInfo->permanent_building)}}">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Landmark</label>
                                    <input type="text" class="form-control" id="permanent_landmark" placeholder="Landmark" name="permanent_landmark" value="{{old('permanent_landmark',$userInfo->permanent_landmark)}}">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Road Name</label>
                                    <input type="text" class="form-control" id="permanent_road" placeholder="Road Name" name="permanent_road" value="{{old('permanent_road',$userInfo->permanent_road)}}">
                                </div>
                                <!-- <div class="col-sm-3">
                                    <label for="" class="col-form-label">Vibhag</label>
                                    <select id="permanent_vibhag" class="form-control" name="permanent_vibhag">
                                        <option class="form-control" value="">Select Vibhag</option>
                                        @foreach($vibhags as $vibhag)
                                        <option class="form-control" value="{{$vibhag->id}}" {{ old('permanent_vibhag',$userInfo->permanent_vibhag) == $vibhag->id ? 'selected' : '' }}>{{$vibhag->name}}</option>
                                        @endforeach
                                    </select>
                                </div> -->
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Pincode</label>
                                    <input type="text" class="form-control" id="permanent_pincode" pattern="^[1-9][0-9]{5}$" placeholder="Pincode" name="permanent_pincode" value="{{old('permanent_pincode',$userInfo->permanent_pincode)}}">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Country</label>
                                    <select id="permanent_country" class="form-control" name="permanent_country">
                                        <option class="form-control" value="">Select Country</option>
                                        @foreach($countries as $country)
                                        <option class="form-control" value="{{$country->id}}" {{ old('permanent_country',$userInfo->permanent_country) == $country->id || (old('permanent_country',$userInfo->permanent_country) == null && $country->id == 101)? 'selected' : '' }}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label" maxlength="10">State</label>
                                    <select id="permanent_state" class="form-control" name="permanent_state">
                                        <option class="form-control" value="">Select State</option>
                                        @foreach($per_states as $state)
                                        <option class="form-control" value="{{$state->id}}" {{ old('permanent_state',$userInfo->permanent_state) == $state->id? 'selected' : '' }}>{{$state->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label" maxlength="10">City</label>
                                    <select id="permanent_city" class="form-control" name="permanent_city">
                                        <option class="form-control" value="">Select City</option>
                                        @foreach($per_cities as $city)
                                        <option class="form-control" value="{{$city->id}}" {{ old('permanent_city',$userInfo->permanent_city) == $city->id? 'selected' : '' }}>{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="" class="col-form-label">Education Details</label>
                                <select class="form-control" name="education" onchange="openEducationDetails(this)">
                                    <option class="form-control" value="">Select Education</option>
                                    <option class="form-control" value="1" {{ old('education',$userInfo->education) == "1" ? 'selected' : '' }}>Incomplete</option>
                                    <option class="form-control" value="2" {{ old('education',$userInfo->education) == "2" ? 'selected' : '' }}>High-School</option>
                                    <option class="form-control" value="3" {{ old('education',$userInfo->education) == "3" ? 'selected' : '' }}>Graduate</option>
                                    <option class="form-control" value="4" {{ old('education',$userInfo->education) == "4" ? 'selected' : '' }}>Post-Graduate</option>
                                    <option class="form-control" value="5" {{ old('education',$userInfo->education) == "5" ? 'selected' : '' }}>PhD</option>
                                    <option class="form-control" value="6" {{ old('education',$userInfo->education) == "6" ? 'selected' : '' }}>Doctorate</option>
                                </select>
                            </div>

                            <div id="education_details" style="{{ ((old('education',$userInfo->education) == "1") || (old('education',$userInfo->education) ==null)) ? 'display:none' : 'display:block'}}">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">College</label>
                                    <input type="text" class="form-control" id="college" placeholder="College" name="college" value="{{old('college',$userInfo->college)}}">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Year Of Passing</label>
                                    <input type="text" class="form-control" id="year" placeholder="Year Of Passing" name="year" value="{{old('passout_year',$userInfo->passout_year)}}">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">University</label>
                                    <input type="text" class="form-control" id="university" placeholder="University" name="university" value="{{old('university',$userInfo->university)}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="" class="col-form-label">Occupation Details</label>
                                <select class="form-control" name="occupation" onchange="openOccupationDetails(this)">
                                    <option class="form-control" value="">Select Occupation</option>
                                    <option class="form-control" value="1" {{ old('occupation',$userInfo->occupation) == "1" ? 'selected' : '' }}>Student</option>
                                    <option class="form-control" value="2" {{ old('occupation',$userInfo->occupation) == "2" ? 'selected' : '' }}>Intern</option>
                                    <option class="form-control" value="3" {{ old('occupation',$userInfo->occupation) == "3" ? 'selected' : '' }}>Job</option>
                                    <option class="form-control" value="4" {{ old('occupation',$userInfo->occupation) == "4" ? 'selected' : '' }}>Freelancer</option>
                                    <option class="form-control" value="5" {{ old('occupation',$userInfo->occupation) == "5" ? 'selected' : '' }}>Professional</option>
                                    <option class="form-control" value="6" {{ old('occupation',$userInfo->occupation) == "6" ? 'selected' : '' }}>Business</option>
                                    <option class="form-control" value="7" {{ old('occupation',$userInfo->occupation) == "7" ? 'selected' : '' }}>House Person</option>
                                </select>
                            </div>
                            <div id="occupation_details" style="{{ ((old('occupation',$userInfo->occupation) == "1") || (old('occupation',$userInfo->occupation) == "7") || (old('occupation',$userInfo->occupation) == null)) ? 'display:none' : 'display:block'}}">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Industries/Profession</label>
                                    <select id="category" class="form-control" name="category">
                                        <option class="form-control" value="">Select Category</option>
                                        @foreach($categories as $category)
                                        <option class="form-control" value="{{$category->id}}" {{ old('category',$userInfo->category) == $category->id ? 'selected' : '' }}>{{$category->name}}</option>
                                        @endforeach
                                        <!-- <option class="form-control" value="1" {{ old('category',$userInfo->category) == "1" ? 'selected' : '' }}>Manager</option>
                                        <option class="form-control" value="2" {{ old('category',$userInfo->category) == "2" ? 'selected' : '' }}>Senior Employee</option>
                                        <option class="form-control" value="3" {{ old('category',$userInfo->category) == "3" ? 'selected' : '' }}>Junior Employee</option> -->
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Company Name</label>
                                    <input type="text" class="form-control" id="company_name" placeholder="Company Name" name="company_name" value="{{old('company_name',$userInfo->company_name)}}">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Company Contact</label>
                                    <input type="tel" class="form-control" id="contact_person_mobile" pattern="[0-9]{10}" placeholder="Mobile" name="contact_person_mobile" value="{{old('contact_person_mobile',$userInfo->contact_person_mobile)}}">
                                </div>
                            </div>
                        </div>

                        <div id="occupation_details_2" style="{{ ((old('occupation',$userInfo->occupation) == "1") || (old('occupation',$userInfo->occupation) == "7") || (old('occupation',$userInfo->occupation) == null)) ? 'display:none' : 'display:block'}}">
                            <!-- <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Contact Person Name</label>
                                    <input type="text" class="form-control" id="contact_person_name" placeholder="Contact Person Name" name="contact_person_name" value="{{old('contact_person_name',$userInfo->contact_person_name)}}">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Mobile</label>
                                    <input type="tel" class="form-control" id="contact_person_mobile" pattern="[0-9]{10}" placeholder="Mobile" name="contact_person_mobile" value="{{old('contact_person_mobile',$userInfo->contact_person_mobile)}}">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Address</label>
                                    <input type="text" class="form-control" id="contact_person_address" placeholder="Address" name="contact_person_address" value="{{old('contact_person_address',$userInfo->contact_person_address)}}">
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Block No./Building Name</label>
                                    <input type="text" class="form-control" id="occupation_building" placeholder="Block No./Building Name" name="occupation_building" value="{{old('occupation_building',$userInfo->occupation_building)}}">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Landmark</label>
                                    <input type="text" class="form-control" id="occupation_landmark" placeholder="Landmark" name="occupation_landmark" value="{{old('occupation_landmark',$userInfo->occupation_landmark)}}">
                                </div>
                                <div class="col-sm-4">
                                    <label for="" class="col-form-label">Road Name</label>
                                    <input type="text" class="form-control" id="occupation_road" placeholder="Road Name" name="occupation_road" value="{{old('occupation_road',$userInfo->occupation_road)}}">
                                </div>
                                <!-- <div class="col-sm-3">
                                    <label for="" class="col-form-label">Vibhag</label>
                                    <select id="occupation_vibhag" class="form-control" name="occupation_vibhag">
                                        <option class="form-control" value="">Select Vibhag</option>
                                        @foreach($vibhags as $vibhag)
                                        <option class="form-control" value="{{$vibhag->id}}" {{ old('occupation_vibhag',$userInfo->occupation_vibhag) == $vibhag->id ? 'selected' : '' }}>{{$vibhag->name}}</option>
                                        @endforeach
                                    </select>
                                </div> -->
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Pincode</label>
                                    <input type="text" class="form-control" id="occupation_pincode" pattern="^[1-9][0-9]{5}$" placeholder="Pincode" name="occupation_pincode" value="{{old('occupation_pincode',$userInfo->occupation_pincode)}}">
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label">Country</label>
                                    <select id="occupation_country" class="form-control" name="occupation_country">
                                        <option class="form-control" value="">Select Country</option>
                                        @foreach($countries as $country)
                                        <option class="form-control" value="{{$country->id}}" {{ old('occupation_country',$userInfo->occupation_country) == $country->id || (old('occupation_country',$userInfo->occupation_country) == null && $country->id == 101)? 'selected' : '' }}>{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label" maxlength="10">State</label>
                                    <select id="occupation_state" class="form-control" name="occupation_state">
                                        <option class="form-control" value="">Select State</option>
                                        @foreach($occ_states as $state)
                                        <option class="form-control" value="{{$state->id}}" {{ old('occupation_state',$userInfo->occupation_state) == $state->id? 'selected' : '' }}>{{$state->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label for="" class="col-form-label" maxlength="10">City</label>
                                    <select id="occupation_city" class="form-control" name="occupation_city">
                                        <option class="form-control" value="">Select City</option>
                                        @foreach($occ_cities as $city)
                                        <option class="form-control" value="{{$city->id}}" {{ old('occupation_city',$userInfo->occupation_city) == $city->id? 'selected' : '' }}>{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                        </div>
                        <hr>
                        <div class="form-group row">
                            <label style="font-size: large;" for="" class="col-sm-12 col-form-label">Other Info</label>
                            <!-- <input type="text" id="family_head_tab" name="tab" value="done" hidden> -->
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-6 col-form-label">Any family members is associated with Charitable / Social / Organization ?</label>
                            <div class="col-sm-1">
                                <div class="form-check">
                                    <input type="radio" onclick="charityDetails(this)" id="charity_option1" name="charity" value="1" {{ old('charity',$userInfo->charity) == 1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-check">
                                    <input type="radio" onclick="charityDetails(this)" id="charity_option2" name="charity" value="0" {{ old('charity',$userInfo->charity) == 0 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="charity_yes" style="{{ old('charity_organization', $userInfo->charity_organization) == null ? 'display: none;' : 'display: block;'}}">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="charity_organization" placeholder="Charity Organization" name="charity_organization" value="{{old('charity_organization',$userInfo->charity_organization)}}">
                            </div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="charity_designation" placeholder="Charity Designation" name="charity_designation" value="{{old('charity_designation',$userInfo->charity_designation)}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-6 col-form-label">Ready to Serve Society or Sanstha ?</label>
                            <div class="col-sm-1">
                                <div class="form-check">
                                    <input type="radio" onclick="serveDetails(this)" id="serve_option1" name="serve" value="1" {{ old('serve',$userInfo->serve) == 1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-check">
                                    <input type="radio" onclick="serveDetails(this)" id="serve_option2" name="serve" value="0" {{ old('serve',$userInfo->serve) == 0 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">No</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row" id="serve_yes" style="{{ old('serve_help', $userInfo->serve_help) == null ? 'display: none;' : 'display: block;'}}">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="serve_help" placeholder=" Are you ready to provide Help" name="serve_help" value="{{old('serve_help', $userInfo->serve_help)}}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-sm-6 col-form-label">Ready to Donate Eye / Organ / Body ?</label>
                            <div class="col-sm-1">
                                <div class="form-check">
                                    <input type="radio" onclick="donateDetails(this)" id="donate_option1" name="donate" value="1" {{ old('donate',$userInfo->donate) == 1 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="form-check">
                                    <input type="radio" onclick="donateDetails(this)" id="donate_option2" name="donate" value="0" {{ old('donate',$userInfo->donate) == 0 ? 'checked' : '' }}>
                                    <label class="form-check-label" for="inlineRadio1">No</label>
                                </div>
                            </div>
                        </div>
                        <?php
                        $parts = json_decode($userInfo->donate_part);
                        //dump($parts);
                        $eye = null;
                        $organ = null;
                        $body = null;
                        if ($parts != null) {
                            foreach ($parts as $part) {
                                if ($part == "eye") {
                                    $eye = "eye";
                                }
                                if ($part == "organ") {
                                    $organ = "organ";
                                }
                                if ($part == "body") {
                                    $body = "body";
                                }
                            }
                        }
                        //dd($eye, $organ, $body);
                        ?>
                        <div class="form-group row" id="donate_yes" style="{{ (old('donate', $userInfo->donate) == "0") || (old('donate', $userInfo->donate) == null) ? 'display: none;' : 'display: block;'}}"">
                            <div class=" col-sm-2">
                            <div class="form-check">
                                <input type="checkbox" id="eye" name="donate_part[]" value="eye" {{ old('donate_part',$eye) == "eye" ? 'checked' : '' }}>
                                <label class="form-check-label" for="inlineRadio1">Eye</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-check">
                                <input type="checkbox" id="organ" name="donate_part[]" value="organ" {{ old('donate_part',$organ) == "organ" ? 'checked' : '' }}>
                                <label class="form-check-label" for="inlineRadio1">Organ</label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-check">
                                <input type="checkbox" id="body" name="donate_part[]" value="body" {{ old('donate_part', $body) == "body" ? 'checked' : '' }}>
                                <label class="form-check-label" for="inlineRadio1">Body</label>
                            </div>
                        </div>
                </div>

                <button id="submit_button" type="submit" class="btn btn-primary">Save</button>

                </form>
                <br>
            </div>
        </div>
</div>
</section>
</div>
@endsection

@section('script')

<script type="text/javascript">
    function checkLastName(that) {
        if (that.value == "0") {
            document.getElementById("LastName_other").style.display = "block";
        } else {
            document.getElementById("LastName_other").style.display = "none";
        }
    }

    function openPermanentAddress() {
        if (document.getElementById("permanent_address_check").checked == false) {
            document.getElementById("permanent_address").style.display = "block";
            // document.getElementById("permanent_country").value = null;
            // document.getElementById("permanent_building").value = null;
            // document.getElementById("permanent_landmark").value = null;
            // document.getElementById("permanent_city").value = null;
            // document.getElementById("permanent_pincode").value = null;
            // document.getElementById("permanent_state").value = null;
            // document.getElementById("permanent_road").value = null;
        } else {
            document.getElementById("permanent_address").style.display = "none";
            // document.getElementById("permanent_country").value = null;
            // document.getElementById("permanent_building").value = null;
            // document.getElementById("permanent_landmark").value = null;
            // document.getElementById("permanent_city").value = null;
            // document.getElementById("permanent_pincode").value = null;
            // document.getElementById("permanent_state").value = null;
            // document.getElementById("permanent_road").value = null;


        }
    }

    function openEducationDetails(that) {
        if (that.value > 1) {
            document.getElementById("education_details").style.display = "block";
        } else {
            document.getElementById("education_details").style.display = "none";
            document.getElementById("college").value = null;
            document.getElementById("year").value = null;
            document.getElementById("university").value = null;

        }
    }


    function openOccupationDetails(that) {
        if (that.value > 1 && that.value < 7) {
            document.getElementById("occupation_details").style.display = "block";
            document.getElementById("occupation_details_2").style.display = "block";

        } else {
            document.getElementById("occupation_details").style.display = "none";
            document.getElementById("occupation_details_2").style.display = "none";
            document.getElementById("category").value = "";
            document.getElementById("company_name").value = null;
            // document.getElementById("contact_person_name").value = null;
            document.getElementById("contact_person_mobile").value = null;
            // document.getElementById("contact_person_address").value = null;
            document.getElementById("occupation_building").value = null;
            document.getElementById("occupation_landmark").value = null;
            document.getElementById("occupation_road").value = null;
            document.getElementById("occupation_city").value = null;
            document.getElementById("occupation_pincode").value = null;
            document.getElementById("occupation_state").value = null;
            // document.getElementById("occupation_country").value = null;
        }
    }

    function openSpouseCurrentAddress() {
        if (document.getElementById("spouse_current_address_check").checked == false) {
            document.getElementById("spouse_current_address").style.display = "block";
        } else {
            document.getElementById("spouse_current_address").style.display = "none";
        }
    }

    function openSpouseEducationDetails(that) {
        if (that.value > 1) {
            document.getElementById("spouse_education_details").style.display = "block";
        } else {
            document.getElementById("spouse_education_details").style.display = "none";
        }
    }


    function openSpouseOccupationDetails(that) {
        if (that.value != '' && +(that.value) > 1 && +(that.value) < 7) {
            document.getElementById("spouse_occupation_details").style.display = "block";
            document.getElementById("spouse_occupation_details_2").style.display = "block";

        } else {
            document.getElementById("spouse_occupation_details").style.display = "none";
            document.getElementById("spouse_occupation_details_2").style.display = "none";

        }
    }

    function openChildrenCurrentAddress() {
        if (document.getElementById("children_current_address_check").checked == false) {
            document.getElementById("children_current_address").style.display = "block";
        } else {
            document.getElementById("children_current_address").style.display = "none";
        }
    }

    function openChildrenEducationDetails(that) {
        if (that.value > 1) {
            document.getElementById("children_education_details").style.display = "block";
        } else {
            document.getElementById("children_education_details").style.display = "none";
        }
    }


    function openChildrenOccupationDetails(that) {
        if (that.value != '' && +(that.value) > 1 && +(that.value) < 7) {
            document.getElementById("children_occupation_details").style.display = "block";
            document.getElementById("children_occupation_details_2").style.display = "block";

        } else {
            document.getElementById("children_occupation_details").style.display = "none";
            document.getElementById("children_occupation_details_2").style.display = "none";

        }
    }

    function charityDetails(that) {
        if (that.value == "1") {
            document.getElementById("charity_yes").style.display = "block";
        } else {
            document.getElementById("charity_yes").style.display = "none";
            document.getElementById("charity_organization").value = null;
            document.getElementById("charity_designation").value = null;
        }
    }

    function serveDetails(that) {
        if (that.value == 1) {
            document.getElementById("serve_yes").style.display = "block";
        } else {
            document.getElementById("serve_yes").style.display = "none";
            document.getElementById("serve_help").value = null;

        }
    }

    function donateDetails(that) {
        if (that.value == 1) {
            document.getElementById("donate_yes").style.display = "block";
        } else {
            document.getElementById("donate_yes").style.display = "none";
            document.getElementById("eye").checked = false;
            document.getElementById("organ").checked = false;
            document.getElementById("body").checked = false;
        }
    }
</script>

<script>
    $(document).ready(function() {

        $("#last_name").autocomplete({
            minLength: 3,
            source: function(request, response) {
                $.ajax({
                    method: "POST",
                    url: "/getLastName",
                    dataType: "json",
                    data: {
                        lastName: request.term,
                        "_token": "{{ csrf_token() }}",
                    },
                    success: function(data) {
                        console.log(data);
                        var arr = data.last_names.map(function(obj) {
                            return obj.name;
                        });
                        response(arr);

                    }
                });
            }
        });

        $('#current_country').on('change', function() {
            $('#current_state').html('');
            $('#current_city').html('');
            $.ajax({
                method: "GET",
                url: "/getState/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#current_state').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    $('#current_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    if (data && data.state) {
                        data.state.forEach(state => {
                            $('#current_state').append($("<option></option>")
                                        .attr("value", state.id)
                                        .text(state.name)); 
                        })
                    }

                }
            });
        });

        $('#current_state').on('change', function() {
            $('#current_city').html('');
            $.ajax({
                method: "GET",
                url: "/getCity/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#current_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select City')); 
                    if (data && data.city) {
                        data.city.forEach(city => {
                            $('#current_city').append($("<option></option>")
                                        .attr("value", city.id)
                                        .text(city.name)); 
                        })
                    }

                }
            });
        });

        $('#permanent_country').on('change', function() {
            $('#permanent_state').html('');
            $('#permanent_city').html('');
            $.ajax({
                method: "GET",
                url: "/getState/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#permanent_state').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    $('#permanent_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    if (data && data.state) {
                        data.state.forEach(state => {
                            $('#permanent_state').append($("<option></option>")
                                        .attr("value", state.id)
                                        .text(state.name)); 
                        })
                    }

                }
            });
        });

        $('#permanent_state').on('change', function() {
            $('#permanent_city').html('');
            $.ajax({
                method: "GET",
                url: "/getCity/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#permanent_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select City')); 
                    if (data && data.city) {
                        data.city.forEach(city => {
                            $('#permanent_city').append($("<option></option>")
                                        .attr("value", city.id)
                                        .text(city.name)); 
                        })
                    }

                }
            });
        });

        $('#occupation_country').on('change', function() {
            $('#occupation_state').html('');
            $('#occupation_city').html('');
            $.ajax({
                method: "GET",
                url: "/getState/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#occupation_state').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    $('#occupation_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select State')); 
                    if (data && data.state) {
                        data.state.forEach(state => {
                            $('#occupation_state').append($("<option></option>")
                                        .attr("value", state.id)
                                        .text(state.name)); 
                        })
                    }

                }
            });
        });

        $('#occupation_state').on('change', function() {
            $('#occupation_city').html('');
            $.ajax({
                method: "GET",
                url: "/getCity/"+this.value,
                dataType: "json",
                success: function(data) {
                    $('#occupation_city').append($("<option></option>")
                                .attr("value", '')
                                .text('Select City')); 
                    if (data && data.city) {
                        data.city.forEach(city => {
                            $('#occupation_city').append($("<option></option>")
                                        .attr("value", city.id)
                                        .text(city.name)); 
                        })
                    }

                }
            });
        });
    });

    $(document).ready(function() {

        $("#email").on('blur', function() {
            var user = '<?php echo $user->id;
                        ?>';
            // console.log(user);
            // console.log("in");
            if ($('#email').val() != "") {
                $.ajax({
                    method: "GET",
                    url: "/check_update_email",
                    dataType: "json",
                    data: {
                        email: $('#email').val(),
                        id: user
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.email_exist == true) {
                            // $('#email').val('');
                            $("#submit_button").attr("disabled", 'disabled');
                            $('#email_exist').css('display', 'block');
                        } else {
                            $("#submit_button").attr("disabled", false);
                            $('#email_exist').css('display', 'none');
                        }
                    }

                });
            }
        });
    });
    $(document).ready(function() {
        $("#mobile").on('blur', function() {
            var user = '<?php echo $user->id;
                        ?>';
            // console.log(user);
            if ($('#mobile').val() != "") {
                $.ajax({
                    method: "GET",
                    url: "/check_update_mobile",
                    dataType: "json",
                    data: {
                        mobile: $('#mobile').val(),
                        id: user
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.mobile_exist == true) {
                            // $('#mobile').val('');
                            $("#submit_button").attr("disabled", 'disabled');
                            $('#mobile_exist').css('display', 'block');
                        } else {
                            $("#submit_button").attr("disabled", false);
                            $('#mobile_exist').css('display', 'none');
                        }
                    }

                });
            }
        });
    });
    $(document).ready(function() {
        $("#alternate_mobile").on('blur', function() {
            var user = '<?php echo $user->id;
                        ?>';
            // console.log(user);
            if ($('#alternate_mobile').val() != "") {
                $.ajax({
                    method: "GET",
                    url: "/check_update_alternate_mobile",
                    dataType: "json",
                    data: {
                        mobile: $('#alternate_mobile').val(),
                        id: user
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.mobile_exist == true) {
                            // $('#alternate_mobile').val('');
                            $("#submit_button").attr("disabled", 'disabled');
                            $('#alternate_mobile_exist').css('display', 'block');
                        } else {
                            $("#submit_button").attr("disabled", false);
                            $('#alternate_mobile_exist').css('display', 'none');
                        }
                    }

                });
            }
        });
    });
</script>
@endsection
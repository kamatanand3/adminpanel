@extends('layouts.user-profile')

@section('subtitle', trans('user.profile'))

@section('user-content')

<div class="profile user-profile">

    @include('users.partials.profile')
    

    <div class="flex justify-between lg:border-t flex-col-reverse lg:flex-row">
        @include('users.partials.action-buttons', ['user' => $user])
    </div>

</div>

<div class="row">
    <?php
    //dump($user->id);
    ?>
    
    <div class="col-md-12">
        @include('users.partials.parent-spouse')
        @include('users.partials.childs')
        @include('users.partials.siblings')

        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Basic Info</h3></div>
            <table class="table">
                <tbody>
                    <tr>
                        <th class="col-sm-4">Original Last Name</th>
                        <td class="col-sm-8">{{ $user_info->last_name_original }}</td>
                    </tr>
                    <tr>
                        <th>Native Place</th>
                        <td>{{ $user_info->native_place }}</td>
                    </tr>
                    <tr>
                        <th>Birth Place</th>
                        <td>{{ $user_info->birth_place }}</td>
                    </tr>
                    <tr>
                        <th>Gotra</th>
                        <td>{{ $user_info->gotra }}</td>
                    </tr>
                    <tr>
                        <th>Divyang</th>
                        <td>{{ $user_info->divyang == 0 ? 'No' : 'Yes'}}</td>
                    </tr>
                    <tr>
                        <th>Mosal</th>
                        <td>{{ $user_info->mosad }}</td>
                    </tr>
                    <tr>
                        <th>Marital Status</th>
                        <td>{{ $user_info->marital_status }}</td>
                    </tr>
                    <tr>
                        <th>Blood Group</th>
                        <td>{{ $user_info->blood_group }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Address</h3></div>
            <table class="table">
                <tbody>
                    <tr>
                        <th class="col-sm-4">Current Address</th>
                        <td class="col-sm-8">{{ $user_info->current_building }}, {{ $user_info->current_landmark }}, {{ $user_info->current_road }}, {{ $current_city }}, {{ $user_info->current_pincode }}, {{ $current_state }}, {{ $current_country }}</td>
                    </tr>
                    <tr>
                        <th>Permanent Address</th>
                        <td class="col-sm-8">{{ $user_info->permanent_building }}, {{ $user_info->permanent_landmark }}, {{ $user_info->permanent_road }}, {{ $permanent_city }}, {{ $user_info->permanent_pincode }}, {{ $permanent_state }}, {{ $permanent_country }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Educational Details</h3></div>
            <table class="table">
                <tbody>
                    <tr>
                        <th class="col-sm-4">Education</th>
                        @switch($user_info->education)
                        @case(1)
                        <td class="col-sm-8">Incomplete</td>
                        @break
                        @case(2)
                        <td class="col-sm-8">High-School</td>
                        @break
                        @case(3)
                        <td class="col-sm-8">Graduate</td>
                        @break
                        @case(4)
                        <td class="col-sm-8">Post-Graduate</td>
                        @break
                        @case(5)
                        <td class="col-sm-8">PhD</td>
                        @break
                        @case(6)
                        <td class="col-sm-8">Doctorate</td>
                        @break
                        @endswitch
                    </tr>
                    @if ($user_info->education != null && $user_info->education != 0  && $user_info->education != 1)
                    <tr>
                        <th>College</th>
                        <td class="col-sm-8">{{ $user_info->college }}</td>
                    </tr>
                    <tr>
                        <th>Year Of Passing</th>
                        <td class="col-sm-8">{{ $user_info->passout_year }}</td>
                    </tr>
                    <tr>
                        <th>University</th>
                        <td class="col-sm-8">{{ $user_info->university }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Occupational Details</h3></div>
            <table class="table">
                <tbody>
                    <tr>
                        <th class="col-sm-4">Occupation</th>
                        @switch($user_info->occupation)
                        @case(1)
                        <td class="col-sm-8">Student</td>
                        @break
                        @case(2)
                        <td class="col-sm-8">Intern</td>
                        @break
                        @case(3)
                        <td class="col-sm-8">Job</td>
                        @break
                        @case(4)
                        <td class="col-sm-8">Freelancer</td>
                        @break
                        @case(5)
                        <td class="col-sm-8">Professional</td>
                        @break
                        @case(6)
                        <td class="col-sm-8">Business</td>
                        @case(7)
                        <td class="col-sm-8">House Person</td>
                        @break
                        @endswitch
                    </tr>
                    @if ($user_info->occupation != null && $user_info->occupation != 0  && $user_info->occupation != 1)
                    <tr>
                        <th>Industries/Profession</th>
                        <td class="col-sm-8">{{ $category_name }}</td>
                    </tr>
                    <tr>
                        <th>Company Name</th>
                        <td class="col-sm-8">{{ $user_info->company_name }}</td>
                    </tr>
                    <tr>
                        <th>Company Contact</th>
                        <td class="col-sm-8">{{ $user_info->contact_person_mobile }}</td>
                    </tr>
                    <tr>
                        <th>Address</th>
                        <td class="col-sm-8">{{ $user_info->occupation_building }}, {{ $user_info->occupation_landmark }}, {{ $user_info->occupation_road }}, {{ $occupation_city }}, {{ $user_info->occupation_pincode }}, {{ $occupation_state }}, {{ $occupation_country }}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>


    </div>
</div>
@endsection

@section ('ext_css')
<link rel="stylesheet" href="{{ asset('css/plugins/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/plugins/jquery.datetimepicker.css') }}">
@endsection

@section ('ext_js')
<script src="{{ asset('js/plugins/select2.min.js') }}"></script>
<script src="{{ asset('js/plugins/jquery.datetimepicker.js') }}"></script>
@endsection

@section ('script')
<script>
    (function () {
        $('select').select2();
        $('input[name=marriage_date]').datetimepicker({
            timepicker:false,
            format:'Y-m-d',
            closeOnDateSelect: true,
            scrollInput: false
        });
    })();
</script>
@endsection

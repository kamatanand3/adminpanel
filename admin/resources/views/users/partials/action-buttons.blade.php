<div class="pull-right btn-group" role="group">
@can ('edit', $user)
    @if($user->isFamilyHead == 1)
    <a class="btn btn-default" href="/create-overseas">Create Overseas Family</a></li>
    <a class="btn btn-default" href="/create-my-family">Create my family tree</a></li>
    @endif
    @endcan

    @can ('edit', $user)
    {{ link_to_route('users.edit', trans('app.edit'), [$user->id], ['class' => 'btn btn-warning']) }}
    @else
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addFamilyModal">Add To Family</button>
    @endcan
    {{ link_to_route('users.show', trans('app.show_profile').' '.$user->name, [$user->id], ['class' => Request::segment(3) == null ? 'btn btn-default active' : 'btn btn-default']) }}
    <!-- {{ link_to_route('users.chart', trans('app.show_family_chart'), [$user->id], ['class' => Request::segment(3) == 'chart' ? 'btn btn-default active' : 'btn btn-default']) }} -->
    {{ link_to_route('users.tree', trans('app.show_family_tree'), [$user->id], ['class' => Request::segment(3) == 'tree' ? 'btn btn-default active' : 'btn btn-default']) }}
    <!-- {{ link_to_route('users.marriages', trans('app.show_marriages'), [$user->id], ['class' => Request::segment(3) == 'marriages' ? 'btn btn-default active' : 'btn btn-default']) }} -->
    @if (!Auth::guest())
    @if (Auth()->user()->isAreaHead == 1)
    <a class="btn btn-default" href="/family">List</a></li>
    @endif
    @endif
</div>

<!-- Modal -->
<div class="modal fade" id="addFamilyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="float:left;">Add To Family</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group row">
                <div class="col-sm-4">
                    <label for="" class="col-form-label" maxlength="10">Relationship</label>
                    <select id="linkRelationDropdown" class="form-control" name="relationship">
                        <option class="form-control" value="''">Select Relationship</option>
                        <!-- check both user marriages didnt happend and opposite gender-->
                        @if ($user->marriages()->count() == 0 && Auth()->user()->marriages()->count() == 0 && Auth()->user()->gender_id != $user->gender_id)
                            <option class="form-control" value="{{ $user->gender_id == 1 ? 'Husband' : 'Wife'}}">{{ $user->gender_id == 1 ? 'Husband' : 'Wife'}}</option>
                        @endif
                        
                        <!-- check gender male and logged in user doesnt have father linked -->
                        @if ($user->gender_id == 1 && Auth()->user()->father_id == null)
                            <option class="form-control" value="Father">Father</option>
                        @endif
                        
                        <!-- check gender female and logged in user doesnt have mother linked -->
                        @if ($user->gender_id == 2 && Auth()->user()->mother_id == null)
                            <option class="form-control" value="Mother">Mother</option>
                        @endif
                        
                        <!-- check if any user has parent -->
                        @if ($user->mother_id == null && $user->father_id == null)
                            <option class="form-control" value="{{ $user->gender_id == 1 ? 'Son' : 'Daughter'}}">{{ $user->gender_id == 1 ? 'Son' : 'Daughter'}}</option>
                        @endif
                        
                        <!-- check if any user has parent -->
                        @if (Auth()->user()->mother_id != null || Auth()->user()->father_id != null || $user->mother_id != null || $user->father_id != null)
                            <option class="form-control" value="{{ $user->gender_id == 1 ? 'Brother' : 'Sister'}}">{{ $user->gender_id == 1 ? 'Brother' : 'Sister'}}</option>
                        @endif
                    </select>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="addToFamilyRequestBtn" type="button" class="btn btn-primary" >Send Request To Link</button>
      </div>
    </div>
  </div>
</div>
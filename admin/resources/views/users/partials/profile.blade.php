<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/popup.css') }}" rel="stylesheet">
<div class="profiles_banner">
   @if($user->photo_path == null)
   @else
   <img src="{{ asset('assets/images/avatars/profile-cover.jpg') }}" alt="">
   <div class="profile_action absolute bottom-0 right-0 space-x-1.5 p-3 text-sm z-50 hidden lg:flex">
      <a href="#" class="flex items-center justify-center h-8 px-3 rounded-md bg-gray-700 bg-opacity-70 text-white space-x-1.5"> 
        <ion-icon name="crop-outline" class="text-xl"></ion-icon>
        <span> Crop  </span>
    </a>
    <a href="#" class="flex items-center justify-center h-8 px-3 rounded-md bg-gray-700 bg-opacity-70 text-white space-x-1.5"> 
        <ion-icon name="create-outline" class="text-xl"></ion-icon>
        <span> Edit </span>
    </a>
</div>
@endif
</div>
<div class="profiles_content">
    @if($user->photo_path == null)
    <div class="profile_avatar">
      <div class="profile_avatar_holder"> 
        {{ userPhoto($user) }}
    </div>
        <div class="user_status status_online"></div>
        <div class="icon_change_photo" hidden> <ion-icon name="camera" class="text-xl"></ion-icon> </div>
    </div>
    
    @else

    <div class="profile_avatar">
      <div class="profile_avatar_holder"> 
        <img src="{{url('/uploads/'.$user->photo_path)}}" class="w-100" alt="">
    </div>
        <div class="user_status status_online"></div>
        <div class="icon_change_photo" hidden> <ion-icon name="camera" class="text-xl"></ion-icon> </div>
    </div>
@endif

<div class="profile_info">
  <h1>{{ $user->profileLink() }}</h1>
  <!-- <p>{{ $user->gender }}</p> -->
</div>

</div>

@section('script')
<script>
    let loggInUserId = "{{ Auth()->user()->id }}";
    let profileUserId = "{{ $user->id }}";
    // console.log("in");
    $(document).ready(function() {

        $('#addToFamilyRequestBtn').on('click', function() {
            const reln = $('#linkRelationDropdown :selected').text();
            if (reln != '' && reln != 'Select Relationship') {
                $.ajax({
                    method: "GET",
                    url: "/link_request",
                    dataType: "json",
                    data: {
                        requester: loggInUserId,
                        request_to: profileUserId,
                        request_for: reln,
                        module: 'Vastipatrak',
                    },
                    success: function(data) {
                        console.log("inhere");
                        // let html = `<p>Success</p>`;
                        // $('#msg').html(html);
                        // .fadeIn('slow');
                        // $('#msg').delay(10000).fadeOut('slow');
                        window.location = "/requests";
                        alert('request send!');
                    }
                });
            }
        });


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#inputfile').change(function() {
            console.log("in");
            var user = '<?php echo $user->id;
            ?>';
            console.log(user);
            var file_data = $('#inputfile').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            console.log(form_data);

            $.ajax({
                url: user + "/photo-upload",
                type: "POST",
                data: form_data,
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                }
            });
        });

    });
    $(document).ajaxStop(function() {
        window.location.reload();
    });

    function crossClicked() {
        if (confirm("Delete profile photo?")) {
            var user = '<?php echo $user->id;
            ?>';
            console.log(user);
            $.ajax({
                url: "/delete-photo",
                type: "POST",
                data: {
                    user: user,
                    "_token": "{{ csrf_token() }}",
                },
                dataType: "json",
                success: function(data) {
                    console.log(data);
                }
            });
        } else {
            console.log("no");
        }
    }
</script>

@endsection
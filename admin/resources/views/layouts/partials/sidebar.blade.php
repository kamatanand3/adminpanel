
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ asset('assets/index3.html') }}" class="brand-link">
   <img src="{{ asset('assets/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
   <span class="brand-text font-weight-light">
      {{ config('app.name', 'Laravel') }}
   </span>
</a>

<!-- Sidebar -->
<div class="sidebar">
   <!-- Sidebar user (optional) -->
   <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
     <img src="{{ asset('assets/images/avatars/avatar-2.jpg') }}" class="img-circle elevation-2" alt="User Image">
  </div>
  <div class="info">
     <a href="#" class="d-block">{{ Auth::user()->name }}</a>
  </div>
  <div class="info">
          <a href="{{ route('logout') }}" class="d-block">logout</a>
  </div>
</div>

<!-- SidebarSearch Form -->
<div class="form-inline">
 <div class="input-group" data-widget="sidebar-search">
  <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
  <div class="input-group-append">
   <button class="btn btn-sidebar">
    <i class="fas fa-search fa-fw"></i>
 </button>
</div>
</div>
</div>

<!-- Sidebar Menu -->
<nav class="mt-2">
 <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
            
<li class="nav-item menu-open">
   <a href="#" class="nav-link {{ Request::segment(1) === 'blogs' || 'addblog' ? 'active' : null }}">
    <i class="nav-icon fas fa-book"></i>
    <p>
     Blog
     <i class="fas fa-angle-left right"></i>
  </p>
</a>
<ul class="nav nav-treeview">


<li class="nav-item ">
  <a href="{{ route('blogs.index') }}" class="nav-link {{ Request::segment(1) === 'blogs' ? 'active' : null }}">
   <i class="far fa-circle nav-icon"></i>
   <p>Blog List</p>
</a>
</li>
<li class="nav-item">
  <a href="{{ route('blogs.addblog') }}" class="nav-link {{ Request::segment(1) === 'addblog' ? 'active' : null }}">
   <i class="far fa-circle nav-icon"></i>
   <p>Blog Add</p>
</a>
</li>
</ul>
</li>



</ul>
</nav>
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>
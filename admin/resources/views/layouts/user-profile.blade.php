@extends('layouts.app')

@section('content')
    <h2 class="page-header">
        {{ $user->name }} <small>@yield('subtitle')</small>
    </h2>
    @yield('user-content')
@endsection

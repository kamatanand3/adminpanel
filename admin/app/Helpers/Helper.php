<?php
namespace App\Helpers;
use App\User;

class Helper{
    public static function userPhoto(User $user, $attributes = [])
    {
        return \Html::image(
            userPhotoPath($user->photo_path, $user->gender_id),
            null,
            $attributes
        );
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = [
        'id', 'user_id', 'one_signal_user_id', 'one_signal_token' , 'one_signal_user_id_web'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'blogCategories';

    protected $fillable = [
        'nameEn', 'nameGr', 'displayOnWebsite', 'displayAsMenu' , 'orderLevel', 'created_id', 'updated_id'
    ];
}

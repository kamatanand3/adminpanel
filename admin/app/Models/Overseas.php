<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Overseas extends Model
{
    protected $table = 'overseas';

    protected $fillable = [
        'id', 'name', 'mobile_code', 'mobile' , 'email', 'address', 'relation', 'created_id', 'updated_id'
    ];
}

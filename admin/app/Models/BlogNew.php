<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogNew extends Model
{
    protected $table = 'blogs';

    protected $fillable = [
        'nameEn', 'nameGr', 'category', 'descriptionEn' , 'descriptionGu', 'attachment', 'authorName','published','status', 'created_id', 'updated_id'
    ];
}

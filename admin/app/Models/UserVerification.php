<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model
{
    protected $fillable = [
        'id', 'mobile', 'otp', 'is_active', 'expired_at', 'created_id', 'updated_id'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FamilyTree extends Model
{
    
    protected $table = 'family_tree';
    protected $fillable = [
        'id', 'user_id', 'first_name', 'middle_name' , 'last_name', 'relation', 'relation_with', 'dob', 'native_place', 'salutation', 'relation_with_id'
    ];
}

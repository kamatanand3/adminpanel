<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model
{
    protected $fillable = [
        'id', 'requester', 'request_to', 'request_for' , 'module', 'status'
    ];
}

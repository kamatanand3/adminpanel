<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    use HasFactory;

    protected $table = 'user_info';

    protected $fillable = [
        'user_id',
        'last_name',
        'middle_name',
        'last_name_original',
        'native_place',
        'blood_group',
        'email',
        'mobile_code',
        'mobile',
        'visibility',
        'current_building',
        'current_landmark',
        'current_road',
        'current_pincode',
        'current_vibhag',
        'current_city',
        'current_state',
        'current_country',
        'permanent_building',
        'permanent_landmark',
        'permanent_road',
        'permanent_pincode',
        'permanent_vibhag',
        'permanent_city',
        'permanent_state',
        'permanent_country',
        'education',
        'college',
        'passout_year',
        'university',
        'occupation',
        'category',
        'company_name',
        'contact_person_name',
        'contact_person_mobile',
        'contact_person_address',
        'charity',
        'charity_organization',
        'charity_designation',
        'serve',
        'serve_help',
        'donate',
        'donate_part',
        'alternate_mobile_code',
        'alternate_mobile',
        'alternate_mobile_visibility',
        'birth_place',
        'gotra',
        'divyang',
        'mosad',
        'occupation_building',
        'occupation_landmark',
        'occupation_road',
        'occupation_pincode',
        'occupation_vibhag',
        'occupation_city',
        'occupation_state',
        'occupation_country',
        'maritial_status',
        'is_primary_mobile',
        'marital_status',
        'otp_status'
    ];
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\UserVerification;
use App\Models\Session;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register','register_verifyOTP']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login_og(Request $request)
    {
        $credentials = $request->only('email', 'password');
        //dd($this->guard());
        if ($token = $this->guard()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    protected function credentials($request)
    {
        if (is_numeric($request->get('email'))) {
            return ['phone' => $request->get('email'), 'password' => $request->get('password')];
        } elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            return ['email' => $request->get('email'), 'password' => $request->get('password')];
        }
        return ['email' => $request->get('email'), 'password' => $request->get('password')];
    }

    public function login(Request $request)
    {
        try {
            $userInfo = UserInfo::where('mobile', $request->get('email'))->first();
            if ($userInfo) {
                if ($userInfo->otp_status == 'pending') {
                    return $this->failure('Mobile is not verified. Please try resetting your password.', null, 200);
                }
            }
            $token = JWTAuth::attempt($this->credentials($request));
            if (!$token) {
                return $this->failure('Incorrect Mobile or Password !!', null, 200);
            }
        } catch (JWTException $e) {
            return $this->failure('Something is Wrong !!', null, 500);
        }
        $user = [
            'id'=> auth()->user()->id,
            'token' => $token,
        ];

        try {
            Session::updateOrCreate(
                [
                    "user_id" => auth()->user()->id,
                ],
                [
                    'one_signal_token' => $request->get('one_signal_token'),
                    'one_signal_user_id' => $request->get('one_signal_user_id')
                ]
            );
        }
        catch (\Exception $e){
        }
        
        return $this->success('Login Successully !!', $user , 200);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        //return response()->json($this->guard()->user());
        return response()->json(["message" => "check"]);
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout_og()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function logout()
    {
        return $this->success('Logout Successfully.', null);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard('api')->factory()->getTTL(),
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'string|email|max:255|unique:users|nullable',
            'gender_id' => 'required|numeric|in:1,2',
            'mobile' => 'required|unique:user_info',
            'password' => 'required|string|min:6|confirmed',
            'current_city' => 'required|string|max:255',
            'current_state' => 'required|string|max:255',
            'current_country' => 'required|string|max:255'
        ]);

        if($validator->fails()){
            return $this->failure($validator->errors()->toJson(), null, 400);
        }
        $uuid = Uuid::uuid4()->toString();
        $user = User::create([
            'id' => $uuid,
            'nickname' => $request->name,
            'name' => $request->name,
            'gender_id' => $request->gender_id,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone' => $request->mobile,
        ]);
        $user->manager_id = $user->id;
        // $user->isAreaHead = $data["is_areahead"];
        $user->isFamilyHead = 1;
        $user->save();

        $user_info = UserInfo::create([
            'user_id' => $uuid,
            'last_name' => $request->last_name,
            'middle_name' => $request->middle_name,
            'email' => $request->email,
            'mobile_code' => $request->mobile_code,
            'mobile' => $request->mobile,
            'otp_status' => 'pending',
            'current_city' => $request->current_city,
            'current_state' => $request->current_state,
            'current_country' => $request->current_country,
            'permanent_city' => $request->current_city,
            'permanent_state' => $request->current_state,
            'permanent_country' => $request->current_country
        ]);
        $user_info->save();
        $token = JWTAuth::fromUser($user);

        $auth_user = [
            'id'=> $uuid,
            'token' => $token,
        ];

        $otp = $this->generateNumericOTP(6);
            
        $current = \Carbon\Carbon::now();
        $current->addMinutes(env('OTP_EXPIRY_MINUTES',5));
        
        UserVerification::where('mobile', $request->mobile)->update(['is_active'=> 0]);

        $verify = new UserVerification();
        $verify->otp = $otp;
        $verify->mobile = $request->mobile;
        $verify->is_active = 1;
        $verify->expired_at = $current;
        $verify->save();

        $message = $otp." is your OTP to reset your password for DSVS Login.\n\nRegards,\nDSVS Team";
        $this->sendTextMsg($request->mobile, $message);


        return $this->success('OTP sent Successully !!', $auth_user , 200);
    }

    public function verifyOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required',
            'mobile' => 'required'
        ]);

        if($validator->fails()){
            return $this->failure($validator->errors()->toJson(), null, 400);
        }

        
        $verification = UserVerification::where('mobile',$request->mobile)->where('is_active',1)->first();
        if($verification && \Carbon\Carbon::now()<$verification->expired_at && $verification->is_active==1){
            if($verification->otp == $request->otp){
                UserVerification::where('mobile', $request->mobile)->update(['is_active'=> 0]);
                
                UserInfo::where('mobile', $request->mobile)->update(['otp_status' => 'verified']);

                return $this->success('OTP is verified!', null, 200);
            } else {
                return $this->failure('OTP not matched!' , null, 500);
            }
        } else {
            return $this->failure('OTP expired or Wrong OTP' , null, 500);
        }
    }
}

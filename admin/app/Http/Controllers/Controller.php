<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function success($message, $data, $code = 200) {
        return response()->json(['status' => 'success', 'message'=> __($message), 'data'=> $data], $code);
    }

    public function failure($message, $data = null, $code = 200) {
        return response()->json(['status' => 'error', 'message'=> __($message), 'data'=> $data], $code);
    }

    function sendTextMsg($mobile, $message) {
        $encodedMessage = urlencode($message);
        $api = "http://msg.dsuinfotech.com/rest/services/sendSMS/sendGroupSms?AUTH_KEY=ee4166df3237d14a5f4be2a3adef2aa&message=" . $encodedMessage . "&senderId=DSVSMJ&routeId=1&mobileNos=" . $mobile . "&smsContentType=english";
        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $api);
            return true;
        } catch (HttpException $ex) {
            return false;
        }
    }
    
    public function generateNumericOTP($n) {

        // Take a generator string which consist of
        // all numeric digits
        $generator = "1357902468";
        $result = "";

        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }

        // Return result
        return $result;
    }
}

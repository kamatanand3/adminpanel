<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Couple;
use App\Models\Requests;
use App\UserInfo;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class RequestsController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $myRequests = DB::table('requests')
                    ->where('requests.requester', $user->id)
                    ->leftJoin('users', function($join)
                        {
                            $join->on('requests.request_to', '=', 'users.id');
                        })
                    ->select('requests.*', 'users.name')
                    ->get();
                    
        $requestsToMe = DB::table('requests')
                    ->where('requests.request_to', $user->id)
                    ->leftJoin('users', function($join)
                        {
                            $join->on('requests.requester', '=', 'users.id');
                        })
                    ->select('requests.*', 'users.name')
                    ->get();
        $data = [
            'myRequests'  => $myRequests,
            'requestsToMe'   => $requestsToMe
        ];
        if (request()->wantsJson()) {
            return $this->success('Request List', $data, 200);
        }
        else {
            return view('requests.index', compact('data'));
        }
    }

    public function linkRequest(Request $request) {
        $request = Requests::create([
            'id' => Uuid::uuid4()->toString(),
            'requester' => $request->requester,
            'request_to' => $request->request_to,
            'request_for' => $request->request_for,
            'module' => $request->module,
        ]);
        $request->save();
        return response()->json(['request_sent' => true]);
    }

    public function actionRequest(Request $request) {
        $user = auth()->user();
        $userRequest = Requests::where('id', $request->request_id)->first();

        if ($userRequest->module == 'Vastipatrak') {
            if ($request->action == 'Approved') {
                if ($userRequest->request_for == 'Wife') {
                    $couple_id = Couple::create([
                        'id' => Uuid::uuid4()->toString(),
                        'husband_id' => $userRequest->requester,
                        'wife_id' => $userRequest->request_to,
                        'manager_id' => $user->manager_id
                    ])->id;
                    UserInfo::where('user_id', $userRequest->requester)->update(['maritial_status'=>1]);
                    UserInfo::where('user_id', $userRequest->request_to)->update(['maritial_status'=>1]);
                    User::where('father_id', $userRequest->requester)->update(['mother_id'=> $userRequest->request_to, 'parent_id' => $couple_id]);
                    User::where('mother_id', $userRequest->request_to)->update(['father_id'=> $userRequest->requester, 'parent_id' => $couple_id]);

                } else if ($userRequest->request_for == 'Husband'){
                    $couple_id = Couple::create([
                        'id' => Uuid::uuid4()->toString(),
                        'husband_id' => $userRequest->request_to,
                        'wife_id' => $userRequest->requester,
                        'manager_id' => $user->manager_id
                    ])->id;
                    UserInfo::where('user_id', $userRequest->requester)->update(['maritial_status'=>1]);
                    UserInfo::where('user_id', $userRequest->request_to)->update(['maritial_status'=>1]);
                    
                    $user = User::where('id', $userRequest->requester)->first();
                    $request_to = User::where('id', $userRequest->request_to)->first();
                    User::where('mother_id', $userRequest->requester)->update(['father_id'=> $userRequest->request_to, 'parent_id' => $couple_id]);
                    User::where('father_id', $userRequest->request_to)->update(['mother_id'=> $userRequest->requester, 'parent_id' => $couple_id]);
                    
                } else if ($userRequest->request_for == 'Father'){
                    // simply add as a father
                    $user = User::where('id', $userRequest->requester)->first();
                    $user->update([
                        "father_id"=> $userRequest->request_to
                    ]);

                    // check father has wife
                    $fatherHasWife = Couple::where('husband_id', $userRequest->request_to)->first();
                    if ($fatherHasWife) {
                        //if has add as my mother
                        $user->update([
                            "parent_id"=> $fatherHasWife->id,
                            "mother_id"=> $fatherHasWife->wife_id
                        ]);
                    } else if ($user->mother_id != null) {
                        #if i already have mother then create couple
                        $couple = Couple::create([
                            'id' => Uuid::uuid4()->toString(),
                            'wife_id' => $user->mother_id,
                            'husband_id' => $userRequest->request_to,
                            'manager_id' => $user->manager_id
                        ]);
                    }

                } else if ($userRequest->request_for == 'Mother'){
                    $user = User::where('id', $userRequest->requester)->first();
                    $user->update([
                        "mother_id"=> $userRequest->request_to
                    ]);

                    // check mother has husband
                    $motherHasHusband = Couple::where('wife_id', $userRequest->request_to)->first();
                    if ($motherHasHusband) {
                        //if has add as my father
                        $user->update([
                            "parent_id"=> $motherHasHusband->id,
                            "father_id"=> $motherHasHusband->wife_id
                        ]);
                    } else if ($user->father_id != null) {
                        #if i already have father then create couple
                        $couple = Couple::create([
                            'id' => Uuid::uuid4()->toString(),
                            'husband_id' => $user->father_id,
                            'wife_id' => $userRequest->request_to,
                            'manager_id' => $user->manager_id
                        ]);
                    }
                } else if ($userRequest->request_for == 'Brother' || $userRequest->request_for == 'Sister'){
                    $user = User::where('id', $userRequest->requester)->first();
                    $request_to = User::where('id', $userRequest->request_to)->first();

                    //check if i have any parent assigned, if yess then assigned to requested to
                    if ($user->mother_id != null || $user->father_id != null) {
                        $request_to->update([
                            "mother_id"=> $user->mother_id,
                            "father_id"=> $user->father_id,
                            "parent_id"=> $user->parent_id
                        ]);
                    } else { //else assign theier parents to me
                        $user->update([
                            "mother_id"=> $request_to->mother_id,
                            "father_id"=> $request_to->father_id,
                            "parent_id"=> $request_to->parent_id
                        ]);
                    }
                } else if ($userRequest->request_for == 'Son' || $userRequest->request_for == 'Daughter'){
                    $user = User::where('id', $userRequest->requester)->first();
                    $request_to = User::where('id', $userRequest->request_to)->first();

                    if ($user->gender_id == 1) {
                        $request_to->update([
                            "father_id"=> $userRequest->requester
                        ]);
    
                        // check father has wife
                        $fatherHasWife = Couple::where('husband_id', $userRequest->requester)->first();
                        if ($fatherHasWife) {
                            //if has add as my mother
                            $request_to->update([
                                "parent_id"=> $fatherHasWife->id,
                                "mother_id"=> $fatherHasWife->wife_id
                            ]);
                        }
                    } else {
                        $request_to->update([
                            "mother_id"=> $userRequest->requester
                        ]);
    
                        // check father has wife
                        $fatherHasWife = Couple::where('wife_id', $userRequest->requester)->first();
                        if ($fatherHasWife) {
                            //if has add as my mother
                            $request_to->update([
                                "parent_id"=> $fatherHasWife->id,
                                "father_id"=> $fatherHasWife->wife_id
                            ]);
                        }
                    }
                }
                
            }
        }

        Requests::where('id', $request->request_id)->update([
            "status"=> $request->action
        ]);

        return response()->json(['action' => true]);
    }
}

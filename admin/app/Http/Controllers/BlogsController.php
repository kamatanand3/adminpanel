<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client as GuzzleClient;

class BlogsController extends Controller
{
    public function index()
    {

        $client = new GuzzleClient([
        ]);

        $r = $client->request('GET', 'http://127.0.0.1:8001/api/blog-list');

        $blogList = $r->getBody()->getContents();  
        // dd($customers);
        $array = json_decode($blogList);
        // print_r($array->data->list); 
        // die; 

        // dd($customers);
        // $client = new Client();
        // $res = $client->request('GET', 'http://127.0.0.1:8001/api/blog-list');

        // $result= $res->getBody();
        // dd($result);
        // die;
        $blogs = $array->data->list;


        return view('blogs.index', compact('blogs'));
    }

    public function addBlogs()
    {

        $client = new GuzzleClient([
        ]);

        $r = $client->request('GET', 'http://127.0.0.1:8001/api/category-list');

        $categoryList = $r->getBody()->getContents();  
        // dd($customers);
        $array = json_decode($categoryList);
        // print_r($array->data->list); 
        // die; 

        // dd($customers);
        // $client = new Client();
        // $res = $client->request('GET', 'http://127.0.0.1:8001/api/blog-list');

        // $result= $res->getBody();
        // dd($result);
        // die;
        $category = $array->data->list;
        // print_r($category);
        // die;


        return view('blogs.addblog', compact('category'));
    }
}

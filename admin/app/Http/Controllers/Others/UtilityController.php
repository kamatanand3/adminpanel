<?php

namespace App\Http\Controllers\Others;

use App\Http\Controllers\Controller;

class UtilityController extends Controller
{
    public function getAppVersions()
    {
        $data = [
            'android_version' => '1.0.2',
            'app_version' => '1.0.1',
        ];
        return $this->success('App Versions', $data, 200);
    }
}
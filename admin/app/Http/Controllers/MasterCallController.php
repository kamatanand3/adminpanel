<?php

namespace App\Http\Controllers;

use App\Models\FamilyTree;
use Illuminate\Http\Request;
use DB;
use Validator;
use Dingo\Api\Routing\Helpers;

class MasterCallController extends Controller
{
    public function mastercall()
	{
		$data['vibhag_list'] = $this->vibhag_list();
		$data['city_list'] = $this->city_list();
		$data['state_list'] = $this->state_list();
		$data['country_list'] = $this->country_list();
		$data['category_list'] = $this->category_list();
		$data['last_name_list'] = $this->last_name_list();
		$data['categories'] = $this->categories();
        return $this->success('Master Call', $data, 200);
    }

    public function vibhag_list(){
        $vibhag_list = DB::table('vibhags')->get();
        return $vibhag_list;
    }

    public function categories(){
        $categories = DB::table('categories')->get();
        return $categories;
    }

    public function city_list(){
        $city_list = DB::table('cities')->where('state_id', '<=', 41)->get();
        return $city_list;
    }

    public function state_list(){
        $state_list = DB::table('states')->get();
        return $state_list;
    }

    public function country_list(){
        $country_list = DB::table('countries')->get();
        return $country_list;
    }
    public function category_list(){
        $category_list = DB::table('categories')->get();
        return $category_list;
    }
    

	public function last_name_list()
	{
        $last_name = DB::table('last_names')->get();
        return $last_name;
	}
}

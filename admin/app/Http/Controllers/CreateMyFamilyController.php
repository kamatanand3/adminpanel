<?php

namespace App\Http\Controllers;

use App\Couple;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;
use App\Models\FamilyTree;
use Illuminate\Support\Facades\DB;

class CreateMyFamilyController extends Controller
{
    public function index(){
        $user = auth()->user();
        // dd($user->id);
        $userFamilyMembers = User::where('manager_id', $user->id)->get();
        $my_family_tree = DB::table('family_tree')->where('user_id', $user->id)->get();
        //dd($userFamily);
        $data = [
            'userFamilyMembers'  => $userFamilyMembers,
            'my_family_tree'   => $my_family_tree,
            'my_family_tree_json' => json_encode($my_family_tree)
        ];
        \Log::info($my_family_tree);
        return view('create-my-family.form', compact('data', 'user'));
    }

    public function familyList(){
        $list = DB::table('family_tree')->where('user_id', auth()->user()->id)->get();
        $userFamilyMembers = User::where('manager_id', auth()->user()->id)->get();
        return $this->success('Family List', ['list' => $list, 'userFamilyMembers'=>$userFamilyMembers] , 200);
    }

    public function addFamilyMember(Request $request, $id){

        $relation_with_user = User::where('id', $request->relation_with)->first();

        if ($request->id == 'new') {
            $manager_id = auth()->user()->id;
            if ($id == 'new') {
                $new_user_id = Uuid::uuid4()->toString();
                $user = User::create([
                    'id'=>$new_user_id,
                    'nickname'=>$request->first_name,
                    'name' =>$request->first_name,
                    'gender_id' => $request->gender,
                    'dob' => $request->dob,
                    'manager_id' => $manager_id,
                    'user_type' => 9,
                ]);
                $user->save();
                $user_info = UserInfo::create([
                    'user_id'=>$new_user_id,
                    'last_name'=>$request->last_name,
                    'middle_name'=>$request->middle_name,
                    'native_place' => $request->native_place,
                ]);
                $user_info->save();
            } else {
                $new_user_id = $id;
            }
    
            if($request->relation == "Father"){
                $relation_with_user->update(['father_id'=>$new_user_id]);
                UserInfo::where('user_id', $new_user_id)->update(['maritial_status'=>1]);
                if($relation_with_user->mother_id != null){
                    $couple_id =  Uuid::uuid4()->toString();
                    Couple::create([
                        'id'=>$couple_id,
                        'husband_id'=>$new_user_id,
                        'wife_id'=>$relation_with_user->mother_id,
                        'manager_id'=> Auth::user()->id,
                    ]);
                    $relation_with_user->update(['parent_id'=>$couple_id]);
                }
            }
            if($request->relation == "Mother"){
                $relation_with_user->update(['mother_id'=>$new_user_id]);
                UserInfo::where('user_id', $new_user_id)->update(['maritial_status'=>1]);
                if($relation_with_user->father_id != null){
                    $couple_id =  Uuid::uuid4()->toString();
                    Couple::create([
                        'id'=>$couple_id,
                        'husband_id'=>$relation_with_user->father_id,
                        'wife_id'=>$new_user_id,
                        'manager_id'=> Auth::user()->id,
                    ]);
                    $relation_with_user->update(['parent_id'=>$couple_id]);
                }
            
            }
    
            $new_user = User::where('id',$new_user_id)->first();
            $new_user_info = UserInfo::where('user_id', $new_user_id)->first();
    
            FamilyTree::create([
                'id'=>$new_user_id,
                'user_id' => auth()->user()->id,
                'salutation' => $request->salutation,
                'first_name' => $new_user->name,
                'middle_name'=> $new_user_info->middle_name,
                'last_name'=> $new_user_info->last_name,
                'relation'=> $request->relation,
                'relation_with'=>  $relation_with_user->name,
                'relation_with_id'=>  $request->relation_with,
                'dob' => $new_user->dob,
                'native_place'=> $new_user_info->native_place
            ]);
    
            $values = [
                'salutation' => $new_user->salutation,
                'first_name' => $new_user->name,
                'middle_name'=> $new_user_info->middle_name,
                'last_name'=> $new_user_info->last_name,
                'relation'=> $request->relation,
                'relation_with'=>  $relation_with_user->name,
                'relation_with_id'=>  $request->relation_with,
                'dob' => $new_user->dob,
                'native_place'=> $new_user_info->native_place
            ];
        } else {
            FamilyTree::where('id', $request->id)->update([
                'salutation' => $request->salutation,
                'first_name' => $request->first_name,
                'middle_name'=> $request->middle_name,
                'last_name'=> $request->last_name,
                'relation'=> $request->relation,
                'relation_with'=>  $relation_with_user->name,
                'relation_with_id'=>  $request->relation_with,
                'dob' => $request->dob,
                'native_place'=> $request->native_place
            ]);
            User::where('id', $request->id)->update([
                'nickname'=>$request->first_name,
                'name' =>$request->first_name,
                'gender_id' => $request->gender,
                'dob' => $request->dob
            ]);
            UserInfo::where('user_id', $request->id)->update([
                'last_name'=>$request->last_name,
                'middle_name'=>$request->middle_name,
                'native_place' => $request->native_place,
            ]);
            $values = [];
        }
        if (request()->wantsJson()) {
            $data = [
                'message' => 'success',
                'values' => $values
            ];
            return $this->success('create-my-family', $data , 200);
        }
    }


    public function checkMyFamilyUser(Request $request){
        $user = auth()->user();
        $same_first_name = User::where('name', $request->first_name)->where('id', "!=" ,$user->id)->get();
        $usersList = [];
        if($same_first_name != null) {
            foreach ($same_first_name as $s) {
                $user_info = UserInfo::where('user_id', $s->id)->where('last_name', $request->last_name)->first();
                if ($user_info) {
                    $father = User::where('id',$s->father_id)->first();
                    if($father != null){
                        $father_name = $father->name;
                    }
                    else{
                        $father_name = null;
                    }
                    $mother = User::where('id',$s->mother_id)->first();
                    if($mother != null){
                        $mother_name = $mother->name;
                    }
                    else{
                        $mother_name = null;
                    }
                    if($s->photo_path == null){
                        $photo_path = null;
                    }
                    else {
                        $photo_path = url('/uploads/'.$s->photo_path);
                    }
                    $usersList[] = [
                        'id'=>$s->id,
                        'first_name'=> $s->name,
                        'dob'=>$s->dob, 
                        'last_name' => $user_info->last_name, 
                        'photo_path'=> $photo_path,
                        'gender_id' =>$s->gender_id,
                        'father_name' =>$father_name,
                        'mother_name' => $mother_name,
                    ];
                }
            }
            if (count($usersList) > 0) {
                if (request()->wantsJson()) {
                    $data = [
                        'user_exist' => true,
                        'user_list' => $usersList
                    ];
                    return $this->success('create-my-family', $data , 200);
                } else {
                    return response()->json(['user_exist' => true, 'user_list'=>$usersList]);
                }
            }
        }
        if (request()->wantsJson()) {
            $data = [
                'user_exist' => false
            ];
            return $this->success('create-my-family', $data , 200);
        }
        return response()->json(['user_exist' => false]);
    }

    public function destroy(Request $request) {
        try {
            FamilyTree::where("id", $request->user_id)->delete();
            if ($request->relation == 'Father') {
                Couple::where("husband_id", $request->user_id)->delete();
                User::where("father_id", $request->user_id)->update(['father_id' => null]);
            } else {
                Couple::where("wife_id", $request->user_id)->delete();
                User::where("mother_id", $request->user_id)->update(['mother_id' => null]);
            }
            UserInfo::where("user_id", $request->user_id)->delete();
            User::where("id", $request->user_id)->delete();
        } catch (Exception $e) {
            \Log::info($e);
            return $this->failure('Something went wrong !!', null, 500);
        }
        return $this->success('Deleted successfully',null , 200);
    }
    
}

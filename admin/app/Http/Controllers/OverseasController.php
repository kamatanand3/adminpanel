<?php

namespace App\Http\Controllers;

use App\Couple;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;
use App\Models\FamilyTree;
use App\Models\Overseas;

class OverseasController extends Controller
{
    public function index(){
        $user = auth()->user();
        $overseas_family = Overseas::where('created_id', $user->id)->get();
        $data = [
            'overseas_family'   => $overseas_family
        ];
        return view('overseas.form', compact('data', 'user'));
    }

    public function familyList(){
        $list = Overseas::where('created_id', auth()->user()->id)->get();
        return $this->success('Overseas List', ['list' => $list] , 200);
    }

    public function addOverseas(Request $request){
        $random_id = Uuid::uuid4()->toString();
        $user = auth()->user();

        Overseas::create([
            'id'=>$random_id,
            'name' => $request->name,
            'mobile_code'=> $request->mobile_code,
            'mobile'=> $request->mobile,
            'email'=> $request->email,
            'address'=>  $request->address,
            'relation' => $request->relation,
            'created_id' => $user->id,
            'updated_id' => $user->id
        ]);
        if (request()->wantsJson()) {
            $data = [
                'message' => 'success'
            ];
            return $this->success('create-my-family', $data , 200);
        }
    }
    
}

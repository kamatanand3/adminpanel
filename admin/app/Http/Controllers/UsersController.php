<?php

namespace App\Http\Controllers;

use App\Couple;
use App\Http\Requests\Users\UpdateRequest;
use App\Jobs\Users\DeleteAndReplaceUser;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Storage;
use Helper;
use App\Models\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

use function PHPSTORM_META\elementType;

class UsersController extends Controller
{
    /**
     * Search user by keyword.
     *
     * @return \Illuminate\View\View
     */
    public function search(Request $request)
    {
        // dd($request->all());
        $q = $request->get('q');
        $users = [];

        if ($q) {
            $users = User::select("users.*", "user_info.middle_name", "user_info.last_name", "user_info.user_id")->with('father', 'mother')->where(function ($query) use ($q) {
                $query->where('users.name', 'like', '%'.$q.'%');
            })
            ->join("user_info","user_info.user_id","=","users.id")
            ->where('user_type', '!=', 9)
                ->orderBy('name', 'asc')
                ->paginate(24);
        }
        // dd($users);
        if (request()->wantsJson()) {
            // return response()->json(['users' => $users ]);
            return $this->success('Profile-search', $users , 200);
        }
        else {
        return view('users.search', compact('users'));
        }
    }

    /**
     * Display the specified User.
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function show(User $user)
    {
        $usersMariageList = $this->getUserMariageList($user);
        $allMariageList = $this->getAllMariageList();
        $malePersonList = $this->getPersonList(1);
        $femalePersonList = $this->getPersonList(2);
        $father = User::where('id', $user->father_id)->first();
        $mother = User::where('id', $user->mother_id)->first();
        $userFamilyMembers = User::where('manager_id', $user->id)->get();

        $relation_with_loggedIn_user = null;
        foreach ($user->childs as $c){
            if(($c->id == auth()->user()->id)){
                $relation_with_loggedIn_user = 'Parent';
            }
        };
        foreach ($user->siblings() as $s){
            if($s->id == auth()->user()->id){
                $relation_with_loggedIn_user = 'Sibling';
            }
        };
        if(($user->father_id == auth()->user()->id) || ($user->mother_id == auth()->user()->id) ){
            $relation_with_loggedIn_user = 'Children';
        }
        foreach ($user->couples as $couple){
            if($couple->id == auth()->user()->id){
                $relation_with_loggedIn_user = 'Spouse';
            }
        };
        
        $user_info = UserInfo::where('user_id', $user->id)->first();

        if ($user_info->category) {
            $category_name = DB::table('categories')->where('id', $user_info->category)->first();
            $category_name = $category_name->name;
        } else {
            $category_name = '';
        }
        
        if ($user_info->current_vibhag) {
            $current_vibhag = DB::table('vibhags')->where('id', $user_info->current_vibhag)->first();
            $current_vibhag = $current_vibhag->name;
        } else {
            $current_vibhag = '';
        }
        
        if ($user_info->current_city) {
            $current_city = DB::table('cities')->where('id', $user_info->current_city)->first();
            $current_city = $current_city->name;
        } else {
            $current_city = '';
        }
        
        if ($user_info->current_state) {
            $current_state = DB::table('states')->where('id', $user_info->current_state)->first();
            $current_state = $current_state->name;
        } else {
            $current_state = '';
        }
        
        if ($user_info->current_country) {
            $current_country = DB::table('countries')->where('id', $user_info->current_country)->first();
            $current_country = $current_country->name;
        } else {
            $current_country = '';
        }
        
        if ($user_info->permanent_vibhag) {
            $permanent_vibhag = DB::table('vibhags')->where('id', $user_info->permanent_vibhag)->first();
            $permanent_vibhag = $permanent_vibhag->name;
        } else {
            $permanent_vibhag = '';
        }
        
        if ($user_info->permanent_city) {
            $permanent_city = DB::table('cities')->where('id', $user_info->permanent_city)->first();
            $permanent_city = $permanent_city->name;
        } else {
            $permanent_city = '';
        }
        
        if ($user_info->permanent_state) {
            $permanent_state = DB::table('states')->where('id', $user_info->permanent_state)->first();
            $permanent_state = $permanent_state->name;
        } else {
            $permanent_state = '';
        }
        
        if ($user_info->permanent_country) {
            $permanent_country = DB::table('countries')->where('id', $user_info->permanent_country)->first();
            $permanent_country = $permanent_country->name;
        } else {
            $permanent_country = '';
        }
        
        if ($user_info->occupation_vibhag) {
            $occupation_vibhag = DB::table('vibhags')->where('id', $user_info->occupation_vibhag)->first();
            $occupation_vibhag = $occupation_vibhag->name;
        } else {
            $occupation_vibhag = '';
        }
        
        if ($user_info->occupation_city) {
            $occupation_city = DB::table('cities')->where('id', $user_info->occupation_city)->first();
            $occupation_city = $occupation_city->name;
        } else {
            $occupation_city = '';
        }
        
        if ($user_info->occupation_state) {
            $occupation_state = DB::table('states')->where('id', $user_info->occupation_state)->first();
            $occupation_state = $occupation_state->name;
        } else {
            $occupation_state = '';
        }
        
        if ($user_info->occupation_country) {
            $occupation_country = DB::table('countries')->where('id', $user_info->occupation_country)->first();
            $occupation_country = $occupation_country->name;
        } else {
            $occupation_country = '';
        }

        if (request()->wantsJson()) {
            $data = [
            'user'=> $user,
            'user_info' => $user_info,
            'father' => $father,
            'mother' => $mother,
            'childs' => $user->childs,
            'sibilings' => $user->siblings(),
            'relation_with_loggedIn_user' => $relation_with_loggedIn_user,
            'usersMariageList' => $usersMariageList,
            'malePersonList'   => $malePersonList,
            'femalePersonList' => $femalePersonList,
            'allMariageList'   => $allMariageList,
            'category_name'   => $category_name,
            'current_city'   => $current_city,
            'current_vibhag'   => $current_vibhag,
            'current_state'   => $current_state,
            'current_country'   => $current_country,
            'permanent_vibhag' => $permanent_vibhag,
            'permanent_city'   => $permanent_city,
            'permanent_state'   => $permanent_state,
            'permanent_country'   => $permanent_country,
            'occupation_vibhag'   => $occupation_vibhag,
            'occupation_city'   => $occupation_city,
            'occupation_state'   => $occupation_state,
            'occupation_country'   => $occupation_country,
            'familyMembersForCreateMyFamilyTree'   => $userFamilyMembers,
        ];
            return $this->success('User Profile', $data , 200);

        }
        else {
        return view('users.show', [
            'user'             => $user,
            'user_info' => $user_info,
            'usersMariageList' => $usersMariageList,
            'malePersonList'   => $malePersonList,
            'femalePersonList' => $femalePersonList,
            'allMariageList'   => $allMariageList,
            'category_name'   => $category_name,
            'current_city'   => $current_city,
            'current_vibhag'   => $current_vibhag,
            'current_state'   => $current_state,
            'current_country'   => $current_country,
            'permanent_vibhag' => $permanent_vibhag,
            'permanent_city'   => $permanent_city,
            'permanent_state'   => $permanent_state,
            'permanent_country'   => $permanent_country,
            'occupation_vibhag'   => $occupation_vibhag,
            'occupation_city'   => $occupation_city,
            'occupation_state'   => $occupation_state,
            'occupation_country'   => $occupation_country
        ]);
        }
    }

    /**
     * Display the user's family chart.
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function chart(User $user)
    {
        $father = $user->father_id ? $user->father : null;
        $mother = $user->mother_id ? $user->mother : null;

        $fatherGrandpa = $father && $father->father_id ? $father->father : null;
        $fatherGrandma = $father && $father->mother_id ? $father->mother : null;

        $motherGrandpa = $mother && $mother->father_id ? $mother->father : null;
        $motherGrandma = $mother && $mother->mother_id ? $mother->mother : null;

        $childs = $user->childs;
        $colspan = $childs->count();
        $colspan = $colspan < 4 ? 4 : $colspan;

        $siblings = $user->siblings();

        if (request()->wantsJson()) {
            return response()->json(['user'=>$user, 'childs'=>$childs, 'father'=>$father, 'mother'=>$mother, 
            'fatherGrandpa'=>$fatherGrandpa,
            'fatherGrandma'=>$fatherGrandma, 'motherGrandpa'=>$motherGrandpa, 'motherGrandma'=>$motherGrandma,
            'siblings'=>$siblings, 'colspan'=>$colspan]);
        }
        else {
        return view('users.chart', compact(
            'user', 'childs', 'father', 'mother', 'fatherGrandpa',
            'fatherGrandma', 'motherGrandpa', 'motherGrandma',
            'siblings', 'colspan'
        ));
    }
    }

    /**
     * Show user family tree.
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function tree_og(User $user)
    {
        //dd($user->id);

        return view('users.tree', compact('user'));
    }
    public function tree(User $user)
    {
        
        $child = $user->childs()->get();
        $couple = $user->couples()->first();
        if (request()->wantsJson()) {
            // return response()->json(['user'=>$user, 'couple'=>$couple]);
            $data = [
                'user' => $user,
                'couple' => $couple
            ];
            return $this->success('Tree', $data , 200);
        }
        else {
        return view('users.tree', compact('user', 'couple'));
        }
    }
    public function mobiletree(User $user)
    {
        $self = new \StdClass();
        $self->user = $user;
        $childs = [];
        foreach ($user->childs as $child) {
            $child = User::where('id', $child->id)->first();
            $grandchilds = [];
            foreach ($child->childs as $grandchild) {
                $grandchild = User::where('id', $grandchild->id)->first();
                $grandchild['couples'] = $grandchild->couples;
                $grandchild['spouse'] = $grandchild->couples;
                $grandchilds[] = $grandchild;
            }
            $child['childrens'] = $grandchilds;
            $child['spouse'] = $child->couples;
            
            $childs[] = $child;
        }
        $self->user['father_id'] = $user->father_id;
        $self->user['mother_id'] = $user->mother_id;
        $self->user['spouse'] = $user->couples;
        $self->user['childrens'] = $childs;
        $data = [
            'user' => $self
        ];
        return $this->success('Tree', $data , 200);
    }
    /**
     * Show the form for editing the specified User.
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        // dd($user->id);
        $userInfo = UserInfo::where('user_id', $user->id)->first();

        if($userInfo == null) {
            $user_info = UserInfo::create([
                'user_id' => $user->id,
                'email' => $user->email,
            ]);
            $user_info->save();
        }

        $vibhag = DB::table('vibhags')->where('id',$userInfo->current_vibhag)->select('name')->first();
        if($vibhag != null){
            $userInfo->current_vibhag_name = $vibhag->name;
        }
        $city = DB::table('cities')->where('id',$userInfo->current_city)->select('name')->first();
        if($city != null){
            $userInfo->current_city_name = $city->name;
        }
        $state = DB::table('states')->where('id',$userInfo->current_state)->select('name')->first();
        if($state != null){
            $userInfo->current_state_name = $state->name;
        }
        $country = DB::table('countries')->where('id',$userInfo->current_country)->select('name')->first();
        if($country != null){
            $userInfo->current_country_name = $country->name;
        }


        $vibhag = DB::table('vibhags')->where('id',$userInfo->permanent_vibhag)->select('name')->first();
        if($vibhag != null){
            $userInfo->permanent_vibhag_name = $vibhag->name;
        }
        $city = DB::table('cities')->where('id',$userInfo->permanent_city)->select('name')->first();
        if($city != null){
            $userInfo->permanent_city_name = $city->name;
        }
        $state = DB::table('states')->where('id',$userInfo->permanent_state)->select('name')->first();
        if($state != null){
            $userInfo->permanent_state_name = $state->name;
        }
        $country = DB::table('countries')->where('id',$userInfo->permanent_country)->select('name')->first();
        if($country != null){
            $userInfo->permanent_country_name = $country->name;
        }




        $vibhag = DB::table('vibhags')->where('id',$userInfo->occupation_vibhag)->select('name')->first();
        if($vibhag != null){
            $userInfo->occupation_vibhag_name = $vibhag->name;
        }
        $city = DB::table('cities')->where('id',$userInfo->occupation_city)->select('name')->first();
        if($city != null){
            $userInfo->occupation_city_name = $city->name;
        }
        $state = DB::table('states')->where('id',$userInfo->occupation_state)->select('name')->first();
        if($state != null){
            $userInfo->occupation_state_name = $state->name;
        }
        $country = DB::table('countries')->where('id',$userInfo->occupation_country)->select('name')->first();
        if($country != null){
            $userInfo->occupation_country_name = $country->name;
        }

        //dd($userInfo);
        $categories = DB::table('categories')->orderBy('name')->get();
        $vibhags = DB::table('vibhags')->orderBy('name')->get();
        $countries = DB::table('countries')->orderBy('name')->get();

        $states = [];
        if($userInfo->current_country != null){
            $states = DB::table('states')->where('countryID', $userInfo->current_country)->orderBy('name')->get();
        } else {
            $states = DB::table('states')->where('countryID', 101)->orderBy('name')->get();
        }
        $cities = [];
        if($userInfo->current_state != null){
            $cities = DB::table('cities')->where('state_id', $userInfo->current_state)->orderBy('name')->get();
        }

        $per_states = [];
        if($userInfo->permanent_country != null){
            $per_states = DB::table('states')->where('countryID', $userInfo->permanent_country)->orderBy('name')->get();
        } else {
            $per_states = DB::table('states')->where('countryID', 101)->orderBy('name')->get();
        }
        $per_cities = [];
        if($userInfo->permanent_state != null){
            $per_cities = DB::table('cities')->where('state_id', $userInfo->permanent_state)->orderBy('name')->get();
        }

        $occ_states = [];
        if($userInfo->occupation_country != null){
            $occ_states = DB::table('states')->where('countryID', $userInfo->occupation_country)->orderBy('name')->get();
        } else {
            $occ_states = DB::table('states')->where('countryID', 101)->orderBy('name')->get();
        }
        $occ_cities = [];
        if($userInfo->occupation_state != null){
            $occ_cities = DB::table('cities')->where('state_id', $userInfo->occupation_state)->orderBy('name')->get();
        }

        
        //dd($userInfo, $user);
        $this->authorize('edit', $user);

        $replacementUsers = [];
        if (request('action') == 'delete') {
            $replacementUsers = $this->getPersonList($user->gender_id);
        }

        $validTabs = ['death', 'contact_address', 'login_account'];
        if (request()->wantsJson()) {
            $data = [
                'user' => $user,
                'user_info' => $userInfo,
                'categories' => $categories,
                'vibhags' => $vibhags
            ];
            return $this->success('Edit User', $data , 200);
            //return response()->json(['user'=>$user]);
        }
        else {
        // return view('users.edit', compact('user', 'replacementUsers', 'validTabs'));
        return view('users.new-edit', compact('user', 'userInfo', 'categories', 'vibhags', 'countries', 'states', 'cities', 'per_states', 'per_cities', 'occ_states', 'occ_cities'));
        }
    }
    public function edit_og(User $user)
    {
        
        $this->authorize('edit', $user);

        $replacementUsers = [];
        if (request('action') == 'delete') {
            $replacementUsers = $this->getPersonList($user->gender_id);
        }

        $validTabs = ['death', 'contact_address', 'login_account'];
        if (request()->wantsJson()) {
            return response()->json(['user'=>$user, 'replacementUsers'=>$replacementUsers, 'validTabs'=>$validTabs]);
        }
        else {
        return view('users.edit', compact('user', 'replacementUsers', 'validTabs'));
        }
    }

    /**
     * Update the specified User in storage.
     *
     * @param  \App\Http\Requests\Users\UpdateRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $userInfo = UserInfo::where('user_id',$user->id)->first();
        
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'middle_name' => 'required',
            'last_name' => 'required',
            'dob' => 'required',
            'gender' => 'required',
        ]);

        if($validator->fails()){
            return $this->failure($validator->errors()->toJson(), null, 400);
        }

        if ($request->visibility == "on") {
            $visibility = 1;
        }
        else {
            $visibility = 0;
        }
        if ($request->alternate_visibility == "on") {
            $alternate_visibility = 1;
        }
        else {
            $alternate_visibility = 0;
        }

        
        if (request()->wantsJson()) {
            $visibility = 1;
            $alternate_visibility = 1;
        
            if(!empty($request->email) && $request->email != $user->email ){
                $email = User::where('email',$request->email)->first();
                if($email != null){
                    return $this->failure('Email Alreay Exists !!', null, 500);
                }
            }
            
            if(!empty($request->mobile) && $request->mobile != $userInfo->mobile ){
                $mobile = UserInfo::where('mobile',$request->mobile)->orWhere('alternate_mobile', $request->mobile)->first();
                if($mobile != null){
                    return $this->failure('Mobile Alreay Exists !!', null, 500);
                }
            }
        }

        //dd($request->all());
        
        //dd($city_id);

        $current_city_id = $request->current_city;
        $current_state_id = $request->current_state;
        $current_country_id = $request->current_country;

        $permanent_city_id = $request->permanent_city;
        $permanent_state_id = $request->permanent_state;
        $permanent_country_id = $request->permanent_country;

        $occupation_city_id = $request->occupation_city;
        $occupation_state_id = $request->occupation_state;
        $occupation_country_id =  $request->occupation_country;
        //dd($current_city_id, $current_country_id, $current_state_id, $permanent_city_id, $permanent_state_id, $permanent_country_id);

        if($request->permanent_address_check == "on") {
            $permanent_building  = $request->current_building ;
            $permanent_landmark = $request->current_landmark ;
            $permanent_road = $request->current_road ;
            $permanent_pincode = $request->current_pincode ;
            $permanent_vibhag = $request->current_vibhag ;
            $permanent_city = $current_city_id ;
            $permanent_state = $current_state_id ;
            $permanent_country = $current_country_id ;
        }
        else {
            $permanent_building  = $request->permanent_building ;
            $permanent_landmark = $request->permanent_landmark ;
            $permanent_road = $request->permanent_road ;
            $permanent_pincode = $request->permanent_pincode ;
            $permanent_vibhag = $request->permanent_vibhag ;
            $permanent_city = $permanent_city_id ;
            $permanent_state = $permanent_state_id ;
            $permanent_country = $permanent_country_id ;
        }
        //dd($request->all());
        
        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $user->update([
            "name"=> $request->first_name,
            "gender_id" => $request->gender,
            "dob"=>$request->dob ,
            "email"=> $request->email ,
            "city"=> $request->current_city,
            "phone" => $request->mobile
        ]);

        $userInfo->update([
            "last_name" => $request->last_name,
            "middle_name" => $request->middle_name,
            "last_name_original" => $request->last_name_original,
            "native_place" => $request->native_place,
            "blood_group" =>$request->bloodgroup, 
            "mobile_code" => $request->mobile_code,
            "mobile" => $request->mobile,
            'visibility' => $visibility,

            "alternate_mobile_code" => $request->alternate_mobile_code,
            "alternate_mobile" => $request->alternate_mobile,
            'alternate_mobile_visibility' => $alternate_visibility,
            "birth_place" => $request->birth_place,
            "gotra" => $request->gotra,
            "divyang" => $request->divyang == 0 || $request->divyang == '0' || $request->divyang == 'null' ? null : $request->divyang,
            "mosad" => $request->mosad,
            'occupation_building' => $request->occupation_building,
            'occupation_landmark' => $request->occupation_landmark,
            'occupation_road' => $request->occupation_road,
            'occupation_vibhag'   => $request->occupation_vibhag,
            'occupation_pincode' => $request->occupation_pincode,
            'occupation_city' => $occupation_city_id,
            'occupation_state' => $occupation_state_id,
            'occupation_country' => $occupation_country_id,

            'current_building' => $request->current_building,
            'current_landmark' => $request->current_landmark,
            'current_road' => $request->current_road,
            'current_pincode' => $request->current_pincode,
            'current_vibhag' => $request->current_vibhag,
            'current_city' => $current_city_id,
            'current_state' => $current_state_id,
            'current_country' => $current_country_id,
            
            'permanent_building' => $permanent_building,
            'permanent_landmark' => $permanent_landmark,
            'permanent_road' => $permanent_road,
            'permanent_pincode' => $permanent_pincode,
            'permanent_vibhag' => $permanent_vibhag,
            'permanent_city' => $permanent_city,
            'permanent_state' => $permanent_state,
            'permanent_country' => $permanent_country,
            'education' => $request->education,
            'college' => $request->college,
            'passout_year' => $request->year,
            'university' => $request->university,
            'occupation' => $request->occupation,
            'category' => $request->category,
            'company_name' => $request->company_name,
            // 'contact_person_name' => $request->contact_person_name,
            'contact_person_mobile' => $request->contact_person_mobile,
            // 'contact_person_address' => $request->contact_person_address,
            "charity" => $request->charity ,
            "charity_organization" => $request->charity_organization ,
            "charity_designation" => $request->charity_designation ,
            "serve" => $request->serve ,
            "serve_help" => $request->serve_help ,
            "donate" => $request->donate ,
            "donate_part" => json_encode($request->donate_part),
            "marital_status" => $request->marital_status,
        ]);

        if (request()->wantsJson()) {
            // return response()->json(['user_id'=>$user->id]);
            $data = ["user_id" => $user->id];
            return $this->success('User Update', $data, 200);
        }
        else {
        return redirect()->route('users.show', $user->id);
        }
    }

    public function getStateId($name){
        $state = DB::table('states')->where('name',$name)->first();
        if($state != null){
            $id = $state->id;
        }
        else {
            $id = null;
        } 
        return $id;
    }

    public function getCityId($name){
        if($name == null){
            return null;
        }
        else {
            $city_exist = DB::table('cities')
                            ->where(DB::raw('lower(name)'), 'like', '%' . strtolower($name) . '%')
                            ->whereRaw('LENGTH(name) = ?',[strlen($name)])
                            ->first();
            if($city_exist == null){
                        DB::table('cities')->insert(['name' => $name]);
                        $city = DB::table('cities')->where('name',$name)->first();
                        if($city == null){
                            return null;
                        }
                        else{
                            return $city->id;
                        }
            }
            else{
                return $city_exist->id;
            }
        }
        // $city = DB::table('cities')->where('name',$name)->first();
        // if($city != null){
        //     $id = $city->id;
        // }
        // else {
        //     $id = null;
        // } 
        // return $id;
    }
    public function getCountryId($name){
        $country = DB::table('countries')->where('name',$name)->first();
        if($country != null){
            $id = $country->id;
        }
        else {
            $id = null;
        } 
        return $id;
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, User $user)
    {
        $this->authorize('delete', $user);

        if ($request->has('replace_delete_button')) {
            $attributes = $request->validate([
                'replacement_user_id' => 'required|exists:users,id',
            ], [
                'replacement_user_id.required' => __('validation.user.replacement_user_id.required'),
            ]);

            $this->dispatchNow(new DeleteAndReplaceUser($user, $attributes['replacement_user_id']));

            return redirect()->route('users.show', $attributes['replacement_user_id']);
        }

        $request->validate([
            'user_id' => 'required',
        ]);

        if ($request->get('user_id') == $user->id && $user->delete()) {
            return redirect()->route('users.search');
        }

        return back();
    }

    /**
     * Upload users photo.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function photoUpload(Request $request, User $user)
    {
        //dd($request->file);
        // $request->validate([
        //     'photo' => 'required|image|max:200',
        // ]);
        if (Storage::exists($user->photo_path)) {
            Storage::delete($user->photo_path);
        }

        \Log::info($request);

        // $user->photo_path = $request->photo->store('images');
        $user->photo_path = $request->file->store('images');

        $user->save();
        if (request()->wantsJson()) {
            return $this->success('Photo uploaded' , 200);
        }
        else {
            return back();
        }
    }

    public function photoDelete(Request $request){
        //dd($request->user);
        $users = User::where('id', $request->user)->first();
        if (Storage::exists($users->photo_path)) {
            Storage::delete($users->photo_path);
        }
        User::where('id',$request->user)->update(['photo_path' => null]);
        //dd($userss->id, $userss->photo_path);
        
        return back();
    }

    /**
     * Get User list based on gender.
     *
     * @param int $genderId
     *
     * @return \Illuminate\Support\Collection
     */
    private function getPersonList(int $genderId)
    {
        return User::where('gender_id', $genderId)->pluck('nickname', 'id');
    }

    /**
     * Get marriage list of a user.
     *
     * @param \App\User $user
     *
     * @return array
     */
    private function getUserMariageList(User $user)
    {
        $usersMariageList = [];

        foreach ($user->couples as $spouse) {
            $usersMariageList[$spouse->pivot->id] = $user->name.' & '.$spouse->name;
        }

        return $usersMariageList;
    }

    /**
     * Get all marriage list.
     *
     * @return array
     */
    private function getAllMariageList()
    {
        $allMariageList = [];

        foreach (Couple::with('husband', 'wife')->get() as $couple) {
            $allMariageList[$couple->id] = $couple->husband->name.' & '.$couple->wife->name;
        }

        return $allMariageList;
    }

    public function openapi() {
        $users = User::select('id', 'email')->get();
        //dd($users);
        foreach ($users as $user) {
            $userInfo = UserInfo::where('user_id', $user->id)->first();
            if($userInfo == null) {
                $user_info = UserInfo::create([
                    'user_id' => $user->id,
                    'email' => $user->email,
                ]);
                $user_info->save();
            }
        }
    }

    public function checkUserPresent(Request $request) {
        $userInfo = UserInfo::where('mobile', $request->mobile)->first();
        // dd($userInfo);
        if($userInfo != null) {
            $couple = Couple::where('wife_id', $userInfo->user_id)->first();
            if ($couple == null) {
                $user = User::where('id', $userInfo->user_id)->where('name', $request->first_name)->where('dob', $request->dob)->where('gender_id', $request->gender_code)->first();
                if($user != null) {
                    $fatherName = '';
                    $motherName = '';
                    $photo_path = '';

                    if ($user->father_id != null) {
                        $userFather = User::where('id', $user->father_id)->first();
                        $userFatherInfo = UserInfo::where('user_id', $userFather->id)->first();
                        $fatherName = $userFather->name.' '.$userFatherInfo->middle_name. ' '.$userFatherInfo->last_name;
                    }
                    if ($user->mother_id != null) {
                        $userMother = User::where('id', $user->mother_id)->first();
                        $userMotherInfo = UserInfo::where('user_id', $userMother->id)->first();
                        $motherName = $userMother->name.' '.$userMotherInfo->middle_name. ' '.$userMotherInfo->last_name;
                    }
                    
                    if ($user->photo_path != null) {
                        // $photo_path = Helper::userPhoto($user, ['style' => 'width:100%;max-width:300px']);
                        $photo_path = url('/uploads/'.$user->photo_path);
                    }
                    
                    // dd("in");
                    return response()->json([
                        'user_already_linked' => false,
                        'user_id' => $user->id,
                        'photo_path' => $photo_path,
                        'first_name' => $user->name,
                        'middle_name' => $userInfo->middle_name,
                        'last_name' => $userInfo->last_name,
                        'dob' => $user->dob,
                        'gender_id' => $user->gender_id,
                        'father_name' => $fatherName,
                        'mother_name' => $motherName,
                    ]);
                }
            } 
        }
        return response()->json(['user_already_linked' => true]);
    }

    public function checkMobile(Request $request){
        
       $user_info = UserInfo::where('mobile',$request->mobile)->orWhere('alternate_mobile', $request->mobile)->first();
       //dd($mobile);
        if($user_info != null){
            return response()->json(['mobile_exist' => true]);
        }
        else{
            return response()->json(['mobile_exist' => false]);
        }
    }
    public function checkEmail(Request $request){
        $email = User::where('email',$request->email)->first();
         if($email != null){
             return response()->json(['email_exist' => true]);
         }
         else{
             return response()->json(['email_exist' => false]);
         }
     }

     public function checkUpdateEmail(Request $request){
        $user = User::where('id',$request->id)->first();
        if($request->email != $user->email ){
            $email = User::where('email',$request->email)->first();
            if($email != null){
                return response()->json(['email_exist' => true]);
            }
            else{
                return response()->json(['email_exist' => false]);
            }
        }
        else{
            return response()->json(['email_exist' => false]);
        }
     }

     public function checkUpdateMobile(Request $request){
        $user = UserInfo::where('user_id',$request->id)->first();
        if($request->mobile != $user->mobile ){
            $mobile = UserInfo::where('mobile',$request->mobile)->orWhere('alternate_mobile', $request->mobile)->first();
            if($mobile != null){
                return response()->json(['mobile_exist' => true]);
            }
            else{
                return response()->json(['mobile_exist' => false]);
            }
        }
        else{
            return response()->json(['mobile_exist' => false]);
        }
     }

     public function checkUpdateAlternateMobile(Request $request){
        $user = UserInfo::where('user_id',$request->id)->first();
        if($request->mobile != $user->alternate_mobile ){
            $mobile = UserInfo::where('mobile',$request->mobile)->orWhere('alternate_mobile', $request->mobile)->first();
            if($mobile != null){
                return response()->json(['mobile_exist' => true]);
            }
            else{
                return response()->json(['mobile_exist' => false]);
            }
        }
        else{
            return response()->json(['mobile_exist' => false]);
        }
     }

    //  public function changeState(){
    //     $infos = UserInfo::select("current_state")->get();
    //     foreach($infos as $info){
    //         // if($info->current_state != null){
    //             //$state = DB::table('states')->where('name', 'LIKE', "%{$info->current_state}%")->first();
    //             //if($state != null){
    //                 dump($info->current_state);
    //             // }
    //             //dump($info->current_state);
            
    //     }
    //  }
}

<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Couple;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Ramsey\Uuid\Uuid;

class CategoryController extends Controller
{
    public function Category(){
        $list = Category::all();
        return $this->success('Category List', ['list' => $list] , 200);
    }

    public function addCategory(Request $request){
        $user = auth()->user();

        Category::create([
            'nameEn' => $request->nameEn,
            'nameGr'=> $request->nameGr,
            'displayOnWebsite'=> $request->displayOnWebsite,
            'displayAsMenu'=> $request->displayAsMenu,
            'orderLevel'=>  $request->orderLevel,
            'created_id' => 1,
            'updated_id' => 1
        ]);
        if (request()->wantsJson()) {
            $data = [
                'message' => 'success'
            ];
            return $this->success('create-category', $data , 200);
        }
    }

    
}

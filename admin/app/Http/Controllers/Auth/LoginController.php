<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\UserInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

  use AuthenticatesUsers;

  /**
   * Where to redirect users after login.
   *
   * @var string
   */
  // protected $redirectTo = '/home';
  protected function redirectTo()
  {
    $user = User::where('id', auth()->user()->id)->first();
    $userInfo = UserInfo::where('user_id', auth()->user()->id)->first();
    // if (
    //   ($user->name && $user->email && $user->dob && $user->phone &&
    //     $userInfo->last_name && $userInfo->middle_name && $userInfo->native_place && $userInfo->birth_place  && $userInfo->blood_group &&
    //     $userInfo->gotra && $userInfo->mosad &&
    //     $userInfo->email && $userInfo->mobile_code && $userInfo->mobile  && $userInfo->alternate_mobile_code && $userInfo->alternate_mobile &&
    //     $userInfo->current_building && $userInfo->current_landmark  && $userInfo->current_road && $userInfo->current_pincode &&
    //     $userInfo->current_city && $userInfo->current_state  && $userInfo->current_country &&
    //     $userInfo->permanent_building && $userInfo->permanent_landmark  && $userInfo->permanent_road && $userInfo->permanent_pincode &&
    //     $userInfo->permanent_city && $userInfo->permanent_state  && $userInfo->permanent_country &&
    //     $userInfo->education && $userInfo->occupation &&
    //     $userInfo->charity_organization && $userInfo->charity_designation && $userInfo->serve && $userInfo->serve_help &&
    //     $userInfo->donate && $userInfo->donate_part)
    //   == false
    // ) {
    //   return '/users/' . auth()->user()->id . '/edit';
    // } else {
    //   return '/users/' . auth()->user()->id;
    // }
    return '/blogs';
  }

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  protected function credentials(Request $request)
  {
    $email = $request->get('email');
    $userInfo = UserInfo::where('mobile', $email)->first();
    if ($userInfo) {
        if ($userInfo->otp_status == 'pending') {
          $email = '12wqsaxasqwdsjwkehwkenwenjbjnsajnsjasnj@asjkasjkasaksa.askaskaskaks';
        }
    }

    if (is_numeric($email)) {
      return ['phone' => $email, 'password' => $request->get('password')];
    } elseif (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return ['email' => $email, 'password' => $request->get('password')];
    }
    return ['email' => $email, 'password' => $request->get('password')];
  }

  
}

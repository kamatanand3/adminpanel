<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UpdatePasswordRequest;
use Illuminate\Http\Request;
use App\User;
use App\Models\UserVerification;
use Illuminate\Support\Facades\Validator;

class ChangePasswordController extends Controller
{
    /**
     * Display change user password form.
     *
     * @return \Illuminate\View\View
     */
    public function show()
    {
        return view('users.change-password');
    }

    public function getOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required'
        ]);

        if($validator->fails()){
            return $this->failure($validator->errors()->toJson(), null, 400);
        }
        
        $userPhone = User::where('phone', $request->mobile)->first();
        if ($userPhone) {
            $otp = $this->generateNumericOTP(6);
            $current = \Carbon\Carbon::now();
            $current->addMinutes(env('OTP_EXPIRY_MINUTES',5));
            
            UserVerification::where('mobile', $request->mobile)->update(['is_active'=> 0]);
            
            $verify = new UserVerification();
            $verify->otp = $otp;
            $verify->mobile = $request->mobile;
            $verify->is_active = 1;
            $verify->expired_at = $current;
            $verify->save();

            $message = $otp." is your OTP to reset your password for DSVS Login.\n\nRegards,\nDSVS Team";
            $this->sendTextMsg($request->mobile, $message);
            
            return $this->success('OTP sent to your mobile number' , null, 200);
        }
        return $this->failure('Mobile Number Does Not Exists !!' , null, 500);
    }

    public function verifyOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required',
            'otp' => 'required'
        ]);

        if($validator->fails()){
            return $this->failure($validator->errors()->toJson(), null, 400);
        }

        
        $verification = UserVerification::where('mobile',$request->mobile)->where('is_active',1)->first();
        if($verification && \Carbon\Carbon::now()<$verification->expired_at && $verification->is_active==1){
            if($verification->otp == $request->otp){
                UserVerification::where('mobile', $request->mobile)->update(['is_active'=> 0]);
                
                $userPhone = User::where('phone', $request->mobile)->update(['password' => bcrypt($request->password)]);

                return $this->success('OTP is verified!', null, 200);
            } else {
                return $this->failure('OTP not matched!' , null, 500);
            }
        } else {
            return $this->failure('OTP expired or Wrong OTP' , null, 500);
        }
    }

    /**
     * Proccessing user password change.
     *
     * @param  \App\Http\Requests\Users\UpdatePasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePasswordRequest $request)
    {
        $user = \Auth::user();
        $user->password = bcrypt($request->new_password);
        $updateResponse = array('error' => __('auth.change_password_error')); 
        if ($user->save()) {
            $updateResponse = array('success' => __('auth.change_password_success'));
        }
        if (request()->wantsJson()) {
            return $this->success('Password changed' , $updateResponse, 200);
        }
        else {
        return redirect()->back()->with($updateResponse);
        }
    }

    public function update_og(UpdatePasswordRequest $request)
    {
        $user = \Auth::user();
        $user->password = bcrypt($request->new_password);
        $updateResponse = array('error' => __('auth.change_password_error'));

        if ($user->save()) {
            $updateResponse = array('success' => __('auth.change_password_success'));
        }

        return redirect()->back()->with($updateResponse);
    }
}

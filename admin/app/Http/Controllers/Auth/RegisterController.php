<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\UserInfo;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\UserVerification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected function redirectTo()
    {
        $ccc = UserInfo::where('user_id', auth()->user()->id)->where('otp_status','verified')->count();
        if ($ccc && $ccc > 0) {
            return '/users/'.auth()->user()->id.'/edit';
        } else {
            $url = '/register?otp=true&userID='.auth()->user()->id;
            $this->guard()->logout();
            return $url;
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        //dd($data);
        
        if ($data && empty($data['otp'])) {
            return Validator::make($data, [
                //'nickname' => 'required|string|max:255',
                'name' => 'required|string|max:255',
                'middle_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                //'email' => 'required|string|email|max:255|unique:users',
                //'mobile' => 'required|unique:user_info',
                'gender_id' => 'required|numeric|in:1,2',
                'password' => 'required|string|min:6|confirmed',
            ]);
        } else {
            return Validator::make($data, [
                'otp' => 'required|string|max:255'
            ]);
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    

    protected function create(array $data)
    {
        if ($data && empty($data['otp'])) {
    
            //dd($data);
            $uuid = Uuid::uuid4()->toString();
            $user = User::create([
                'id' => $uuid,
                'nickname' => $data['name'],
                'name' => $data['name'],
                'gender_id' => $data['gender_id'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'phone' => $data['mobile'],
            ]);
            $user->manager_id = $user->id;
            // $user->isAreaHead = $data["is_areahead"];
            $user->isFamilyHead = 1;
            $user->save();
    
            $user_info = UserInfo::create([
                'user_id' => $uuid,
                'last_name' => $data['last_name'],
                'middle_name' => $data['middle_name'],
                'email' => $data['email'],
                'mobile_code' => $data['mobile_code'],
                'mobile' => $data['mobile'],
                'otp_status' => 'pending',
                'current_city' => $data['current_city'],
                'current_state' => $data['current_state'],
                'current_country' => $data['current_country'],
                'permanent_city' => $data['current_city'],
                'permanent_state' => $data['current_state'],
                'permanent_country' => $data['current_country']
            ]);
            $user_info->save();

            
            $otp = $this->generateNumericOTP(6);
            $current = \Carbon\Carbon::now();
            $current->addMinutes(env('OTP_EXPIRY_MINUTES',5));
            
            UserVerification::where('mobile', $data['mobile'])->update(['is_active'=> 0]);

            $verify = new UserVerification();
            $verify->otp = $otp;
            $verify->mobile = $data['mobile'];
            $verify->is_active = 1;
            $verify->expired_at = $current;
            $verify->save();

            $message = $otp." is your OTP to reset your password for DSVS Login.\n\nRegards,\nDSVS Team";
            $this->sendTextMsg($data['mobile'], $message);
            return $user;
        } else {
            UserInfo::where('user_id', $data['userID'])->update(['otp_status' => 'verified']);
            $user = User::where('id', $data['userID'])->first();

            $verification = UserVerification::where('mobile',$user->phone)->where('is_active',1)->first();
            if($verification && \Carbon\Carbon::now()<$verification->expired_at && $verification->is_active==1){
                if($verification->otp ==  $data['otp']){
                    UserVerification::where('mobile', $user->phone)->update(['is_active'=> 0]);
                    return $user;
                } else {
                    throw \Illuminate\Validation\ValidationException::withMessages([
                        'otp' => ['OTP not match'],
                    ]);
                }
            } else {
                throw \Illuminate\Validation\ValidationException::withMessages([
                    'otp' => ['OTP expired or Wrong OTP'],
                ]);
            }

        }
    }
}

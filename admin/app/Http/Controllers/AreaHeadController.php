<?php

namespace App\Http\Controllers;

use App\Couple;
use App\Setting;
use Illuminate\Http\Request;
use App\User;
use App\UserInfo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Validator;

class AreaHeadController extends Controller
{
    public function index()
    {
        //$id = Auth::user()->id;
        $query = User::join('user_info', 'user_info.user_id', '=', 'users.id')
            ->select('users.name', 'user_info.mobile', 'user_info.current_state', 'users.city', 'users.id',)
            ->where('users.manager_id', Auth::user()->id)
            ->where('users.isFamilyHead', 1);

        $list = $this->search($query)->orderBy('users.id', 'DESC')->paginate(10);
        if (request()->wantsJson()) {
            return response()->json(['data' => $list]);
        } else {
            // dd($list);
            return view('AreaHead.list')->with(['data' => $list]);
        }
    }

    public function create($tab)
    {
        //dd($head_id);
        //$validTabs = ['null', 'spouse_form', 'children_form', 'other_form'];
        $validTabs = $tab;
        $added_child = null;
        $head_id = null;
        if (request()->wantsJson()) {
            return response()->json(['validTabs' => $validTabs, 'added_child' => $added_child, 'head_id' => $head_id]);
        } else {
            return view('AreaHead.form', compact('validTabs', 'added_child', 'head_id'));
        }
    }


    public function store(Request $request)
    {
            
        if (!empty($request->email)) {
            $email = User::where('email',$request->email)->first();
            if($email != null){
                return $this->failure('Email Alreay Exists !!', null, 500);
            }
        }

        if (!empty($request->mobile)) {
            $mobile = UserInfo::where('mobile',$request->mobile)->orWhere('alternate_mobile', $request->mobile)->first();
            if($mobile != null){
                return $this->failure('Whatsapp Mobile Alreay Exists !!', null, 500);
            }
        }

        //dd($request->all());
        if ($request->first_name != null) {
            if ($request->current_country != null && $request->current_state && $request->current_city != null) {
                $country_code = DB::table('countries')->where('name', $request->current_country)->first();
                $state_code = DB::table('states')->where('name', $request->current_state)->first();
                $city_code = DB::table('cities')->where('name', $request->current_city)->first();
                $country_code = sprintf("%03d", $country_code->id);
                $state_code = sprintf("%04d", $state_code->id);
                $city_code = sprintf("%05d", $city_code->id);
                $unique_code = DB::table('settings')->where('key', 'family code')->first();
                DB::table('settings')->increment('value', 1);
                $unique_code = sprintf("%05d", $unique_code->value);
                $family_code = $country_code . $state_code . $city_code . $unique_code;
            } else {
                $family_code = null;
            }

            $head_uuid = Uuid::uuid4()->toString();
            //dd($uuid);
            //dd($family_code);
            $user = User::create([
                'id' => $head_uuid,
                'nickname' => $request->first_name,
                'name' => $request->first_name,
                'gender_id' => $request->gender,
                'email' => $request->email,
                'family_code' => $family_code,
                'city' => $request->current_city,
                'dob' => $request->dob,
                "phone" => $request->mobile

            ]);
            $user->manager_id = Auth::user()->id;
            $user->isFamilyHead = 1;
            //dd($user);
            $user->save();
            //dd("done");
            $user_info = UserInfo::create([
                'user_id' => $head_uuid,
                'last_name' => $request->last_name,
                'middle_name' => $request->middle_name,
                'last_name_original' => $request->last_name_original,
                'native_place' => $request->native_place,
                'blood_group' => $request->bloodgroup,
                'email' => $request->email,
                'mobile_code' => $request->mobile_code,
                'mobile' => $request->mobile,
                'current_building' => $request->current_building,
                'current_landmark' => $request->current_landmark,
                'current_road' => $request->current_road,
                'current_pincode' => $request->current_pincode,
                'current_state' => $request->current_state,
                'current_country' => $request->current_country,
                'permanent_building' => $request->permanent_building,
                'permanent_landmark' => $request->permanent_landmark,
                'permanent_road' => $request->permanent_road,
                // 'permanent_city' => $request->permanent_city ,
                'permanent_pincode' => $request->permanent_pincode,
                'permanent_state' => $request->permanent_state,
                'permanent_country' => $request->permanent_country,
                'education' => $request->education,
                'college' => $request->college,
                'passout_year' => $request->year,
                'university' => $request->university,
                'occupation' => $request->occupation,
                'category' => $request->category,
                'company_name' => $request->company_name,
                'contact_person_name' => $request->contact_person_name,
                'contact_person_mobile' => $request->contact_person_mobile,
                'contact_person_address' => $request->contact_person_address,
            ]);

            $user_info->save();
            if (request()->wantsJson()) {
                return response()->json(['user' => $head_uuid]);
            } else {
                return redirect()->route('users.show', ['user' => $head_uuid]);
            }
        }
        if ($request->spouse_first_name != null) {
            
            $head_info = UserInfo::where('user_id',$request->head_id)->first();
            
            if ($request->spouse_visibility == "on") {
                $spouse_visibility = 1;
            }
            else {
                $spouse_visibility = 0;
            }
            if ($request->spouse_alternate_visibility == "on") {
                $spouse_alternate_visibility = 1;
            }
            else {
                $spouse_alternate_visibility = 0;
            }
            $spouse_current_city_id = $request->spouse_current_city;
            $spouse_current_state_id = $request->spouse_current_state;
            $spouse_current_country_id = $request->spouse_current_country;

            $head_current_city_id = $head_info->current_city;
            $head_current_state_id = $head_info->current_state;
            $head_current_country_id = $head_info->current_country;


            $spouse_occupation_city_id = $request->spouse_occupation_city;
            $spouse_occupation_state_id = $request->spouse_occupation_state;
            $spouse_occupation_country_id =  $request->spouse_occupation_country;

            if($request->spouse_current_address_check == "on") {
                $spouse_current_building  = $head_info->current_building ;
                $spouse_current_landmark = $head_info->current_landmark ;
                $spouse_current_road = $head_info->current_road ;
                $spouse_current_vibhag = $head_info->current_vibhag ;
                $spouse_current_pincode = $head_info->current_pincode ;
                $spouse_current_city = $head_current_city_id ;
                $spouse_current_state = $head_current_state_id ;
                $spouse_current_country = $head_current_country_id ;
            }
            else {
                $spouse_current_building  = $request->spouse_current_building ;
                $spouse_current_landmark = $request->spouse_current_landmark ;
                $spouse_current_road = $request->spouse_current_road ;
                $spouse_current_vibhag = $head_info->spouse_current_vibhag ;
                $spouse_current_pincode = $request->spouse_current_pincode ;
                $spouse_current_city = $spouse_current_city_id ;
                $spouse_current_state = $spouse_current_state_id ;
                $spouse_current_country = $spouse_current_country_id ;
            }
            $spouse_uuid = Uuid::uuid4()->toString();
            $user = User::create([
                'id' => $spouse_uuid,
                'nickname' => $request->spouse_first_name,
                'name' => $request->spouse_first_name,
                'gender_id' => $request->spouse_gender,
                'email' => $request->email,
                'dob' => $request->spouse_dob,
                "phone" => $request->mobile,
                'city' => $spouse_current_city,
                'user_type' => 1,

            ]);
            $user->manager_id = Auth::user()->id;
            // if ($request->head_id != null) {          //new
            //     $head = User::where('id', $request->head_id)->first();
            //     $user->family_code = $head->family_code;
            // }
            $user->save();
            // dd("test");
            if ($request->head_id != null) {
                //dd("in");
                $head = User::where('id', $request->head_id)->first();
                if ($head->gender_id == "1") {
                    $husband_id = $request->head_id;
                    $wife_id = $spouse_uuid;
                } else {
                    $husband_id = $spouse_uuid;
                    $wife_id = $request->head_id;
                }
                $couple_id = Uuid::uuid4()->toString();
                $couple = Couple::create([
                    'id' => $couple_id,
                    'manager_id' => $request->head_id,
                    'city' => $request->spouse_current_city,
                    'husband_id' => $husband_id,
                    'wife_id' => $wife_id,
                ]);

                //dd($couple);
                $couple->save();
                
                UserInfo::where('user_id', $request->head_id)->update(['maritial_status' => 1]);

                if($head->gender_id == "1") {
                    $childs = User::where('father_id', $head->id)->get();
                    if($childs != null){
                        foreach($childs as $child) {
                            $child->mother_id = $spouse_uuid;
                            $child->parent_id = $couple_id;
                            $child->save();
                    }
                    }
                }
                else{
                    $childs = User::where('mother_id', $head->id)->get();
                    if($childs != null) {
                        foreach($childs as $child) {
                            $child->father_id = $spouse_uuid;
                            $child->parent_id = $couple_id;
                            $child->save();
                    }
                    }
                }
                
                //dd($childs);
            }

           // dd("out");


            $user_info = UserInfo::create([
                'user_id' => $spouse_uuid,
                'last_name' => $request->spouse_last_name,
                'middle_name' => $request->spouse_middle_name,
                'blood_group' => $request->spouse_bloodgroup,
                'email' => $request->email,
                'mobile_code' => $request->spouse_mobile_code,
                'mobile' => $request->mobile,
                'visibility' => $spouse_visibility,
                'current_building' => $spouse_current_building,
                'current_landmark' => $spouse_current_landmark,
                'current_road' => $spouse_current_road,
                'current_vibhag' => $spouse_current_vibhag,
                'current_pincode' => $spouse_current_pincode,
                'current_city'=> $spouse_current_city,
                'current_state' => $spouse_current_state,
                'current_country' => $spouse_current_country,

                'permanent_building' => $spouse_current_building,
                'permanent_landmark' => $spouse_current_landmark,
                'permanent_road' => $spouse_current_road,
                'permanent_vibhag' => $spouse_current_vibhag,
                'permanent_pincode' => $spouse_current_pincode,
                'permanent_city'=> $spouse_current_city,
                'permanent_state' => $spouse_current_state,
                'permanent_country' => $spouse_current_country,

            "alternate_mobile_code" => $request->spouse_alternate_mobile_code,
            "alternate_mobile" => $request->alternate_mobile,
            'alternate_mobile_visibility' => $spouse_alternate_visibility,
            "birth_place" => $request->spouse_birth_place,
            "gotra" => $request->spouse_gotra,
            "divyang" => $request->spouse_divyang,
            "mosad" => $request->spouse_mosad,
            'occupation_building' => $request->spouse_occupation_building,
            'occupation_landmark' => $request->spouse_occupation_landmark,
            'occupation_road' => $request->spouse_occupation_road,
            'occupation_pincode' => $request->spouse_occupation_pincode,
            'occupation_vibhag' => $request->spouse_occupation_vibhag,
            'occupation_city' => $spouse_occupation_city_id,
            'occupation_state' => $spouse_occupation_state_id,
            'occupation_country' => $spouse_occupation_country_id,

                'education' => $request->spouse_education,
                'college' => $request->spouse_college,
                'passout_year' => $request->spouse_year,
                'university' => $request->spouse_university,
                'occupation' => $request->spouse_occupation,
                'category' => $request->spouse_category,
                'company_name' => $request->spouse_company_name,
                'contact_person_name' => $request->spouse_contact_person_name,
                // 'contact_person_mobile' => $request->spouse_contact_person_mobile,
                // 'contact_person_address' => $request->spouse_contact_person_address,
                'maritial_status' => 1, 
            ]);

            $user_info->save();
            if (request()->wantsJson()) {
                $data = [
                    'head_id' => $request->head_id,
                    'added_spouse_id' => $spouse_uuid,
                ];
                return $this->success('Spouse Added', $data, 200);
                // return response()->json(['user' => $request->head_id]);
            } else {
                return redirect()->route('users.show', ['user' => $request->head_id]);
            }
        }
        if ($request->children_first_name != null) {
            // dd($request->all());
            // dd("wait");
            $head_info = UserInfo::where('user_id',$request->head_id)->first();

            if ($request->children_visibility == "on") {
                $children_visibility = 1;
            }
            else {
                $children_visibility = 0;
            }
            if ($request->children_alternate_visibility == "on") {
                $children_alternate_visibility = 1;
            }
            else {
                $children_alternate_visibility = 0;
            }
            $children_current_city_id = $request->children_current_city;
            $children_current_state_id = $request->children_current_state;
            $children_current_country_id = $request->children_current_country;

            $head_current_city_id = $head_info->current_city;
            $head_current_state_id = $head_info->current_state;
            $head_current_country_id = $head_info->current_country;


            $children_occupation_city_id = $request->children_occupation_city;
            $children_occupation_state_id = $request->children_occupation_state;
            $children_occupation_country_id =  $request->children_occupation_country;
        


            if($request->children_current_address_check == "on") {
                $children_current_building  = $head_info->current_building ;
                $children_current_landmark = $head_info->current_landmark ;
                $children_current_road = $head_info->current_road ;
                $children_current_pincode = $head_info->current_pincode ;
                $children_current_vibhag = $head_info->current_vibhag ;
                $children_current_city = $head_current_city_id ;
                $children_current_state = $head_current_state_id ;
                $children_current_country = $head_current_country_id ;
            }
            else {
                $children_current_building  = $request->children_current_building ;
                $children_current_landmark = $request->children_current_landmark ;
                $children_current_road = $request->children_current_road ;
                $children_current_pincode = $request->children_current_pincode ;
                $children_current_vibhag = $request->children_current_vibhag ;
                $children_current_city = $children_current_city_id ;
                $children_current_state = $children_current_state_id ;
                $children_current_country = $children_current_country_id ;
            }
            $children_uuid = Uuid::uuid4()->toString();
            $head = User::where('id', $request->head_id)->first();
            
            if($head->gender_id == 1){
                $couple = Couple::where('husband_id', $head->id)->first();
                if($couple == null){
                    $mother_id = null;
                    $parent_id = null;
                    $father_id = $head->id;
                }
                else {
                    $mother_id = $couple->wife_id;
                    $father_id = $couple->husband_id;
                    if(($couple->wife_id != null) && ($couple->husband_id != null)){
                        $parent_id = $couple->id;
                    }
                }
            }
            else {
                //dd("in");
                $couple = Couple::where('wife_id', $head->id)->first();
                if($couple == null){
                    $father_id = null;
                    $parent_id = null;
                    $mother_id = $head->id;
                }
                else {
                    $mother_id = $couple->wife_id;
                    $father_id = $couple->husband_id;
                    if(($couple->wife_id != null) && ($couple->husband_id != null)){
                        $parent_id = $couple->id;
                    }
                }
            }
          //dd($request->head_id, $mother_id, $father_id, $parent_id);
            
            
            //dd($parent_id, $couple);
            $user = User::create([
                'id' => $children_uuid,
                'nickname' => $request->children_first_name,
                'name' => $request->children_first_name,
                'gender_id' => $request->children_gender,
                'email' => $request->email,
                'father_id' => $father_id,
                'mother_id' => $mother_id,
                'parent_id' => $parent_id,
                'city' => $children_current_city,
                'family_code' => $head->family_code,
                'dob' => $request->children_dob,
                "phone" => $request->mobile,
                'user_type' => 2

            ]);
            $user->manager_id = Auth::user()->id;
            //dd($user);
            $user->save();

            $user_info = UserInfo::create([
                'user_id' => $children_uuid,
                'last_name' => $request->children_last_name,
                'middle_name' => $request->children_middle_name,
                'blood_group' => $request->children_bloodgroup,
                'email' => $request->email,
                'mobile_code' => $request->children_mobile_code,
                'mobile' => $request->mobile,
                'visibility' => $children_visibility,
                'current_building' => $children_current_building,
                'current_landmark' => $children_current_landmark,
                'current_road' => $children_current_road,
                'current_pincode' => $children_current_pincode,
                'current_vibhag' => $children_current_vibhag,
                'current_city'=> $children_current_city,
                'current_state' => $children_current_state,
                'current_country' => $children_current_country,

                'permanent_building' => $children_current_building,
                'permanent_landmark' => $children_current_landmark,
                'permanent_road' => $children_current_road,
                'permanent_pincode' => $children_current_pincode,
                'permanent_vibhag' => $children_current_vibhag,
                'permanent_city'=> $children_current_city,
                'permanent_state' => $children_current_state,
                'permanent_country' => $children_current_country,

                "alternate_mobile_code" => $request->children_alternate_mobile_code,
                "alternate_mobile" => $request->alternate_mobile,
                'alternate_mobile_visibility' => $children_alternate_visibility,
                "birth_place" => $request->children_birth_place,
                "gotra" => $request->children_gotra,
                "divyang" => $request->children_divyang,
                "mosad" => $request->children_mosad,
                'occupation_building' => $request->children_occupation_building,
                'occupation_landmark' => $request->children_occupation_landmark,
                'occupation_road' => $request->children_occupation_road,
                'occupation_pincode' => $request->children_occupation_pincode,
                'occupation_vibhag' => $request->children_occupation_vibhag,
                'occupation_city' => $children_occupation_city_id,
                'occupation_state' => $children_occupation_state_id,
                'occupation_country' => $children_occupation_country_id,
                
                'education' => $request->children_education,
                'college' => $request->children_college,
                'passout_year' => $request->children_year,
                'university' => $request->children_university,
                'occupation' => $request->children_occupation,
                'category' => $request->children_category,
                'company_name' => $request->children_company_name,
                'contact_person_name' => $request->children_contact_person_name,
                'marital_status' => $request->children_marital_status,
                // 'contact_person_mobile' => $request->children_contact_person_mobile,
                // 'contact_person_address' => $request->children_contact_person_address,
            ]);

            $user_info->save();
            if (request()->wantsJson()) {
                // return response()->json(['user' => $request->head_id]);
                $data = [
                    'added_child_id'=> $children_uuid
                ];
                return $this->success('Child Added', $data , 200);
            } else {
                return redirect()->route('users.show', ['user' => $request->head_id]);
            }
        }
    }

    public function store_og(Request $request)
    {
        //dd($request->all());

        if ($request->first_name != null) {
            if ($request->current_country != null && $request->current_state && $request->current_city != null) {
                $country_code = DB::table('countries')->where('name', $request->current_country)->first();
                $state_code = DB::table('states')->where('name', $request->current_state)->first();
                $city_code = DB::table('cities')->where('name', $request->current_city)->first();
                $country_code = sprintf("%03d", $country_code->id);
                $state_code = sprintf("%04d", $state_code->id);
                $city_code = sprintf("%05d", $city_code->id);
                $unique_code = DB::table('settings')->where('key', 'family code')->first();
                DB::table('settings')->increment('value', 1);
                $unique_code = sprintf("%05d", $unique_code->value);
                $family_code = $country_code . $state_code . $city_code . $unique_code;
            } else {
                $family_code = null;
            }

            $head_uuid = Uuid::uuid4()->toString();
            //dd($uuid);
            //dd($family_code);
            $user = User::create([
                'id' => $head_uuid,
                'nickname' => $request->first_name,
                'name' => $request->first_name,
                'gender_id' => $request->gender,
                'email' => $request->email,
                'family_code' => $family_code,
                'city' => $request->current_city,

            ]);
            $user->manager_id = Auth::user()->id;
            $user->isFamilyHead = 1;
            //$user->save();
            $user_info = UserInfo::create([
                'user_id' => $head_uuid,
                'last_name' => $request->last_name,
                'middle_name' => $request->middle_name,
                'last_name_original' => $request->last_name_original,
                'native_place' => $request->native_place,
                'blood_group' => $request->bloodgroup,
                'email' => $request->email,
                'mobile_code' => $request->mobile_code,
                'mobile' => $request->mobile,
                'current_building' => $request->current_building,
                'current_landmark' => $request->current_landmark,
                'current_road' => $request->current_road,
                'current_pincode' => $request->current_pincode,
                'current_state' => $request->current_state,
                'current_country' => $request->current_country,
                'permanent_building' => $request->permanent_building,
                'permanent_landmark' => $request->permanent_landmark,
                'permanent_road' => $request->permanent_road,
                // 'permanent_city' => $request->permanent_city ,
                'permanent_pincode' => $request->permanent_pincode,
                'permanent_state' => $request->permanent_state,
                'permanent_country' => $request->permanent_country,
                'education' => $request->education,
                'college' => $request->college,
                'passout_year' => $request->year,
                'university' => $request->university,
                'occupation' => $request->occupation,
                'category' => $request->category,
                'company_name' => $request->company_name,
                'contact_person_name' => $request->contact_person_name,
                'contact_person_mobile' => $request->contact_person_mobile,
                'contact_person_address' => $request->contact_person_address,
            ]);

            // $user_info->save();
        }



        $latest_family_head_id = User::select('id')
            ->where('manager_id', Auth::user()->id)
            ->where('isFamilyHead', 1)
            ->latest()
            ->first();

        if ($request->spouse_first_name != null) {
            //dd($request->all());
            $spouse_uuid = Uuid::uuid4()->toString();
            $user = User::create([
                'id' => $spouse_uuid,
                'nickname' => $request->spouse_first_name,
                'name' => $request->spouse_first_name,
                'gender_id' => $request->spouse_gender,
                'email' => $request->spouse_email,
                //'family_code' => $head->family_code
            ]);
            $user->manager_id = Auth::user()->id;
            if ($request->head != null) {          //new
                $head = User::where('id', $request->head_id)->first();
                $user->family_code = $head->family_code;
            }
            // $user->save();
            if ($request->head != null) {
                $couple = Couple::create([
                    'id' => Uuid::uuid4()->toString(),
                    'husband_id' => $request->head_id,
                    'wife_id' => $spouse_uuid,
                    'manager_id' => $request->head_id,
                    'city' => $request->spouse_current_city,
                ]);

                //$couple->save();
            }


            $user_info = UserInfo::create([
                'user_id' => $spouse_uuid,
                'last_name' => $request->spouse_last_name,
                'middle_name' => $request->spouse_middle_name,
                'blood_group' => $request->spouse_bloodgroup,
                'email' => $request->spouse_email,
                'mobile_code' => $request->spouse_mobile_code,
                'mobile' => $request->spouse_mobile,
                'current_building' => $request->spouse_current_building,
                'current_landmark' => $request->spouse_current_landmark,
                'current_road' => $request->spouse_current_road,
                'current_vibhag' => $request->spouse_current_vibhag,
                'current_pincode' => $request->spouse_current_pincode,
                'current_state' => $request->spouse_current_state,
                'current_country' => $request->spouse_current_country,
                // 'permanent_building' => $request->permanent_building ,
                // 'permanent_landmark' => $request->permanent_landmark ,
                // 'permanent_road' => $request->permanent_road ,
                // 'permanent_city' => $request->permanent_city ,
                // 'permanent_pincode' => $request->permanent_pincode ,
                // 'permanent_state' => $request->permanent_state ,
                // 'permanent_country' => $request->permanent_country,
                'education' => $request->spouse_education,
                'college' => $request->spouse_college,
                'passout_year' => $request->spouse_year,
                'university' => $request->spouse_university,
                'occupation' => $request->spouse_occupation,
                'category' => $request->spouse_category,
                'company_name' => $request->spouse_company_name,
                'contact_person_name' => $request->spouse_contact_person_name,
                'contact_person_mobile' => $request->spouse_contact_person_mobile,
                'contact_person_address' => $request->spouse_contact_person_address,
            ]);

            //$user_info->save();
        }

        if ($request->children_first_name != null) {
            $children_uuid = Uuid::uuid4()->toString();
            $head = User::where('id', $latest_family_head_id->id)->first();
            $mother_id = Couple::where('husband_id', $latest_family_head_id->id)->first();
            $user = User::create([
                'id' => $children_uuid,
                'nickname' => $request->children_first_name,
                'name' => $request->children_first_name,
                'gender_id' => $request->children_gender,
                'email' => $request->children_email,
                'father_id' => $latest_family_head_id->id,
                'mother_id' => $mother_id->wife_id,
                'city' => $request->children_current_city,
                'family_code' => $head->family_code
            ]);
            $user->manager_id = Auth::user()->id;
            //$user->save();

            $user_info = UserInfo::create([
                'user_id' => $children_uuid,
                'last_name' => $request->children_last_name,
                'middle_name' => $request->children_middle_name,
                'blood_group' => $request->children_bloodgroup,
                'email' => $request->children_email,
                'mobile_code' => $request->children_mobile_code,
                'mobile' => $request->children_mobile,
                'current_building' => $request->children_current_building,
                'current_landmark' => $request->children_current_landmark,
                'current_road' => $request->children_current_road,
                'current_pincode' => $request->children_current_pincode,
                'current_state' => $request->children_current_state,
                'current_country' => $request->children_current_country,
                // 'permanent_building' => $request->permanent_building ,
                // 'permanent_landmark' => $request->permanent_landmark ,
                // 'permanent_road' => $request->permanent_road ,
                // 'permanent_city' => $request->permanent_city ,
                // 'permanent_pincode' => $request->permanent_pincode ,
                // 'permanent_state' => $request->permanent_state ,
                // 'permanent_country' => $request->permanent_country,
                'education' => $request->children_education,
                'college' => $request->children_college,
                'passout_year' => $request->children_year,
                'university' => $request->children_university,
                'occupation' => $request->children_occupation,
                'category' => $request->children_category,
                'company_name' => $request->children_company_name,
                'contact_person_name' => $request->children_contact_person_name,
                'contact_person_mobile' => $request->children_contact_person_mobile,
                'contact_person_address' => $request->children_contact_person_address,
                'marital_status' => $request->children_marital_status,
            ]);

            // $user_info->save();
        }

        if ($request->add_child) {
            if ($request->add_child == "yes") {
                if ($request->children_first_name == null) {
                    return redirect()->route('family.create', ['tab' => 'children_form']);
                } else {
                    return redirect()->route('family.createAddChild', ['tab' => 'children_form', 'latest_family_head_id' => $latest_family_head_id->id]);
                }
            } else {
                return redirect()->route('family.create', ['tab' => $request->tab]);
            }
        }
        if ($request->tab == "spouse_form") {
            if ($request->first_name != null) {
                return redirect()->route('family.createSpouse', ['tab' => $request->tab, 'head_id' => $head_uuid]);
            } else {
                return redirect()->route('family.create', ['tab' => $request->tab]);
            }
        }
        // else if ($request->tab == "children_form") {
        //     //dd("in");
        //     return redirect()->route('family.create', ['tab' => 'children_form']);
        // }
        else if ($request->tab == "done") {
            return redirect('family');
        } else {
            return redirect()->route('family.create', ['tab' => $request->tab]);
        }
    }

    public function createAddChild_og($tab, $latest_family_head_id)
    {
        $validTabs = $tab;
        $added_child = User::join('user_info', 'user_info.user_id', '=', 'users.id')
            ->select('users.name', 'users.email', 'user_info.mobile')
            ->where('users.father_id', $latest_family_head_id)
            ->get();
        //dd($added_child);
        $head_id = null;
        if (request()->wantsJson()) {
            return response()->json(['validTabs' => $validTabs, 'added_child' => $added_child, 'head_id' => $head_id]);
        } else {
            return view('AreaHead.form', compact('validTabs', 'added_child', 'head_id'));
        }
    }

    public function createChild($tab, $head_id)
    {
        $categories = DB::table('categories')->orderBy('name')->get();
        $countries = DB::table('countries')->orderBy('name')->get();
        $vibhags = DB::table('vibhags')->orderBy('name')->get();
        $validTabs = $tab;
        $head_id = $head_id;
        
        $userInfo = UserInfo::where('user_id', $head_id)->first();
        $states = [];
        if($userInfo->current_country != null){
            $states = DB::table('states')->where('countryID', $userInfo->current_country)->orderBy('name')->get();
        } else {
            $states = DB::table('states')->where('countryID', 101)->orderBy('name')->get();
        }
        $cities = [];
        if($userInfo->current_state != null){
            $cities = DB::table('cities')->where('state_id', $userInfo->current_state)->orderBy('name')->get();
        }

        $occ_states = DB::table('states')->where('countryID', 101)->orderBy('name')->get();

        if (request()->wantsJson()) {
            return response()->json(['validTabs' => $validTabs, 'head_id' => $head_id, 'vibhags' => $vibhags]);
        } else {
            return view('AreaHead.form', compact('validTabs', 'head_id', 'categories', 'vibhags', 'countries', 'states', 'cities','occ_states'));
        }
    }

    public function createSpouse($tab, $head_id)
    {
        $categories = DB::table('categories')->orderBy('name')->get();
        $countries = DB::table('countries')->orderBy('name')->get();
        $vibhags = DB::table('vibhags')->orderBy('name')->get();
        $validTabs = $tab;
        $head_id = $head_id;
        
        $userInfo = UserInfo::where('user_id', $head_id)->first();
        $states = [];
        if($userInfo->current_country != null){
            $states = DB::table('states')->where('countryID', $userInfo->current_country)->orderBy('name')->get();
        } else {
            $states = DB::table('states')->where('countryID', 101)->orderBy('name')->get();
        }
        $cities = [];
        if($userInfo->current_state != null){
            $cities = DB::table('cities')->where('state_id', $userInfo->current_state)->orderBy('name')->get();
        }

        $occ_states = DB::table('states')->where('countryID', 101)->orderBy('name')->get();
        //$added_child = null;
        if (request()->wantsJson()) {
            return response()->json(['validTabs' => $validTabs, 'head_id' => $head_id, 'vibhags' => $vibhags]);
        } else {
            return view('AreaHead.form', compact('validTabs', 'head_id', 'categories', 'vibhags', 'countries', 'states', 'cities','occ_states'));
        }
    }

    public function getLastName(Request $request)
    {
        $lname = $request->lastname;
        $last_names = DB::table('last_names')
            ->select('name')
            // ->where('name', 'LIKE', '%' . $request->lastName . '%')
            ->where('name', 'LIKE', '%' . $lname . '%')
            ->get();
        return response()->json(['success' => true, 'last_names' => $last_names]);
    }

    public function getCity(Request $request, $id)
    {
        $requested_city = $request->city;
        $city = DB::table('cities')
            ->where('state_id', $id)
            ->get();
        return response()->json(['success' => true, 'city' => $city]);
    }

    public function getState(Request $request, $id)
    {
        $requested_state = $request->state;
        $state = DB::table('states')
            ->where('countryID', $id)
            ->get();
        return response()->json(['success' => true, 'state' => $state]);
    }

    public function getCountry(Request $request)
    {
        $country = DB::table('countries')
            ->get();
        return response()->json(['success' => true, 'country' => $country]);
    }

    public function getCityByName(Request $request, $name)
    {
        $stateId = $request->stateId;
        $city = DB::table('cities')
            ->where('name', 'like', '%' . $name . '%')
            ->where('state_id', $stateId)
            ->get();
        return $this->success('Cities', $city , 200);
    }

    public function getStateByName(Request $request, $name)
    {
        $countryId = $request->countryId;
        $state = DB::table('states')
            ->where('name', 'like', '%' . $name . '%')
            ->where('countryID', $countryId)
            ->get();
        return $this->success('States', $state , 200);
    }

    public function getCountryByName(Request $request, $name)
    {
        $country = DB::table('countries')
            ->where('name', 'like', '%' . $name . '%')
            ->get();
        return $this->success('Countries', $country , 200);
    }

    public function getStateId($name){
        $state = DB::table('states')->where('name',$name)->first();
        if($state != null){
            $id = $state->id;
        }
        else {
            $id = null;
        } 
        return $id;
    }

    public function getCityId($name){
        if($name == null){
            return null;
        }
        else {
            $city_exist = DB::table('cities')
                            ->where(DB::raw('lower(name)'), 'like', '%' . strtolower($name) . '%')
                            ->whereRaw('LENGTH(name) = ?',[strlen($name)])
                            ->first();
            if($city_exist == null){
                        DB::table('cities')->insert(['name' => $name]);
                        $city = DB::table('cities')->where('name',$name)->first();
                        if($city == null) {
                            return null;
                        }
                        else{
                            return $city->id;
                        }
            }
            else{
                return $city_exist->id;
            }
        }
        // $city = DB::table('cities')->where('name',$name)->first();
        // if($city != null){
        //     $id = $city->id;
        // }
        // else {
        //     $id = null;
        // } 
        // return $id;
    }
    public function getCountryId($name){
        $country = DB::table('countries')->where('name',$name)->first();
        if($country != null){
            $id = $country->id;
        }
        else {
            $id = null;
        } 
        return $id;
    }


    public function search($query)
    {
        $search = \Request::get('search');
        if (!empty($search)) {

            foreach ($search as $key => $searchvalue) {
                if ($searchvalue !== '') {

                    if (method_exists($this, $key) && $searchvalue != '') {

                        $query = $this->{$key}($query, $searchvalue);
                    }
                }
            }
        }
        return $query;
    }

    public function update(Request $request, User $user) {
        
        $userInfo = UserInfo::where('user_id',$user->id)->first();
        
        $validator = Validator::make($request->all(), [
            'spouse_first_name' => 'required',
            'spouse_middle_name' => 'required',
            'spouse_last_name' => 'required',
            'spouse_dob' => 'required',
            'spouse_gender' => 'required',
        ]);

        if($validator->fails()){
            return $this->failure($validator->errors()->toJson(), null, 400);
        }
        
        if(!empty($request->email) && $request->email != $user->email ){
            $email = User::where('email',$request->email)->first();
            if($email != null){
                return $this->failure('Email Alreay Exists !!', null, 500);
            }
        }
        
        if(!empty($request->mobile) && $request->mobile != $userInfo->mobile ){
            $mobile = UserInfo::where('mobile',$request->mobile)->orWhere('alternate_mobile', $request->mobile)->first();
            if($mobile != null){
                return $this->failure('Mobile Alreay Exists !!', null, 500);
            }
        }

        if ($request->spouse_visibility == "on") {
            $spouse_visibility = 1;
        }
        else {
            // $spouse_visibility = 0; // nitin temp
            $spouse_visibility = 1;
        }
        if ($request->spouse_alternate_visibility == "on") {
            $spouse_alternate_visibility = 1;
        }
        else {
            // $alternate_visibility = 0;// nitin temp
            $spouse_alternate_visibility = 1;
        }

        //dd($request->all());
        
        //dd($city_id);

        $current_city_id = $this->getCityId($request->spouse_current_city);
        //dd($current_city_id);
        $current_state_id = $this->getStateId($request->spouse_current_state);
        $current_country_id = $this->getCountryId($request->spouse_current_country);

        $permanent_city_id = $this->getCityId($request->spouse_permanent_city);
        $permanent_state_id = $this->getStateId($request->spouse_permanent_state);
        $permanent_country_id =  $this->getCountryId($request->spouse_permanent_country);

        $occupation_city_id = $this->getCityId($request->spouse_occupation_city);
        $occupation_state_id = $this->getStateId($request->spouse_occupation_state);
        $occupation_country_id =  $this->getCountryId($request->spouse_occupation_country);
        //dd($current_city_id, $current_country_id, $current_state_id, $permanent_city_id, $permanent_state_id, $permanent_country_id);

        $permanent_building  = $request->spouse_permanent_building ;
        $permanent_landmark = $request->spouse_permanent_landmark ;
        $permanent_road = $request->spouse_permanent_road ;
        $permanent_pincode = $request->spouse_permanent_pincode ;
        $permanent_city = $permanent_city_id ;
        $permanent_state = $permanent_state_id ;
        $permanent_country = $permanent_country_id ;
        //dd($request->all());
        
        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $user->update([
            "name"=> $request->spouse_first_name,
            "gender_id" => $request->spouse_gender,
            "dob"=>$request->spouse_dob ,
            "email"=> $request->email ,
            "city"=> $request->spouse_current_city,
            "phone" => $request->mobile
        ]);

        $userInfo->update([
            "last_name" => $request->spouse_last_name,
            "middle_name" => $request->spouse_middle_name,
            "last_name_original" => $request->spouse_last_name_original,
            "native_place" => $request->spouse_native_place,
            "blood_group" =>$request->spouse_bloodgroup, 
            "mobile_code" => $request->spouse_mobile_code,
            "mobile" => $request->mobile,
            'visibility' => $spouse_visibility,

            "alternate_mobile_code" => $request->spouse_alternate_mobile_code,
            "alternate_mobile" => $request->alternate_mobile,
            'alternate_mobile_visibility' => $spouse_alternate_visibility,
            "birth_place" => $request->spouse_birth_place,
            "gotra" => $request->spouse_gotra,
            "divyang" => $request->spouse_divyang,
            "mosad" => $request->spouse_mosad,
            'occupation_building' => $request->spouse_occupation_building,
            'occupation_landmark' => $request->spouse_occupation_landmark,
            'occupation_road' => $request->spouse_occupation_road,
            'occupation_pincode' => $request->spouse_occupation_pincode,
            'occupation_vibhag' => $request->spouse_occupation_vibhag,
            'occupation_city' => $occupation_city_id,
            'occupation_state' => $occupation_state_id,
            'occupation_country' => $occupation_country_id,

            'current_building' => $request->spouse_current_building,
            'current_landmark' => $request->spouse_current_landmark,
            'current_road' => $request->cspouse_urrent_road,
            'current_pincode' => $request->spouse_current_pincode,
            'current_vibhag' => $request->spouse_current_vibhag,
            'current_city' => $current_city_id,
            'current_state' => $current_state_id,
            'current_country' => $current_country_id,
            
            'permanent_building' => $permanent_building,
            'permanent_landmark' => $permanent_landmark,
            'permanent_road' => $permanent_road,
            'permanent_pincode' => $permanent_pincode,
            'permanent_vibhag' => $request->spouse_permanent_vibhag,
            'permanent_city' => $permanent_city,
            'permanent_state' => $permanent_state,
            'permanent_country' => $permanent_country,
            'education' => $request->spouse_education,
            'college' => $request->spouse_college,
            'passout_year' => $request->spouse_year,
            'university' => $request->spouse_university,
            'occupation' => $request->spouse_occupation,
            'category' => $request->spouse_category,
            'company_name' => $request->spouse_company_name,
            // 'contact_person_name' => $request->contact_person_name,
            'contact_person_mobile' => $request->spouse_contact_person_mobile,
            // 'contact_person_address' => $request->contact_person_address,
            "charity" => $request->spouse_charity ,
            "charity_organization" => $request->spouse_charity_organization ,
            "charity_designation" => $request->spouse_charity_designation ,
            "serve" => $request->spouse_serve ,
            "serve_help" => $request->spouse_serve_help ,
            "donate" => $request->spouse_donate ,
            "donate_part" => json_encode($request->spouse_donate_part),
        ]);

        $data = ["user_id" => $user->id];
        return $this->success('User Update', $data, 200);
    }

    public function updatechild(Request $request, User $user) {
        
        $userInfo = UserInfo::where('user_id',$user->id)->first();
        
        $validator = Validator::make($request->all(), [
            'children_first_name' => 'required',
            'children_middle_name' => 'required',
            'children_last_name' => 'required',
            'children_dob' => 'required',
            'children_gender' => 'required',
        ]);

        if($validator->fails()){
            return $this->failure($validator->errors()->toJson(), null, 400);
        }
        
        if(!empty($request->email) && $request->email != $user->email ){
            $email = User::where('email',$request->email)->first();
            if($email != null){
                return $this->failure('Email Alreay Exists !!', null, 500);
            }
        }
        
        if(!empty($request->mobile) && $request->mobile != $userInfo->mobile ){
            $mobile = UserInfo::where('mobile',$request->mobile)->orWhere('alternate_mobile', $request->mobile)->first();
            if($mobile != null){
                return $this->failure('Mobile Alreay Exists !!', null, 500);
            }
        }

        if ($request->children_visibility == "on") {
            $children_visibility = 1;
        }
        else {
            // $children_visibility = 0; // nitin temp
            $children_visibility = 1;
        }
        if ($request->children_alternate_visibility == "on") {
            $children_alternate_visibility = 1;
        }
        else {
            // $alternate_visibility = 0;// nitin temp
            $children_alternate_visibility = 1;
        }

        //dd($request->all());
        
        //dd($city_id);

        $current_city_id = $request->children_current_city;
        //dd($current_city_id);
        $current_state_id = $request->children_current_state;
        $current_country_id = $request->children_current_country;

        $permanent_city_id = $request->children_permanent_city;
        $permanent_state_id = $request->children_permanent_state;
        $permanent_country_id =  $request->children_permanent_country;

        $occupation_city_id = $request->children_occupation_city;
        $occupation_state_id = $request->children_occupation_state;
        $occupation_country_id =  $request->children_occupation_country;
        //dd($current_city_id, $current_country_id, $current_state_id, $permanent_city_id, $permanent_state_id, $permanent_country_id);

        $permanent_building  = $request->children_permanent_building ;
        $permanent_landmark = $request->children_permanent_landmark ;
        $permanent_road = $request->children_permanent_road ;
        $permanent_pincode = $request->children_permanent_pincode ;
        $permanent_city = $permanent_city_id ;
        $permanent_state = $permanent_state_id ;
        $permanent_country = $permanent_country_id ;
        //dd($request->all());
        
        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $user->update([
            "name"=> $request->children_first_name,
            "gender_id" => $request->children_gender,
            "dob"=>$request->children_dob ,
            "email"=> $request->email ,
            "city"=> $request->children_current_city,
            "phone" => $request->mobile
        ]);

        $userInfo->update([
            "last_name" => $request->children_last_name,
            "middle_name" => $request->children_middle_name,
            "last_name_original" => $request->children_last_name_original,
            "native_place" => $request->children_native_place,
            "blood_group" =>$request->children_bloodgroup, 
            "mobile_code" => $request->children_mobile_code,
            "mobile" => $request->mobile,
            'visibility' => $children_visibility,

            "alternate_mobile_code" => $request->children_alternate_mobile_code,
            "alternate_mobile" => $request->alternate_mobile,
            'alternate_mobile_visibility' => $children_alternate_visibility,
            "birth_place" => $request->children_birth_place,
            "gotra" => $request->children_gotra,
            "divyang" => $request->children_divyang,
            "mosad" => $request->children_mosad,
            'occupation_building' => $request->children_occupation_building,
            'occupation_landmark' => $request->children_occupation_landmark,
            'occupation_road' => $request->children_occupation_road,
            'occupation_pincode' => $request->children_occupation_pincode,
            'occupation_vibhag' => $request->children_occupation_vibhag,
            'occupation_city' => $occupation_city_id,
            'occupation_state' => $occupation_state_id,
            'occupation_country' => $occupation_country_id,

            'current_building' => $request->children_current_building,
            'current_landmark' => $request->children_current_landmark,
            'current_road' => $request->cchildren_urrent_road,
            'current_pincode' => $request->children_current_pincode,
            'current_city' => $current_city_id,
            'current_state' => $current_state_id,
            'current_country' => $current_country_id,
            
            'permanent_building' => $permanent_building,
            'permanent_landmark' => $permanent_landmark,
            'permanent_road' => $permanent_road,
            'permanent_pincode' => $permanent_pincode,
            'permanent_city' => $permanent_city,
            'permanent_state' => $permanent_state,
            'permanent_country' => $permanent_country,
            'education' => $request->children_education,
            'college' => $request->children_college,
            'passout_year' => $request->children_year,
            'university' => $request->children_university,
            'occupation' => $request->children_occupation,
            'category' => $request->children_category,
            'company_name' => $request->children_company_name,
            // 'contact_person_name' => $request->contact_person_name,
            'contact_person_mobile' => $request->children_contact_person_mobile,
            // 'contact_person_address' => $request->contact_person_address,
            "charity" => $request->children_charity ,
            "charity_organization" => $request->children_charity_organization ,
            "charity_designation" => $request->children_charity_designation ,
            "serve" => $request->children_serve ,
            "serve_help" => $request->children_serve_help ,
            "donate" => $request->children_donate ,
            "donate_part" => json_encode($request->children_donate_part),
            'marital_status' => $request->children_marital_status,
        ]);

        $data = ["user_id" => $user->id];
        return $this->success('User Update', $data, 200);
    }
}
